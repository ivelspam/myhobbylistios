import UIKit
import Alamofire
import SwiftyJSON

class LoginInVC: UIViewController {

    @IBOutlet weak var loginView: UIView!
    @IBOutlet var emailTV: TextFieldForm!
    @IBOutlet weak var passwordTV: TextFieldForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("LoginInViewController")
      
        emailTV.customInit(parentView: nil, textFieldType: .Email)
        passwordTV.customInit(parentView: emailTV, textFieldType: .Password)
    }
    
    @IBOutlet weak var temp: UIButton!
    
    func checkErrors(){
        
        var checks = [Bool]()
        
        if(!emailTV.check()){
          checks.append(false)
        }
        
        if(!passwordTV.check()){
            checks.append(false)
        }
        
        
        if(checks.count == 0){
            submitLogin()
        }
        
    }
    
   func submitLogin(){
        
        let parameters = ["Email" : emailTV.text!, "Password" : passwordTV.text!]
        
        ServerRequest.postRequest(url: "api/login/email", parameters: parameters, usingToken: false) { (response, worked) in
            print(response)
            
            if response["message"].stringValue == "match"{
                UserDefaults.standard.set(response["token"].string, forKey: UserDefaultsKeys.TOKEN_STR.rawValue)
                self.performSegue(withIdentifier: SegueName.LoggedInSegue.rawValue, sender: nil)
                
            }
        }
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        print("login button pressed")
       checkErrors()
        

    }
    
    @IBAction func otherLogin(_ sender: Any) {
        Alamofire.request("https://synchobby.com/api/login/email", method: .post, parameters: ["Email" : "junior_yxpksug_araujo@tfbnw.net", "Password" : "F0caliz4r"])
            .responseJSON{response in
                if response.result.isSuccess {
                    let requestJSON : JSON = JSON(response.result.value!)
                    let message = requestJSON["message"]
                    if message == "match"{
                        UserDefaults.standard.set(requestJSON["token"].string, forKey: UserDefaultsKeys.TOKEN_STR.rawValue)
                        self.performSegue(withIdentifier: SegueName.LoggedInSegue.rawValue, sender: nil)

                    }
                }
        }
    }
   
}

