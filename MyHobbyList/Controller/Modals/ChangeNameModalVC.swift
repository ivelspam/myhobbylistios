import UIKit
import Alamofire
import SwiftyJSON

class ChangeNameModalVC: UIViewController{
    
    @IBOutlet weak var nameTF: TextFieldForm!
    @IBOutlet weak var warningLBL: UILabel!
    
    var delegate : ChangeProfileNameDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTF.text = UserDefaults.standard.string(forKey: DefaultKey.Name_str.rawValue)
        nameTF.customInit(topAnchorView: nil, textFieldType: .Name)
        
    }
    
    func checkErrors(){
        var valid = true
        if(!nameTF.checkIfIsValid()){
            valid = false
        }
        
        if(valid){
            submitChange()
        }
    }
    
    func submitChange(){
        ServerRequest.postRequest(request : Appuser_updateName(name: nameTF.text!)){
            allInfoJSON, worked in
            
            if(worked){
                self.warningLBL.text = "Changed"

                UserDefaults.standard.set(self.nameTF.text!, forKey: DefaultKey.Name_str.rawValue)
                self.delegate?.changeName()
            }else{
                self.warningLBL.text = "The server is offline"
            }
        }
    }

    @IBAction func btNameChange(_ sender: Any) {
        checkErrors()
    }
    
    @IBAction func btClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
