import UIKit
import Alamofire
import SwiftyJSON

class ChangeUsernameModalVC: UIViewController{

    var delegate : ChangeProfileNameDelegate?
    @IBOutlet weak var usernameTF: TextFieldForm!
    @IBOutlet weak var lblUsernameMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTF.customInit(topAnchorView: nil, textFieldType: .Username)

        usernameTF.text = UserDefaults.standard.string(forKey: DefaultKey.Username_str.rawValue)
    }
    
    func checkFormIsValid(){
        
        var valid = true
        
        if(!usernameTF.checkIfIsValid()){
            valid = false
        }
        
        if(valid){
            submitChange()
        }
    }
    
    @IBAction func btClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func submitChange(){
        
        let message = lblUsernameMessage!;
        
        ServerRequest.postRequest(request: Appuser_updateUsername(username : usernameTF.text!)){
            allInfoJSON, worked in
            
            if(worked){
                message.text = "Changed"
                UserDefaults.standard.set(self.usernameTF.text!, forKey: DefaultKey.Username_str.rawValue)
                self.delegate?.changeUsername()
            }else{
                message.text = "The server is offline"
            }
        }
    }
   
    @IBAction func btChangeUsername(_ sender: Any) {
        checkFormIsValid()
    }
}

