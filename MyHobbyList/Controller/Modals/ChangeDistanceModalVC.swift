import UIKit

class ChangeDistanceModalVC: UIViewController {

    @IBOutlet var scMeasurement: UISegmentedControl!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDistanceMessage: UILabel!
    @IBOutlet weak var sldDistance: UISlider!
    
    var distance = UserDefaults.standard.float(forKey: DefaultKey.Distance_int.rawValue)
    var measure = "Miles"
    var multiplier : Float = 100;
    var delegate : ChangeProfileNameDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let measurement = UserDefaults.standard.string(forKey: DefaultKey.MeasureSystem_str.rawValue){
            if measurement == "mi"{
                lblDistance.text = "\(Int(distance)) miles"
            }else{
                multiplier = 160
                measure = "Kilometers"
                scMeasurement.selectedSegmentIndex = 1
                lblDistance.text = "\(Int(distance)) \(measure)"
            }
            sldDistance.value = distance/multiplier
        }
        
    }
    
    @IBAction func btClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scMeasurement(_ sender: Any) {
        
        
        let index = scMeasurement.selectedSegmentIndex
        switch index {
        case 0 :
            multiplier = 100
            measure = "Miles"
        case 1 :
            measure = "Kilometers"
            multiplier = 160
        default :
            print("default")
            
        }
        updateValue()
    }
    
    func updateValue (){
        distance = sldDistance.value * multiplier
        lblDistance.text = "\(Int(distance)) \(measure)"
    }
    
    @IBAction func sldDistanceRange(_ sender: Any) {
        updateValue()
    }
    
    @IBAction func btChangeDistance(_ sender: Any) {
        
        let message = lblDistanceMessage!
        var measureSystemType = Appuser_UpdateDistanceAndMeasurementSystem.MeasureSystemType.km
        if measure == "Miles"{
            measureSystemType = Appuser_UpdateDistanceAndMeasurementSystem.MeasureSystemType.mi
        }
        
        let appuser_UpdateDistanceAndMeasurementSystem = Appuser_UpdateDistanceAndMeasurementSystem(measureSystemType: measureSystemType, distance: Int(distance))
        
        ServerRequest.postRequest(request: appuser_UpdateDistanceAndMeasurementSystem){
            allInfoJSON, worked in
            
            if(worked){
                message.text = "Changed"
                UserDefaultsUtils.set(value: self.distance, key: .Distance_int)
                UserDefaultsUtils.set(value: measureSystemType.rawValue, key: .MeasureSystem_str)
               
                self.delegate?.changeDistance()
            }else{
                message.text = "The server is offline"
            }
        }
    }

}
