import UIKit
import Alamofire
import SwiftyJSON

class ChangeNameViewController: UIViewController{

    var profileMenuCat : ProfileMenu.ProfileMenuCat?
    let w = UIScreen.main.bounds.width
    let h = UIScreen.main.bounds.height
    
    @IBOutlet weak var contentViews: UIView!
    
    var delegate : ChangeProfileNameDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch profileMenuCat! {
            case .Name:
                build_name()
            case .Username:
                build_username()
            case .Distance:
                build_distance()
            default:
                print("default")
            }
    }
    
    @IBAction func btClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //Name
    @IBOutlet weak var tvName: UITextField!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var nameView: UIView!

    
    func build_name(){
        tvName.text = UserDefaults.standard.string(forKey: UserDefaultsKeys.NAME_STR.rawValue)
        nameView.isHidden = false
        nameView.center = CGPoint(x: w / 2, y: h / 2)
    }
    
    @IBAction func btNameChange(_ sender: Any) {
        
        if let name = tvName.text{
            if name.count > 3 {
                
                ServerRequest.postRequest(request : Appuser_updateName(name: name)){
                    allInfoJSON, worked in
                    print(allInfoJSON)
                    
                    if(worked){
                        self.lblWarning.text = "Changed"
                        print(allInfoJSON)
                        
                        print(name)
                        UserDefaults.standard.set(name, forKey: UserDefaultsKeys.NAME_STR.rawValue)
                        self.delegate?.changeName()
                    }else{
                        self.lblWarning.text = "The server is offline"
                    }
                }
                
            }else{
                lblWarning.text = "Name need at Least 3 Characters"
            }
        } else {
            lblWarning.text = "Name is Empty"
        }
    }
    
    
    //Username
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var lblUsernameMessage: UILabel!
    @IBOutlet weak var usernameView: UIView!
    func build_username(){
        tfUsername.text = UserDefaults.standard.string(forKey: UserDefaultsKeys.USERNAME_STR.rawValue)
        usernameView.isHidden = false
        usernameView.center = CGPoint(x: w / 2, y: h / 2)
    }
    
    @IBAction func btChangeUsername(_ sender: Any) {
        let message = lblUsernameMessage!;
        
        if let username = tfUsername.text{
            if username.count > 5 {
                let parameters : Dictionary = ["Username" : username]
                ServerRequest.postRequest(url : "api/appuser/updateusername", parameters : parameters, usingToken : true){
                    allInfoJSON, worked in
                    
                    if(worked){
                        message.text = "Changed"
                        UserDefaults.standard.set(username, forKey: UserDefaultsKeys.USERNAME_STR.rawValue)
                        self.delegate?.changeUsername()
                    }else{
                        message.text = "The server is offline"
                    }
                }
            }else{
                message.text = "Name need at Least 5 Characters"
            }
        } else {
            message.text = "Name is Empty"
        }
        
    }
    
    
    //Distance
    
    @IBOutlet var scMeasurement: UISegmentedControl!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDistanceMessage: UILabel!
    
    @IBOutlet weak var sldDistance: UISlider!
    var distance = UserDefaults.standard.float(forKey: UserDefaultsKeys.DISTANCE_INT.rawValue)
    var measure = "Miles"
    var multiplier : Float = 100;
    
    func build_distance(){
        
        if let measurement = UserDefaults.standard.string(forKey: UserDefaultsKeys.MEASURE_STR.rawValue){
            if measurement == "mi"{
                lblDistance.text = "\(Int(distance)) miles"
            }else{
                multiplier = 160
                measure = "Kilometers"
                scMeasurement.selectedSegmentIndex = 1
                lblDistance.text = "\(Int(distance)) \(measure)"
            }
            
            sldDistance.value = distance/multiplier
            
            
            distanceView.center = CGPoint(x: w / 2, y: h / 2)
            distanceView.isHidden = false

        }
    }

    @IBAction func scMeasurement(_ sender: Any) {
        
        
        let index = scMeasurement.selectedSegmentIndex
        switch index {
            case 0 :
                multiplier = 100
                measure = "Miles"
            case 1 :
                measure = "Kilometers"
                multiplier = 160
            default :
            print("default")
           
        }
        
        updateValue()
        
    }
    
    func updateValue (){
        distance = sldDistance.value * multiplier
        
        lblDistance.text = "\(Int(distance)) \(measure)"
    }
    
    @IBAction func sldDistanceRange(_ sender: Any) {
        updateValue()
        
    }
    
    
    @IBAction func btChangeDistance(_ sender: Any) {
        
        let message = lblDistanceMessage!;
        let distanceString = String(Int(distance))
        var measurement = "km"
        if measure == "Miles"{
            measurement = "mi"
        }
       
        let parameters = ["Distance" : distanceString, "MeasureSystem" : measurement]
        ServerRequest.postRequest(url : "api/appuser/updateUserDistanceAndMeasurement", parameters : parameters, usingToken : true){
            allInfoJSON, worked in

            if(worked){
                message.text = "Changed"
                UserDefaults.standard.set(distanceString, forKey: UserDefaultsKeys.DISTANCE_INT.rawValue)
                UserDefaults.standard.set(measurement, forKey: UserDefaultsKeys.MEASURE_STR.rawValue)

                self.delegate?.changeDistance()
            }else{
                message.text = "The server is offline"
            }
        }
    }
}
