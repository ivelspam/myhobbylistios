import UIKit


class SentRequestVC: UIViewController {

    @IBOutlet weak var img: UIImageView!
    var delegate : UpdateFriendsDelegate?

    var friend : Friend?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tempFriend = friend{
            Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: tempFriend.AppUserID, imageVersion: tempFriend.ImageVersion){
                image in
                self.img.image = image
            }
        }
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func removeRequest(_ sender: Any) {
        ServerRequest.postRequest(request : Friend_cancel(appUserID: friend!.AppUserID)) { (response, bool) in
            print(response)
            if(response["message"].stringValue == "canceled"){
//                Motherbase().getFriendTable().funcDeleteFriend(appUserID: self.friend?.AppUserID ?? -1)
                self.delegate?.update(friend: self.friend!, updateFriendDelegateOption: UpdateFriendsDelegateOption.removeSentRequest)
            }
        }
    }
    
}
