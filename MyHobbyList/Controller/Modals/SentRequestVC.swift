import UIKit


class SentRequestVC: UIViewController {

    @IBOutlet weak var img: UIImageView!
    var delegate : UpdateFriendsDelegate?

    @IBOutlet weak var responseLBL: UILabel!
    var friend : Friend?
    
    @IBOutlet weak var vCancelBT: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tempFriend = friend{
            Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: tempFriend.AppUserID, imageVersion: tempFriend.ImageVersion){
                image in
                self.img.image = image
            }
        }
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func removeRequest(_ sender: Any) {
        print("Remove request")
        ServerRequest.postRequest(request : Friend_cancel(appUserID: friend!.AppUserID)) { (response, bool) in
            print(response)
            if(response["message"].stringValue == "canceled"){
                self.responseLBL.text = "Canceled"
                self.vCancelBT.isHidden = true
                self.friend!.State = FriendState.Removed
                self.delegate?.update(friend: self.friend!, updateFriendDelegateOption: UpdateFriendsDelegateOption.removeSentRequest)
            }
        }
    }
    
}
