import UIKit

class AcceptRequestViewController: UIViewController {
    
    @IBOutlet weak var img: UIImageView!
    var delegate : UpdateFriendsDelegate?

    var friend : Friend?
    
    @IBOutlet weak var responseLBL: UILabel!
    
    @IBOutlet weak var acceptBT: UIButton!
    @IBOutlet weak var removeBT: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tempFriend = friend {
            Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE ,appUserID: tempFriend.AppUserID, imageVersion: tempFriend.ImageVersion) { (image) in
                self.img.image = image
            }
        }
    }

    @IBAction func removeRequest(_ sender: UIButton) {
        if let tempFriend = friend{
            
            ServerRequest.postRequest(request: Friend_cancel(appUserID: tempFriend.AppUserID)) { (response, bool) in
                print(response)
                if(response["message"].stringValue == Friend_cancel.Response.canceled){

                        self.clickedButton(label: "Canceled")
                    Motherbase().getFriendTable().funcDeleteFriend(appUserID: tempFriend.AppUserID)
                    self.friend?.State = FriendState.Removed
                    
                    self.delegate?.update(friend: tempFriend, updateFriendDelegateOption: UpdateFriendsDelegateOption.rejectReceivedRequest)
                }
            }
        }
    }
    
    @IBAction func acceptRequest(_ sender: UIButton) {
        ServerRequest.postRequest(request: Friend_accept(appUserID: friend!.AppUserID)) { (response, bool) in
            print(response)
            if(response["message"].stringValue == Friend_accept.Response.accepted){
                

                self.clickedButton(label: "Accepted")

                Motherbase().getFriendTable().updateFriendState(appUserID: self.friend?.AppUserID ?? -1, state: 2)
                
                self.friend?.State = FriendState.Confirmed
                self.delegate?.update(friend: self.friend!, updateFriendDelegateOption: UpdateFriendsDelegateOption.acceptReceivedRequest)
            }
        }
    }
    
    func clickedButton(label : String){
        self.responseLBL.text = label
        self.acceptBT.isHidden = true
        self.removeBT.isHidden = true
        
    }
}
