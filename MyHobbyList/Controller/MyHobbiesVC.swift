import UIKit

protocol MyHobbiesProtocol {
    func updateHobby(hobbyID : Int, inHobby: Bool, option : String)
}

class MyHobbiesVC: UIViewController, MyHobbiesProtocol
{

    @IBOutlet weak var parentView: UIView!
    @IBOutlet var scOptions: UISegmentedControl!
    let myHobbiesSubviewVC = MyHobbiesSubviewVC();
    let categorySubviewVC = CategorySubviewVC();

    @IBOutlet weak var ShowAll: UIBarButtonItem!
    var subviews = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myHobbiesSubviewVC.customInit(myHobbiesProtocol: self)
        subviews.append(myHobbiesSubviewVC)
        
        categorySubviewVC.customInit(myHobbiesProtocol: self)
        subviews.append(categorySubviewVC)

        for subview in subviews {
            parentView.addSubview(subview.view)
            subview.view.frame = parentView.bounds
        }
        parentView.bringSubview(toFront: subviews[0].view)
    }
    
    @IBAction func scOptions(_ sender: Any) {
        switch scOptions.selectedSegmentIndex {
            case 0:
                parentView.bringSubview(toFront: subviews[0].view)
            case 1:
                parentView.bringSubview(toFront: subviews[1].view)
            default:
                print("error occurs")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SearchHobbyVC {
            destination.myHobbiesProtocol = self
        }
    }
    
    func addHobby(hobbyID: Int, inHobby: Bool) {
        
    }

    func updateHobby(hobbyID: Int, inHobby: Bool, option: String) {
        
        if("categories" == option){
            myHobbiesSubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
        }else if("list" == option){
            categorySubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
        }else if("both" == option){
            categorySubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
            myHobbiesSubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
        }else{
            print("MyHobbiesVC updateHobby ERROR")
        }
    }
}


