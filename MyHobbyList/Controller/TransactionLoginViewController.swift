import UIKit
import Alamofire
import SwiftyJSON
import JWTDecode
import CoreLocation
import SQLite

class TransactionLoginViewController: UIViewController, gotUpdatedLocationDelegate {
    
    let dispatchGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        checkToken()
    }
    
    func checkToken(){
        print("Check Token")
        if let token = UserDefaults.standard.value(forKey: DefaultKey.Token_str.rawValue) as? String {
            do {
                let jwt = try decode(jwt: token)
                print(jwt.body["Email"]!)
                print(jwt)
                
                Location.start()
                Radar.shared.getNewUpdate(delegate: self)

            }catch let error as NSError{
                print(error.localizedDescription)
            }
            
        }else{
            performSegue(withIdentifier: SegueName.TokenNotFound.rawValue , sender: nil)
        }
    }
    
    func gotLocation(location: CLLocation?) {
        
        print("Update All or Download new")
        
        if UserDefaults.standard.string(forKey: DefaultKey.UpdatedTS_str.rawValue) != nil{
            print("got location")

            updateAllInfo()
        }else{
            downloadAllInfo()
        }
        RepeatingServerRequest.shared.startTimer()
    }
    
    func updateAllInfo(){
        
        ServerRequest.postRequest(request: AllInfo_getUpdates()) { (updates, worked) in
            if updates["message"].string == AllInfo_getUpdates.Response.foundUpdates {
                RequestServerUpdates.updateTables(updates: updates)
            }
            
            self.performSegue(withIdentifier: SegueName.TokenChecksOut.rawValue, sender: nil)
            
        }
    }
    
    func downloadAllInfo(){
        print("downloadAllInfo")
        ServerRequest.postRequest(request : AllInfo_getAllInfo()){
            allInfoJSON, worked in
            
            self.saveData(allInfoJSON: allInfoJSON)
            self.performSegue(withIdentifier: SegueName.TokenChecksOut.rawValue, sender: nil)
            Location.start()
        }
    }
    
    func saveData(allInfoJSON : JSON){
        print("saveToDatabase")
        UserDefaultsUtils.fillTheUserDefaults(allInfoJSON: allInfoJSON)
        RequestServerUpdates.fillTheDatabase(allInfoJSON: allInfoJSON)
    }
}

//        ServerRequest.getRequest(url : "api/firebase/getCustomToken", parameters: [:], usingToken : true){
//            result, worked in
//            print("api/firebase/getCustomToken")



//            Auth.auth().signIn(withCustomToken: result["customToken"].string ?? "") { (user, error) in
//
//                print("custom tokensssss")
//
//
//            }
//        }
