import UIKit


protocol UpdateMyHobbies{
    func updateMyHobbies(hobby : Hobby)
}

class SearchHobbyVC: UIViewController {

    @IBOutlet weak var filterTF: UITextField!
    @IBOutlet weak var hobbiesTV: UITableView!
    
    var hobbies = [Hobby]()
    var hobbyTable = Motherbase().getHobbyTable()
    var currentHobby : Hobby?
    
    @IBOutlet weak var returnBT: UIButton!
    var myHobbiesProtocol : MyHobbiesProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        hobbiesTV.keyboardDismissMode = .onDrag
        
        var image = UIImage(named: "left arrow")
        image = image?.scaleImageToSize(newSize: CGSize(width: 30, height: 30))
        returnBT.setImage(image, for: .normal)
        loadNewParent(hobby: nil)
        buildTable()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if filterTF.text?.count == 0 {
            loadNewParent(hobby: currentHobby)
            
        }else{
            getFilterNode()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getFilterNode(){
        
        hobbies = hobbyTable.selectHobbyByToken(token: filterTF.text!)
        hobbiesTV.reloadData()
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func btGoToParent(_ sender: UIButton) {
        loadNewParent(hobby: hobbyTable.selectBy(hobbyID: currentHobby!.ParentID))
    }
}

extension SearchHobbyVC: UITableViewDelegate, UITableViewDataSource {
    
    func buildTable(){
        
        //give delegates
        hobbiesTV.delegate = self
        hobbiesTV.dataSource = self
        
        //Register Nibs
        hobbiesTV.register(UINib(nibName: "ParentTVC", bundle: nil), forCellReuseIdentifier: "ParentTVC")
        hobbiesTV.register(UINib(nibName: "HobbyTVC", bundle: nil), forCellReuseIdentifier: "HobbyTVC")
        
        //Get Data
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hobbies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = hobbies[indexPath.row]
        
        if row.aType == "child" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTVC") as! HobbyTVC
            cell.customInit(hobby: row, myHobbiesProtocol: myHobbiesProtocol, option: "both")
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParentTVC") as! ParentTVC
            cell.customInit(hobby: row)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hobby = hobbies[indexPath.row]
        
        if hobby.aType == "parent" {
            loadNewParent(hobby: hobby)

        }
    }
    
    func loadNewParent(hobby: Hobby?){
        
        print("func loadNewParent(hobby: Hobby?)")
        
        var parentID = -1
        
        if let tempHobby = hobby {
            currentHobby = tempHobby.copy() as? Hobby
            parentID = currentHobby!.HobbyID
        }
        
        hobbies = hobbyTable.getHobbiesByParent(parentID: parentID)
        hobbiesTV.reloadData()
        filterTF.text = ""
        
        if parentID == -1 {
            returnBT.setTitle("Categories", for: .normal)
            returnBT.isEnabled = false
            returnBT.setTitle("Hobbies", for: .disabled)
            currentHobby = nil
            
        }else{
            returnBT.isEnabled = true
            returnBT.setTitle(currentHobby?.Name, for: .normal)
        }
    }
    
}
