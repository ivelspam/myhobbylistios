import UIKit
import Foundation
protocol ChangeSegueDelegate {
    func changeSegue(segueName : SegueName, friend : Friend)
}

protocol UpdateFriendsDelegate {
    func update(friend : Friend, updateFriendDelegateOption : UpdateFriendsDelegateOption)
}

enum  UpdateFriendsDelegateOption{
    case sendSentRequest
    case rejectReceivedRequest
    case acceptReceivedRequest
    case removeFriendRequest
    case removeSentRequest
    case unexpectedErrow
}

class FriendsVC: UIViewController, ChangeSegueDelegate, UpdateFriendsDelegate {
    
    var mClientUpdatedTS = MagicNumbers.LOWEST_TS
    
    let friendTable = Motherbase().getFriendTable()
    
    @IBOutlet weak var tempView: UIView!
    let subviews = [FriendSegmentVC(), FriendSegmentVC(), FriendSegmentVC()]
    var updateTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subviews[FriendState.Confirmed.rawValue].customInit(segueName: SegueName.OpenFriendChat, friendState: FriendState.Confirmed, changeSegueDelegate: self)
        
        subviews[FriendState.ReceivedRequest.rawValue].customInit(segueName: SegueName.ShowFriendReceivedRequest, friendState: FriendState.ReceivedRequest, changeSegueDelegate: self)
        
        subviews[FriendState.SentRequest.rawValue].customInit(segueName: SegueName.ShowFriendSentRequest, friendState: FriendState.SentRequest, changeSegueDelegate: self)
        
        for subview in subviews{
            tempView.addSubview(subview.view)
            subview.view.frame = tempView.bounds
        }
        
        selectedView(option: 0)
        
        startUpdateTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("FriendsVC: viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("FriendsVC: viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("FriendsVC: viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("FriendsVC: viewDidDisappear")
    }
    
    @IBAction func openFindFriendsSegue(_ sender: UIButton) {
        performSegue(withIdentifier: SegueName.OpenFindFriends.rawValue, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("FriendsVC perform segue")
        switch segue.identifier {
            case SegueName.OpenFriendChat.rawValue:
                if let destination = segue.destination as? ChatVC {
                    destination.delegate = self
                    destination.friend = sender as? Friend
                }
            case SegueName.ShowFriendReceivedRequest.rawValue:
                if let destination = segue.destination as? AcceptRequestViewController {
                    destination.delegate = self
                    destination.friend = sender as? Friend
                }

            case SegueName.ShowFriendSentRequest.rawValue:
                if let destination = segue.destination as? SentRequestVC  {
                    destination.delegate = self
                    destination.friend = sender as? Friend
                }
            case SegueName.OpenFindFriends.rawValue:
                if let destination = segue.destination as? FindFriendVC  {
                    destination.delegate = self
                }
            default:
                print("This is Unexpected")
        }
    }
    
    func update(friend : Friend, updateFriendDelegateOption: UpdateFriendsDelegateOption) {
       
        for tempView in self.subviews {
            tempView.change(friend: friend)
        }
        for tempView in self.subviews {
            tempView.makeSortedList()
        }
    }
    
    @IBAction func switchViewsAction(_ sender: UISegmentedControl) {
        selectedView(option : sender.selectedSegmentIndex)
    }
    
    func selectedView(option: Int){
        var selectedOption = 0
        switch option {
        case 0:
            selectedOption = FriendState.Confirmed.rawValue
        case 1:
            selectedOption = FriendState.ReceivedRequest.rawValue
        case 2:
            selectedOption = FriendState.SentRequest.rawValue
        default:
            print("DEFAULT VALUE ERROR")
        }
        tempView.bringSubview(toFront:  subviews[selectedOption].view)
    }
    
    func changeSegue(segueName : SegueName, friend: Friend) {
        performSegue(withIdentifier: segueName.rawValue, sender: friend)

    }
  
}

extension FriendsVC {
    
    func startUpdateTimer(){
        updateTimer = Timer.scheduledTimer(timeInterval: RepTimers.UPDATE_FRIENDS, target: self, selector: #selector(getNewFriends), userInfo: nil, repeats: true)
        getNewFriends()
    }
    
    @objc func getNewFriends(){
        
        DispatchQueue.global().async {
            let friends = self.friendTable.selectBy(clientUpdatedTS: self.mClientUpdatedTS)
            
            if friends.count > 0 {
                
                for friend in friends {
                    
                    if self.mClientUpdatedTS < friend.ClientUpdatedTS {
                        self.mClientUpdatedTS = friend.ClientUpdatedTS
                    }

                    for tempView in self.subviews {
                        tempView.change(friend: friend)
                    }
                }
                
                DispatchQueue.main.async {
                    
                    for tempView in self.subviews {
                        tempView.makeSortedList()
                    }
                }
                
            }
        }
    }
}


