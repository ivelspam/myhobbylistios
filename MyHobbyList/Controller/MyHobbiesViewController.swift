import UIKit

class MyHobbiesVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    func showChildren(hobby: Hobby) {
        print("aaaaaaa")
    }
    
    var appUserHobbyTable = Motherbase().getAppUserHobbyTable()
    var hobbyTable = Motherbase().getHobbyTable()

    //HobbiesTable
    var appUserHobbies : [Hobby] = []
    var appUserHobbiesFiltered : [Hobby] = []
    
    
    let temp : [Hobby] = [
        Hobby(hobbyID: 1, name: "first", type: "parent", version: 0, parentID: 3, inHobby: true),
        Hobby(hobbyID: 2, name: "second", type: "parent", version: 0, parentID: 3, inHobby: true),
        ]
    
    let dispatchGroup = DispatchGroup()
    
    //CategoriesTable
    var categoriesRows : [Hobby] = []
    var categoriesRowsFiltered : [Hobby] = []
    var currentParent : Hobby?
    
    @IBOutlet var scOptions: UISegmentedControl!
    @IBOutlet var tfHobbyFilter: UITextField!
    
    @IBOutlet weak var hobbiesView: UIView!
    @IBOutlet weak var categoriesView: UIView!
    
    @IBOutlet var hobbiesTableView: UITableView!
    @IBOutlet var categoriesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        appUserHobbies = appUserHobbyTable.selectAppUserHobbyComplete()
        appUserHobbiesFiltered = appUserHobbies
        
        getParentNode()
        updateButton(hobby : currentParent, isEnabled: false)
        hobbiesTableView.register(UINib(nibName: "HobbyTableCell", bundle: nil), forCellReuseIdentifier: "HobbyTableCell")
        
        
 
    
        
        categoriesTableView.register(UINib(nibName: "ParentTableViewCell", bundle: nil), forCellReuseIdentifier: "ParentTableViewCell")
        categoriesTableView.register(UINib(nibName: "HobbyTableCell", bundle: nil), forCellReuseIdentifier: "HobbyTableCell")
        
    }
    
    @IBAction func scOptions(_ sender: Any) {
        switch scOptions.selectedSegmentIndex {
            case 0:
                hobbiesView.isHidden = false
                categoriesView.isHidden = true

            case 1:
                hobbiesView.isHidden = true
                categoriesView.isHidden = false
            default:
                print("default")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case hobbiesTableView:
            return appUserHobbiesFiltered.count
        case categoriesTableView:
//            return categoriesRowsFiltered.count
            return categoriesRows.count

        default:
            print("default")
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("cellForRowAt categoriesTableView")

        switch tableView {
            case hobbiesTableView:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTableCell") as! HobbyTableCell
                print(appUserHobbies[indexPath.row].toString())
                cell.customInit(hobby: appUserHobbiesFiltered[indexPath.row])
                return cell
            case categoriesTableView:
                if categoriesRows[indexPath.row].aType == "child" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTableCell") as! HobbyTableCell
                    print(categoriesRows[indexPath.row].toString())
                    cell.customInit(hobby: categoriesRows[indexPath.row])
                    cell.selectionStyle = .none
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ParentTableViewCell") as! ParentTableViewCell
                    cell.customInit(hobby: categoriesRowsFiltered[indexPath.row])
                    cell.selectionStyle = .none

                    return cell
                }
 
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTableCell") as! HobbyTableCell
                print(appUserHobbies[indexPath.row].toString())
                cell.customInit(hobby: appUserHobbiesFiltered[indexPath.row])
                cell.selectionStyle = .none
                return cell
        }
    }
    
    @IBAction func tfHobbiesFilterEditingChanged(_ sender: UITextField) {
        
        if(tfHobbyFilter.text?.count == 0){
            appUserHobbiesFiltered = appUserHobbies
        }else{
            appUserHobbiesFiltered = appUserHobbies.filter{
                matches(for: tfHobbyFilter.text!, in: $0.Name).count > 0
            }
        }
        hobbiesTableView.reloadData()

    }
    
    @IBOutlet weak var btToTheParent: UIButton!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
        
        DispatchQueue.global(qos: .default).async {
            // Bounce back to the main thread to update the UI
            
            switch tableView {
            case self.hobbiesTableView:
                print("hobbiesTableView")
            case self.categoriesTableView:
                print("categoriesTableView")
                
                
                let hobby = self.categoriesRowsFiltered[indexPath.row]
                
                if hobby.aType == "child" {
                    
                }else{
                    print("isMainThread: \(Thread.isMainThread)")
                    self.currentParent = hobby.copy() as? Hobby
                    //                    self.getHobbiesByLayer(hobby: hobby)
                    self.categoriesRows = self.appUserHobbyTable.getHobbiesByLayers(hobbyID: hobby.HobbyID)
                    self.categoriesRowsFiltered = self.categoriesRows
                    
                    
                }
            default:
                print("default")
            }
            print("aaaasdadsasd")
            
            DispatchQueue.main.async {
                print("isMainThread 2: \(Thread.isMainThread)")

                self.categoriesTableView.reloadData()

                
            }
        }
        
    }
    
    func updateButton(hobby : Hobby?, isEnabled : Bool){
        
        if let tempHobby = hobby {
            btToTheParent.setTitle(tempHobby.Name, for: .normal)

        }else{
            btToTheParent.setTitle("Categories", for: .normal)
        }
        
//        btToTheParent.isEnabled = isEnabled
//
//        var image = UIImage(named: "left arrow")
//        image = image?.scaleImageToSize(newSize: CGSize(width: 30, height: 30))
//        btToTheParent.setImage(image, for: .normal)
    }
    

    
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    @IBAction func tfHobbiesFilterEditingDidEnd(_ sender: UITextField) {
        print("tfHobbiesFilterEditingDidEnd")
    }
    
    @IBAction func btGoToParent(_ sender: UIButton) {
        print("btGoToParent")
        
        if let tempCurrentParent = currentParent {
            
            print(tempCurrentParent.toString())
            
            if tempCurrentParent.ParentID == -1 {
                print("if tempCurrentParent.ParentID == -1")
                getParentNode()
                btToTheParent.isEnabled = false
                btToTheParent.setTitle("Hobbies", for: .disabled)
                currentParent = nil
            }else{
                print("if tempCurrentParent.ParentID == -1 else")

                getHobbiesByLayer(hobby : tempCurrentParent)
                
                btToTheParent.isEnabled = true
                btToTheParent.setTitle("Hobbies", for: .normal)
                
                if let tempFoundHobby = hobbyTable.selectBy(hobbyID: tempCurrentParent.HobbyID){
                    currentParent = tempFoundHobby
                }
            }
        }else{
            
        }
    }
    
    
    func getParentNode(){
        categoriesRows = appUserHobbyTable.getParentNodes()
        categoriesRowsFiltered = categoriesRows
//        categoriesTableView.reloadData()
//        print(categoriesRowsFiltered.count)
    }
    
    func getHobbiesByLayer(hobby : Hobby){
        
        categoriesRows = appUserHobbyTable.getHobbiesByLayers(hobbyID: hobby.HobbyID)
        categoriesRowsFiltered = categoriesRows
    }
    
}


