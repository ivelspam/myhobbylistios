import UIKit

class RegisterHobbyVC: UIViewController {

    let dispatchGroup = DispatchGroup()
    @IBOutlet weak var parentView: UIView!
    @IBOutlet var scOptions: UISegmentedControl!
    let myHobbiesSubviewVC = MyHobbiesSubviewVC();
    let categorySubviewVC = CategorySubviewVC();
    
    var subviews = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myHobbiesSubviewVC.customInit(myHobbiesProtocol: self)
        subviews.append(myHobbiesSubviewVC)
        
        categorySubviewVC.customInit(myHobbiesProtocol: self)
        subviews.append(categorySubviewVC)
        
        for subview in subviews {
            parentView.addSubview(subview.view)
            subview.view.frame = parentView.bounds
        }
        parentView.bringSubview(toFront: subviews[0].view)
    }
    
    @IBAction func scOptions(_ sender: Any) {
        switch scOptions.selectedSegmentIndex {
        case 0:
            parentView.bringSubview(toFront: subviews[0].view)
        case 1:
            parentView.bringSubview(toFront: subviews[1].view)
        default:
            print("error occurs")
        }
    }
    
    func updateHobby(hobbyID: Int, inHobby: Bool, option: String) {
        
        if("categories" == option){
            myHobbiesSubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
        }else if("list" == option){
            categorySubviewVC.updateItemInList(hobbyID: hobbyID, inHobby: inHobby)
        }
    }


}
