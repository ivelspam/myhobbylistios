import UIKit

class CreateEventVC: UIViewController{
    
    var eventsVC : EventsVC?
    
    let appUserHobbyTable = Motherbase().getAppUserHobbyTable()
    let listOfReach : [PickerRow] = [PickerRow(title: "Everyone", id: 0),
                                     PickerRow(title: "Friends", id: 1),
                                     PickerRow(title: "Friends and Theirs Friends", id : 2)]
    var listofHobbies = [Hobby]()

    @IBOutlet weak var eventNameTF: TextFieldForm!
    @IBOutlet weak var addressTF: TextFieldAutoAddress!
    @IBOutlet weak var eventPlaceNameTF: TextFieldForm!
    @IBOutlet weak var hobbyTF: TextFieldPicker!
    @IBOutlet weak var reachTF: TextFieldPicker!
    @IBOutlet weak var informationTV: TextViewForm!
    @IBOutlet weak var startEndTimeView: UIView!
    
    let informationTVPlaceHolder = "Extra Information"
    
    var searchController : UISearchController?
    var resultView : UITextView?
    
    let startTimePicker = UIDatePicker()
    let endTimePicker = UIDatePicker()
    
    var startEndTimeCVC = StartEndTimeCVC()
    
    
    @IBOutlet weak var errorsTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listofHobbies = appUserHobbyTable.getHobbiesWithInfo()
        print("List of Hobbies")
        print(listofHobbies)
        
        var pickerRows = [PickerRow]()
        
        print("Hobby Names")
        
        for hobby in listofHobbies {
            pickerRows.append(PickerRow(title: hobby.Name, id: hobby.HobbyID))
        }
        eventNameTF.customInit(topAnchorView: nil, textFieldType: .EventName)
        addressTF.customInit(topArchorView: eventNameTF, parentVC: self)
        eventPlaceNameTF.customInit(topAnchorView: addressTF, textFieldType: .PlaceName)
        hobbyTF.customInit(topArchorView: eventPlaceNameTF, parentVC: self, pickerRows: pickerRows, fieldType: .EventType)
        reachTF.customInit(topArchorView: hobbyTF, parentVC: self, pickerRows: listOfReach, fieldType: .Reach)
        informationTV.customInit(parentVC: self)
        
        startEndTimeView.addSubview(startEndTimeCVC.view)
        startEndTimeCVC.customInit(topArchorView: reachTF, parentVC: self)

    }
    
    func checkFormIsValid(){
        
        var valid = true
        
        if(!eventNameTF.checkIfIsValid()){ valid = false }
        if(!addressTF.checkIfIsValid()){ valid = false }
        if(!eventPlaceNameTF.checkIfIsValid()){ valid = false }
        if(!hobbyTF.checkIfIsValid()){ valid = false }
        if(!reachTF.checkIfIsValid()){ valid = false }
        if(!startEndTimeCVC.checkIfIsValid()){ valid = false }
        if(valid){
            sendEventToServer()
        }
    }

    
    @IBAction func createEvent(_ sender: Any) {
        checkFormIsValid()
    }
    
    func sendEventToServer(){
        print("sendEventToServer")
        
        ServerRequest.postRequest(request: Event_register(            address: addressTF.place!.formattedAddress!,
                                                                      description: informationTV.getText(),
                                                                      endDatetime: startEndTimeCVC.endTime!,
                                                                      hobbyID: hobbyTF.getSelectedID(),
                                                                      latitude: addressTF.place!.coordinate.latitude,
                                                                      longitude: addressTF.place!.coordinate.longitude,
                                                                      name: eventNameTF.text!,
                                                                      placeName: eventPlaceNameTF.text!,
                                                                      reach: Event.ReachOptions(rawValue: reachTF.getSelectedID())!,
                                                                      startDatetime: startEndTimeCVC.startTime!))
        { (response, worked) in
            if(response["message"].stringValue == "event created"){
                self.navigationController?.popViewController(animated: true)

                Motherbase().getEventTable().insertOrIgnoreEvent(fromMySQL: response["event"])

                let event = Motherbase().getEventTable().selectEventBy(eventID: response["event"]["EventID"].intValue)

                if let temp = event{
                    self.eventsVC!.performSegueShowEventInfo(event: temp)
                }
            }
        }
    }
}

