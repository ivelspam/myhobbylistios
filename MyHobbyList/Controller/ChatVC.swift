import UIKit
import SwiftyJSON
import SocketIO

class ChatVC: UIViewController, ChatSocketDelegate, sendMessageVCDelegate{

    

    @IBOutlet weak var chatMessageTV: UITableView!
    @IBOutlet weak var messageView: UIView!

    var chatSocketIO : ChatSocketIO?
    
    var delegate : UpdateFriendsDelegate?
    
    var AppUserID : Int?
    
    var chatMessages : [Chat] = []
    var chatTable = Motherbase().getChatTable()
    
    var friend : Friend?
    @IBOutlet weak var heightContraint: NSLayoutConstraint!

    @IBOutlet weak var bottomKeyboardConstraint: NSLayoutConstraint!
    var sendMessageChildVC = SendMessageChildVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        chatMessageTV.addGestureRecognizer(tapGesture)
        
        AppUserID = UserDefaults.standard.integer(forKey: DefaultKey.AppUserID_int.rawValue)
        sendMessageChildVC.customInit(viewController: self, superview: messageView, chatMessageVCDelegate: self, bottomKeyboardConstraint : bottomKeyboardConstraint)
        buildSocketIO()
        buildTable()
    }
    
    @objc func tableViewTapped(){
        print("tableViewTapped")
        sendMessageChildVC.endEditing()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        chatSocketIO!.disconnect()
        sendMessageChildVC.removeObserver()
        
    }
    
    func receivedMessageFromSocketIO(chat: Chat) {
        chatMessages.append(chat)
        chatTable.insertOrReplace(chat: chat)
        chatMessageTV.reloadData()
    }
    
    func getNewMessages(){
        
        ServerRequest.getRequest(request : Chat_getNewFromFriend(appUserID : friend!.AppUserID)) { (response, worked) in
            let jsonValues = JSON(response)
            
            if(jsonValues["message"].stringValue == Chat_getNewFromFriend.Response.foundNewMessages){
                self.chatTable.insertOrReplace(chatsFromMySQL: jsonValues["chatMessages"])
                
                self.chatMessages = self.chatTable.getFriendChatMessages(owner: self.AppUserID!, receiver: self.friend!.AppUserID, limit : 15)
                self.chatMessageTV.reloadData()
            }else{
                print("Nothing found")
            }
        }
    }
    
   
    func sendMessage(message: String) {
        let preChat = Chat(receiver: friend!.AppUserID, message: message)
        chatMessages.append(preChat)
        chatTable.insertOrReplace(chat: preChat)
        chatMessageTV.reloadData()
        chatSocketIO?.sendMessage(chatMessage: preChat){
            worked, result in
            let chat = Chat(fromMySQLChatMessageJSON: result)
            self.chatTable.insertOrReplace(chat: chat)
        }
    }
}

extension ChatVC {
    
    func buildSocketIO(){
        chatSocketIO = ChatSocketIO(friendAppUserID: friend!.AppUserID, chatSocketDelegate: self)
    }
    
}


extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    
    func buildTable(){
        
        chatMessageTV.allowsSelection = false
        
        //Give Delegates
        chatMessageTV.delegate = self
        chatMessageTV.dataSource = self
        
        //Rotate Table
        
        chatMessageTV.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
        chatMessages = chatTable.getFriendChatMessages(owner: AppUserID!, receiver: friend!.AppUserID, limit : 15)
        
        //Give Table Variable Height
        chatMessageTV.estimatedRowHeight = 80
        chatMessageTV.rowHeight = UITableViewAutomaticDimension
        chatMessageTV.separatorStyle = .none
        
        
        //Register Nibs
        chatMessageTV.register(UINib(nibName: "MessageReceivedRow", bundle: nil), forCellReuseIdentifier: "MessageReceivedRow")
        chatMessageTV.register(UINib(nibName: "MessageSentRow", bundle: nil), forCellReuseIdentifier: "MessageSentRow")
        
        //Get Data
        
        getNewMessages()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(chatMessages.count)
        
        return chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowIndex = chatMessages.count - 1 - indexPath.row
        let row = chatMessages[rowIndex]
        
        if(AppUserID == row.Owner){
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSentRow") as! MessageSentRow
            cell.customInit(chatMessage: row)
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReceivedRow") as! MessageReceivedRow
        cell.customInit(chatMessage: row, friend : friend!)
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
        return cell
    }
    
    func updateTableContentInset() {
        let numRows = tableView(self.chatMessageTV, numberOfRowsInSection: 0)
        var contentInsetTop = self.chatMessageTV.bounds.size.height
        for i in 0..<numRows {
            let rowRect = self.chatMessageTV.rectForRow(at: IndexPath(item: i, section: 0))
            contentInsetTop -= rowRect.size.height
            if contentInsetTop <= 0 {
                contentInsetTop = 0
            }
        }
        self.chatMessageTV.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)
    }
    
}
