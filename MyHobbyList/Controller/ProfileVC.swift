import UIKit
import Alamofire
import SwiftyJSON
import MobileCoreServices

protocol ChangeProfileNameDelegate: class {
    func changeName()
    func changeUsername()
    func changeDistance()
}

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource{

    let profileMenu = ProfileMenu()
    
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var userInfoTableView: UITableView!
    private var imagePicker : UIImagePickerController!
    
    override func viewDidLoad() {
        userInfoTableView.delegate = self
        userInfoTableView.dataSource = self
        
        Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: UserDefaults.standard.integer(forKey: DefaultKey.AppUserID_int.rawValue), imageVersion: UserDefaults.standard.integer(forKey: DefaultKey.ImageVersion_int.rawValue)) { (image) in
             self.ivProfile.image = image
        }
        
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = userInfoTableView.dequeueReusableCell(withIdentifier: "customCell") as! CustomTableViewCell
       
        cell.menuItemImage.image = UIImage(named: profileMenu.rows[indexPath.row].image)
        cell.menuNameLabel.text = profileMenu.rows[indexPath.row].profileMenuCat.rawValue
        cell.menuValueLabel.text = profileMenu.rows[indexPath.row].getValue()
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        profileMenu.rows[indexPath.row].runSegue(profileViewController: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueName.ShowChangeDistance.rawValue{
            let destination = segue.destination as? ChangeDistanceModalVC
            destination!.delegate = self
        }else if segue.identifier == SegueName.ShowEditName.rawValue{
            let destination = segue.destination as? ChangeNameModalVC
            destination!.delegate = self
        }else if segue.identifier == SegueName.ShowEditUsername.rawValue{
            let destination = segue.destination as? ChangeUsernameModalVC
            destination!.delegate = self
        }
    }
    
    @IBAction func uploadAImage(_ sender: Any) {
        takePicture()
    }
}


extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func takePicture(){
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .photoLibrary
//            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
        }
        
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: imagePicker.sourceType)!
        present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        if mediaType == (kUTTypeImage as String) {
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                
                let data = UIImageJPEGRepresentation(pickedImage, 1.0)!
                

                    let directoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                
                    let filename = directoryPath.appendingPathComponent("profile.jpg")
                    try? data.write(to: filename)
                
//                do {
//
//                }catch{
//
//                }
                
                
                
                ivProfile.image = pickedImage
                
                ServerRequest.uploadProfilePictureToServer(uiImage: pickedImage) { (worked) in
                    print("Aquiiii")
                }
            }
          
        }else{
            // a video was taken
            
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProfileVC: ChangeProfileNameDelegate {

    func changeName() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Name), section : 0)
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
    
    func changeUsername() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Username), section : 0)
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
    
    func changeDistance() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Distance), section : 0)
        
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
}
