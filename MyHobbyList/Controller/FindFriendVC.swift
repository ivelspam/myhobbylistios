import UIKit


class FindFriendVC: UIViewController {

    var delegate : UpdateFriendsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var appUserID = -1
    
    @IBOutlet weak var foundView: UIView!
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBAction func findUser(_ sender: UIButton) {
        
        ServerRequest.getRequest(request : Appuser_findAUserByUsername(username: username.text!)) { (response, worked) in
            print(response)
            if(response["message"].stringValue == "user found"){
                print("found")
                self.appUserID = response["user"]["AppUserID"].intValue
                self.foundView.isHidden = false
                self.name.text = response["user"]["Name"].stringValue
                Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: self.appUserID, imageVersion: response["user"]["ImageVersion"].intValue, completion: { (image) in
                    self.img.image = image
                })

            }else{
                self.foundView.isHidden = true
                self.name.text = "User Not Found"
                print("not found")
            }
        }
    }
    
    @IBAction func addFriend(_ sender: UIButton) {
        print("addFriend")
        
        ServerRequest.postRequest(request: Friend_sendRequest(appUserID: appUserID)) { (response, worked) in
            print(response)
            if(response["message"].stringValue == "user added"){
                print("found")
                
                let friend = Friend(friendJSONFromMySQL: response["friendInfo"])
                print(Thread.isMainThread)
                self.delegate?.update(friend: friend, updateFriendDelegateOption: UpdateFriendsDelegateOption.sendSentRequest)
                Motherbase().getFriendTable().insertOrReplace(friendFromMySQL: response["friendInfo"])
            }else{
                
            }
        }
    }
}
