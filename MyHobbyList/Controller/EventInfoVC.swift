import UIKit
import MapKit
import CoreLocation
import SwiftyJSON

class EventInfoVC: UIViewController {
    enum EventInfoVCControllerEnum : Int{
        case info = 0
        case discussion = 1
    }
    
    var event : Event?
    var eventComments = [EventComment]()
    var eventMessageSocketIO : EventMessageSocketIO?
    var subviews = [UIViewController]()
    @IBOutlet weak var changeStateBT: UIBarButtonItem!
    @IBOutlet weak var contentView: UIView!

    @IBAction func changeState(_ sender: Any) {
        if(UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue) == event?.AppUserID){
            if event!.State.rawValue == 0 {
                hideEventRequest()
            }else if event!.State.rawValue == 1{
                closeEventRequest()
                
            }
        }else{
            
            if UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue) == event?.EventID{
                leaveEventRequest()
            }else{
                joinEventRequest()
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createSubViews()
        changeBTText()
    }
    
    func changeBTText(){
        
        if event!.State.rawValue < 2 {
            if UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue) == event?.AppUserID{
                if event!.State.rawValue == 0 {
                    changeStateBT.title = "Hide Event"
                }else if event!.State.rawValue == 1{
                    changeStateBT.title = "Close Event"
                }
            }else{
                if UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue) == event?.EventID{
                    changeStateBT.title = "Leave Event"
                }else{
                    changeStateBT.title = "Join Event"
                }
            }
            
        }else{
            changeStateBT.title = "Ended"

            changeStateBT.isEnabled = false
        }
    }
    
    @IBAction func changeSubView(_ sender: Any) {
        
    }
    
    func createSubViews(){
       print("createSubViews")
        subviews.append(EventDataChildVC())
        (subviews[EventInfoVCControllerEnum.info.rawValue] as! EventDataChildVC).customInit(event: event!)
        
        subviews.append(EventCommentChildVC())
        (subviews[EventInfoVCControllerEnum.discussion.rawValue] as! EventCommentChildVC).customInit(event: event!, parentVC: self)

        for subview in subviews{
            let view = subview.view
            
            contentView.addSubview(subview.view)

            view?.frame = contentView.bounds
        }
        contentView.bringSubview(toFront: subviews[0].view)
        
    }
    
    @IBAction func switchViewsAction(_ sender: UISegmentedControl) {
        contentView.bringSubview(toFront: subviews[sender.selectedSegmentIndex].view)
    }
    
    func hideEventRequest(){
        ServerRequest.postRequest(request : Event_hide(eventID: event!.EventID)) { (response, worked) in

            if response["message"].string == Event_hide.Response.hidedEvent {
                self.event!.State = Event.EventState.Hided
                Motherbase().getEventTable().updateState(from: self.event!.EventID , state: 1)
                self.changeBTText()

            }
        }
    }
    
    func closeEventRequest(){
        ServerRequest.postRequest(request: Event_close(eventID: event!.EventID)) { (response, worked) in
            if response["message"].string == Event_close.Response.closedEvent {
                self.event!.State = Event.EventState.Ended
                Motherbase().getEventTable().updateState(from: self.event!.EventID , state: 2)
                UserDefaults.standard.set(-1, forKey: DefaultKey.EventID_int.rawValue)
                self.changeBTText()

            }
        }
    }
    
    func leaveEventRequest(){
        ServerRequest.postRequest(request: Event_leave(eventID: event!.EventID)) { (response, worked) in
            if response["message"].string == Event_leave.Response.leaveEvent {
                UserDefaults.standard.set(-1, forKey: DefaultKey.EventID_int.rawValue)
                self.changeBTText()
            }
        }
    }
    
    func joinEventRequest(){
        ServerRequest.postRequest(request: Event_join(eventID: event!.EventID)) { (response, worked) in
            if response["message"].string == Event_join.Response.joinedEvent {
                self.event!.State = Event.EventState.Hided
                UserDefaults.standard.set(self.event!.EventID, forKey: DefaultKey.EventID_int.rawValue)
                
                self.changeBTText()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("EventCommentChildVC: viewDidAppear")
        (subviews[1] as! EventCommentChildVC).createSocket()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print("EventInfoVC: viewDidDisappear")
        (subviews[1] as! EventCommentChildVC).endSocket()
    }
    
}
