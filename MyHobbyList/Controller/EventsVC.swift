import UIKit
import Alamofire
import SwiftyJSON
import Foundation

class EventsVC: UIViewController, EventsTVConDelegate {

    @IBOutlet weak var segmentsView: UIView!
    
    let eventTable = Motherbase().getEventTable()
    var events = [Event]()
    @IBOutlet weak var createOrShowEvent: UIBarButtonItem!
    var updateViewTimer : Timer?
    var selectedState = 0
    var mClientUpdatedTS = MagicNumbers.LOWEST_TS
    
    var mCurrentEvents = EventsTVCon()
    var mPastEvents = EventsTVCon()
    
    var updateTimer : Timer?

    
    @IBOutlet weak var eventTypeSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        print("EventsVC: viewDidLoad")
        super.viewDidLoad()
        
        mCurrentEvents.customInit(eventState: [Event.EventState.Created], eventsTVConDelegate: self)
        mPastEvents.customInit(eventState: [Event.EventState.Ended, Event.EventState.Ended], eventsTVConDelegate: self)

        segmentsView.addSubview(mCurrentEvents.view)
        segmentsView.addSubview(mPastEvents.view)

        mCurrentEvents.view.frame = segmentsView.bounds
        mPastEvents.view.frame = segmentsView.bounds
        
        segmentsView.bringSubview(toFront:  mCurrentEvents.view)
        startUpdateTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("EventsVC: viewWillAppear")
        super.viewWillAppear(true)
    }
    
    @IBAction func changeEventType(_ sender: UISegmentedControl) {
        selectedState = sender.selectedSegmentIndex
        
        switch sender.selectedSegmentIndex {
            case 0:
                segmentsView.bringSubview(toFront: mCurrentEvents.view)
            case 1:
                segmentsView.bringSubview(toFront: mPastEvents.view)
        default:
            print("Not Suppose to be here")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("EventsVC: viewDidAppear")
        updateViewTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(updateView), userInfo: nil, repeats: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        print("EventsVC: viewWillDisappear")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print("EventsVC: viewDidDisappear")
        updateViewTimer?.invalidate()
    }
    
    func performTheSegue(event: Event) {
        performSegue(withIdentifier: SegueName.ShowEventInfo.rawValue, sender: event)
    }
}

//segues
extension EventsVC {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueName.OpenCreateEvent.rawValue {
            print(SegueName.OpenCreateEvent.rawValue)
            if let destination = segue.destination as? CreateEventVC {
                destination.eventsVC = self
            }
        }
        
        if segue.identifier == SegueName.ShowEventInfo.rawValue {
            print(SegueName.ShowEventInfo.rawValue)
            
            if let destination = segue.destination as? EventInfoVC {
                destination.event = sender as? Event
            }
        }
    }
    
    func performSegueShowEventInfo(event : Event) {
        self.perform(#selector(performSegueShowEventInfoHandler), with:["Event" : event], afterDelay: 0.4)
    }
    
    @objc func performSegueShowEventInfoHandler(_ params : [String: Any]){
        print("performSegueShowEventInfoHandler")
        performSegue(withIdentifier: SegueName.ShowEventInfo.rawValue, sender: params["Event"] as! Event)
    }
    
}

//events
extension EventsVC {
    
    @IBAction func openCreateEvent(_ sender: Any) {
        let eventID = UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue)
        if eventID > 0 {
            if let event = eventTable.selectEventBy(eventID: UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue)) {
                performSegue(withIdentifier: SegueName.ShowEventInfo.rawValue, sender: event)
            }else{
                print("ERROR: SHOULD HAVE FOUND A EVENT")
            }
        }else{
            performSegue(withIdentifier: SegueName.OpenCreateEvent.rawValue, sender: nil)
        }
    }
    
    func checkIfIsInEvent(){
        if UserDefaults.standard.integer(forKey: DefaultKey.EventID_int.rawValue) > 0 {
            createOrShowEvent.title = "Show Joined Event"
        }else{
            createOrShowEvent.title = "Create Event"
        }
    }
}

//timer updates
extension EventsVC {
    @objc func updateView()
    {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {

            }
        }
    }
}

//UPDATE TIMER
extension EventsVC {
    
    func startUpdateTimer(){
        updateTimer = Timer.scheduledTimer(timeInterval: RepTimers.UPDATE_EVENTS, target: self, selector: #selector(getNewEvents), userInfo: nil, repeats: true)
        getNewEvents()
    }
    
    @objc func getNewEvents(){
        
        
        print("@objc func getNewEvents()")
        
        
        print(eventTable.toString())
        
        DispatchQueue.main.async {
            
            let events = self.eventTable.selectEventsBy(clientUpdatedTS: self.mClientUpdatedTS)
            
            if events.count > 0 {
               
                for event in events {
                    if self.mClientUpdatedTS < event.ClientUpdatedTS {
                        self.mClientUpdatedTS = event.ClientUpdatedTS
                    }
                    self.mCurrentEvents.change(event: event)
                    self.mPastEvents.change(event: event)
                }
                
                DispatchQueue.main.async {
                    self.mCurrentEvents.makeSorted()
                    self.mPastEvents.makeSorted()
                }
            }
        }
    }
}



