import UIKit
import Alamofire
import SwiftyJSON

class EmailRegisterViewController: UIViewController {
    
   
    @IBOutlet weak var tvName: UITextField!
    @IBOutlet weak var tvEmail: UITextField!
    @IBOutlet weak var tvPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btCreateAccount(_ sender: Any) {
        Alamofire.request("https://synchobby.com/api/registration/email/adduser", method: .post, parameters: ["Name" : tvName.text!, "Email" : tvEmail.text!, "Password" : tvPassword.text!])
            .responseJSON{response in
                print(response.result)
        }
    }
    
    @IBAction func btGoToLoginScreen(_ sender: Any) {
        print("go back")
        dismiss(animated: true, completion: nil)
        
    }
    
}



