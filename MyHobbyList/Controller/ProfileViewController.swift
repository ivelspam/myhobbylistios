import UIKit
import Alamofire
import SwiftyJSON



class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ChangeProfileNameDelegate{

    let profileMenu = ProfileMenu()
    
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var userInfoTableView: UITableView!
    
    override func viewDidLoad() {
        userInfoTableView.delegate = self
        userInfoTableView.dataSource = self
        
        if let image = Utils.checkIfImageExists(fileName : "profile.jpg"){
            ivProfile.image = image
        }

        super.viewDidLoad()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = userInfoTableView.dequeueReusableCell(withIdentifier: "customCell") as! CustomTableViewCell
       
        cell.menuItemImage.image = UIImage(named: profileMenu.rows[indexPath.row].image)
        cell.menuNameLabel.text = profileMenu.rows[indexPath.row].profileMenuCat.rawValue
        cell.menuValueLabel.text = profileMenu.rows[indexPath.row].getValue()
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(profileMenu.rows[indexPath.row].profileMenuCat.rawValue)
//        print(profileMenu.rows[indexPath.row].segueName.rawValue)
//
//        performSegue(withIdentifier: profileMenu.rows[indexPath.row].segueName.rawValue, sender: self)
        profileMenu.rows[indexPath.row].runSegue(profileViewController: self)
  }
    
    func changeName() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Name), section : 0)
  
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
    
    func changeUsername() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Username), section : 0)
        
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
    
    func changeDistance() {
        let indexPath = IndexPath(item : profileMenu.getRowPosition(profileMenuCat: ProfileMenu.ProfileMenuCat.Distance), section : 0)
        
        userInfoTableView.reloadRows(at: [indexPath], with: .top)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChangeNameViewController {
            destination.delegate = self
           
            if segue.identifier == SegueName.EditNameSegue.rawValue{
                if let profileMenuCat = sender as? ProfileMenu.ProfileMenuCat{
                    destination.profileMenuCat = profileMenuCat
                }
            }
        }
        
    }
    

    
}
