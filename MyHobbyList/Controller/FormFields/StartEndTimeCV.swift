//
//  StartEndTimes.swift
//  MyHobbyList
//
//  Created by Nicholas Homolka on 7/7/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import GooglePlaces
import UIKit

class StartEndTimeCVC: UIViewController {
    
    enum FieldError: String {
        case Empty = "&&& Can't Be Empty"
    }
    
    enum ErrorType: String {
        case AddressIsEmpty
        case OverFourHours
    }
    
    enum FieldType: String {
        case EventType = "Event Type"
        case Reach = "Reach"
        
        
    }

    @IBOutlet weak var endTimeTF: UITextField!
    @IBOutlet weak var startTimeTF: UITextField!
    
    
    
    var errorLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var startDP = UIDatePicker()
    var endDP = UIDatePicker()
    
    //views
    var parentVC : UIViewController?
    var topArchorView : UIView?
    
    func customInit(topArchorView : UIView?, parentVC : UIViewController){
        self.topArchorView = topArchorView
        self.parentVC = parentVC
        createStartDatePicker()
        donePressedStartDatePicker()
    }
    
   
    func showError(){
        
        let superview = self.view.superview!;
        
        superview.addSubview(errorLabel)
        errorLabel.backgroundColor = UIColor.red
        errorLabel.textAlignment = .right
        errorLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        
        if let topView = topArchorView {
            errorLabel.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        }else{
            errorLabel.topAnchor.constraint(equalTo: superview.topAnchor, constant: 2).isActive = true
        }
        self.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 2).isActive = true
        errorLabel.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
    }
    
    func removeError(){
        errorLabel.removeFromSuperview()
    }
    
    func checkIfIsValid() -> Bool{
        return !checkForErrors()
    }
    
    func checkForErrors() -> Bool{
        
        var fieldErrors = [FieldError]()
        
        if let fieldError = isLongerThanFourHours() {fieldErrors.append(fieldError)}
        if let fieldError = fieldIsEmpty() {fieldErrors.append(fieldError)}

        if(fieldErrors.count == 0){
            removeError()
            return false
        }
        
        showError()
        return true
        
    }
    
    func isLongerThanFourHours(){
        
        
        
    }
    

    
}

extension StartEndTimeCVC {
    
        func createStartDatePicker(){
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let minimunDate = Date().nearestTenMinutes()
            let maximunDate = Calendar.current.date(byAdding: .day, value: 1, to: minimunDate)?.nearestTenMinutes()
    
            startTimeTF.minimumDate = minimunDate
            startTimeTF.maximumDate = maximunDate
            startTimeTF.minuteInterval = 10
    
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedStartDatePicker))
            toolbar.setItems([done], animated: false)
            startTimeTF.inputAccessoryView = toolbar
            startTimeTF.inputView = startTimePicker
    
        }
    
        func createEndDatePicker(){
    
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let minimunDate = Date().nearestTenMinutes()
            var dateComponent = DateComponents()
            dateComponent.day = 1
            dateComponent.hour = 4
            let maximunDate = Calendar.current.date(byAdding: dateComponent, to: minimunDate)
    
            endTimeTF.minimumDate = minimunDate
            endTimeTF.maximumDate = maximunDate
            endTimeTF.minuteInterval = 10
    
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedEndDatePicker))
            toolbar.setItems([done], animated: false)
            endTimeTF.inputAccessoryView = toolbar
            endTimeTF.inputView = endTimeTF
    
        }
    
        @objc func donePressedStartDatePicker(){
    
            let formatter = DateFormatter()
    
            formatter.dateFormat = "E, hh:mm a"
            let dateString = formatter.string(from: startTimePicker.date)
            startTimeTF.text = "\(dateString)"
            event.StartDatetime = startTimePicker.date
    
            self.view.endEditing(true)
        }
    //
    //    @objc func donePressedEndDatePicker(){
    //
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "E, hh:mm a"
    //
    //        let dateString = formatter.string(from: endTimePicker.date)
    //        endTimeTF.text = "\(dateString)"
    //        event.EndDatetime = endTimePicker.date
    //        self.view.endEditing(true)
    //    }
}
