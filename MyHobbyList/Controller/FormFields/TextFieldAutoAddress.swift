import GooglePlaces
import UIKit

class TextFieldAutoAddress: UITextField {
    
    enum FieldError: String {
        case Empty = "Address is Empty"
    }
    
    enum ErrorType: String {
        case AddressIsEmpty
    }
    
    var errorLabel = FormErrorLabel()

    //views
    var parentVC : UIViewController?
    var topArchorView : UIView?
    
    var place : GMSPlace?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func customInit(topArchorView : UIView?, parentVC : UIViewController){
        self.topArchorView = topArchorView
        self.parentVC = parentVC
        
        errorLabel.customInit(topView: self.superview!, topAnchorView: topArchorView, siblingView: self)
        
        self.addTarget(self, action: #selector(openGoogleAddressAutoComplete), for: UIControlEvents.editingDidBegin)
    }
    
    @objc func openGoogleAddressAutoComplete(){
        print("Aquiiiiiiiiiiiiii")
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        parentVC!.present(autocompleteController, animated: true, completion: nil)
    }
    
    func updateValue(place : GMSPlace){
        self.text = place.formattedAddress
        self.place = place
    }
    
    func checkIfIsValid() -> Bool{
       return !checkForErrors()
    }
    
    func checkForErrors() -> Bool{
        var fieldErrors = [FieldError]()
        if let error = fieldIsEmpty() {fieldErrors.append(error)}

        if(fieldErrors.count == 0){
            errorLabel.removeFromSuperview()
            return false
        }
        
        errorLabel.text = fieldErrors[0].rawValue
        errorLabel.showError()
        return true
    }
}

extension TextFieldAutoAddress{
    
    func fieldIsEmpty() -> FieldError? {
        print("fieldIsEmpty")
        if place == nil {
            return FieldError.Empty
        }else{
            return nil
        }
    }
}

extension TextFieldAutoAddress: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        updateValue(place: place)
        parentVC!.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        parentVC!.dismiss(animated: true, completion: nil)
    }

    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

