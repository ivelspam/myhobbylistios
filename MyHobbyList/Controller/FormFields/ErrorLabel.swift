import UIKit

class FormErrorLabel : UILabel {
   
    var topView : UIView?
    var topAnchorView : UIView?
    var siblingView : UIView?
    
    func customInit(topView : UIView, topAnchorView : UIView?, siblingView : UIView){
        
        self.topView = topView
        self.topAnchorView = topAnchorView
        self.siblingView = siblingView
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.red
        self.textAlignment = .right
    }
    
    func showError(){
        
        topView?.addSubview(self)
        self.widthAnchor.constraint(equalTo: siblingView!.widthAnchor, multiplier: 1).isActive = true
        
        if let topViewTemp = topAnchorView {
            self.topAnchor.constraint(equalTo: topViewTemp.bottomAnchor, constant: 2).isActive = true
        }else{
            self.topAnchor.constraint(equalTo: topView!.topAnchor, constant: 2).isActive = true
        }
        siblingView!.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 2).isActive = true
        self.centerXAnchor.constraint(equalTo: topView!.centerXAnchor).isActive = true
        
    }
    
}


