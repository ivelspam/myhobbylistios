import GooglePlaces
import UIKit

class StartEndTimeCVC: UIViewController {
    
    enum FieldError: String {
        case OverFourHours = "Event can't be longer than four hours"
        case StartGreaterThanEnd = "Start Time Greater Than End Time"
        case StartEndSame = "Start and End can't be the same"

    }
    
    enum ErrorType: String {
        case AddressIsEmpty
        case OverFourHours
    }
    
    enum FieldType: String {
        case EventType = "Event Type"
        case Reach = "Reach"
    }
    
    let minimunDate = Date(nearestMinuteRoundUp: 10)
    @IBOutlet weak var startTimeTF: UITextField!
    @IBOutlet weak var endTimeTF: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        createStartDatePicker()
        createEndDatePicker()
        donePressedStartDatePicker()
        donePressedEndDatePicker()
    }
    
    
    var errorLabel = FormErrorLabel()
    
    var startDP = UIDatePicker()
    var endDP = UIDatePicker()
    
    //views
    var parentVC : UIViewController?
    var topArchorView : UIView?
    
    func customInit(topArchorView : UIView?, parentVC : UIViewController){
        self.topArchorView = topArchorView
        self.parentVC = parentVC
        errorLabel.customInit(topView: self.view.superview!.superview!, topAnchorView: topArchorView, siblingView: self.view.superview!)
        

    }
    
    var startTime : Date?
    var endTime : Date?
   
    func checkIfIsValid() -> Bool{
        return !checkForErrors()
    }
    
    func checkForErrors() -> Bool{
        
        var fieldErrors = [FieldError]()
        
        if let fieldError = isLongerThanFourHours() {fieldErrors.append(fieldError)}
        if let fieldError = startThanGreaterThanEndTime() {fieldErrors.append(fieldError)}
        if let fieldError = StartEndSame() {fieldErrors.append(fieldError)}
        
        if(fieldErrors.count == 0){
            errorLabel.removeFromSuperview()
            return false
        }
        
        errorLabel.text = fieldErrors[0].rawValue
        errorLabel.showError()
        return true
    }
    
    func isLongerThanFourHours() -> FieldError?{
        let minutes = startTime!.minutes(from: endTime!)
        
        if(minutes > 240){
            return FieldError.OverFourHours
        }
        return nil
    }
    
    func startThanGreaterThanEndTime() -> FieldError?{
        let minutes = endTime!.minutes(from: startTime!)
        
        print(minutes)
        
        if(minutes < 0){
            return FieldError.StartGreaterThanEnd
        }
        return nil
    }
    
    func StartEndSame() -> FieldError?{
        let minutes = startTime!.minutes(from: endTime!)
        
        if(minutes == 0){
            return FieldError.StartEndSame
        }
        return nil
    }
}

extension StartEndTimeCVC {
    
        func createStartDatePicker(){
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let maximunDate = Calendar.current.date(byAdding: .day, value: 1, to: minimunDate)
    
            startDP.minimumDate = minimunDate
            startDP.maximumDate = maximunDate
            startDP.minuteInterval = 10
    
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedStartDatePicker))
            toolbar.setItems([done], animated: false)
            startTimeTF.inputAccessoryView = toolbar
            startTimeTF.inputView = startDP
    
        }
    
        func createEndDatePicker(){
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var dateComponent = DateComponents()
            dateComponent.day = 1
            dateComponent.hour = 4
            let maximunDate = Calendar.current.date(byAdding: dateComponent, to: minimunDate)
            endDP.minimumDate = minimunDate
            endDP.maximumDate = maximunDate
            endDP.minuteInterval = 10
            
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedEndDatePicker))
            toolbar.setItems([done], animated: false)
            endTimeTF.inputAccessoryView = toolbar
            endTimeTF.inputView = endDP
    
        }
    
        @objc func donePressedStartDatePicker(){
    
            let formatter = DateFormatter()
    
            formatter.dateFormat = "E, hh:mm a"
            let dateString = formatter.string(from: startDP.date)
            startTimeTF.text = "\(dateString)"
            startTime = startDP.date.getTimeWithoutSeconds()
            self.view.endEditing(true)
        }
    
        @objc func donePressedEndDatePicker(){
    
            let formatter = DateFormatter()
            formatter.dateFormat = "E, hh:mm a"
    
            let dateString = formatter.string(from: endDP.date)
            endTimeTF.text = "\(dateString)"
            endTime = endDP.date.getTimeWithoutSeconds()

            self.view.endEditing(true)
            
        }
}
