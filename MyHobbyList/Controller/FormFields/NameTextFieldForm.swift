import UIKit

class TextFieldForm: UITextField {
    
    enum TextFieldTypes {
        case Email
        case Name
        case Password
    }
    
    enum FieldErrors : String {
        case EmailIsEmpty = "Email is Empty"
        case EmailIsWrongFormat = "Email is Wrong Format"
        case NameIsEmpty = "Name is Empty"
        case PasswordIsEmpty = "Password is Empty"
    }

    var errorLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var textFieldType : TextFieldTypes?
    var highestLevelError : FieldErrors?
    
    
    var parentView : UIView?
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func customInit(parentView : UIView, textFieldType : TextFieldTypes){
        self.parentView = parentView
        self.textFieldType = textFieldType
    }
    
    func showError(){
        
        let superview = self.superview!;
        
        superview.addSubview(errorLabel)
        errorLabel.text = highestLevelError?.rawValue
        errorLabel.backgroundColor = UIColor.red
        errorLabel.textAlignment = .right
        errorLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        errorLabel.topAnchor.constraint(equalTo: parentView!.topAnchor, constant: 2).isActive = true
        self.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 2).isActive = true
        errorLabel.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
    }
    
    func removeError(){
        errorLabel.removeFromSuperview()
    }
    
    func check() -> Bool{
        
        print("Check")
        switch textFieldType! {
            case .Email:
                return checkEmail()
            case .Name:
                return checkName()
            default :
                print("Not to be here")
                return false
            
        }
    }
    
    func checkName() -> Bool{
        return true
    }
    
}


//Email functions

extension TextFieldForm{
    
    func checkEmail() -> Bool{
        print("Check Email")
        var fieldErros = [FieldErrors]()
        
        if let error = emailIsEmpty() {
            fieldErros.append(error)
        }
        
        if let error = emailIsNotValid() {
            fieldErros.append(error)
        }
        
        
        for error in fieldErros{
            
            print(error.rawValue)
        }
        
        if(fieldErros.count > 0){
            highestLevelError = fieldErros[0]
            showError()
            return false
        }else{
            
            removeError()
            return true
        }
    }
    
    func emailIsNotValid() -> FieldErrors? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if(emailTest.evaluate(with: self.text)){
            return nil
        }else{
            return FieldErrors.EmailIsWrongFormat
        }
    }
    
    func emailIsEmpty() -> FieldErrors? {
    
        print("emailIsEmpty")
        if (self.text ?? "").isEmpty {
            return FieldErrors.EmailIsEmpty
        }else{
            return nil
        }
        
    }
    
}

//Password functions

extension TextFieldForm{
    
    func checkEmail() -> Bool{
        print("Check Email")
        var fieldErros = [FieldErrors]()
        
        if let error = emailIsEmpty() {
            fieldErros.append(error)
        }
        
        if let error = emailIsNotValid() {
            fieldErros.append(error)
        }
        
        
        for error in fieldErros{
            
            print(error.rawValue)
        }
        
        if(fieldErros.count > 0){
            highestLevelError = fieldErros[0]
            showError()
            return false
        }else{
            
            removeError()
            return true
        }
    }
    
    func emailIsNotValid() -> FieldErrors? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if(emailTest.evaluate(with: self.text)){
            return nil
        }else{
            return FieldErrors.EmailIsWrongFormat
        }
    }
    
    func emailIsEmpty() -> FieldErrors? {
        
        print("emailIsEmpty")
        if (self.text ?? "").isEmpty {
            return FieldErrors.EmailIsEmpty
        }else{
            return nil
        }
        
    }
    
}


