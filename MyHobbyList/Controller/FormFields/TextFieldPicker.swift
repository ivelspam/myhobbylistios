import Foundation

import GooglePlaces
import UIKit

struct PickerRow {
    var title: String
    var id: Int
    
    init(title: String, id: Int) {
        self.title = title
        self.id = id
    }
}

class TextFieldPicker: UITextField {
    
    enum FieldError: String {
        case Empty = "&&& Can't Be Empty is Empty"
    }
    
    enum ErrorType: String {
        case AddressIsEmpty
    }
    
    enum FieldType: String {
        case EventType = "Event Type"
        case Reach = "Reach"

    }
    
    func getSelectedID() -> Int{
        return pickerRows[picker.selectedRow(inComponent: 0)].id
    }
    
    var errorLabel = FormErrorLabel()
    
    var picker = UIPickerView()
    
    //views
    var parentVC : UIViewController?
    var topArchorView : UIView?
    var pickerRows = [PickerRow]()
    
    var place : GMSPlace?
    
   
    var fieldType : FieldType?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func customInit(topArchorView : UIView?, parentVC : UIViewController, pickerRows : [PickerRow], fieldType : FieldType){
        
                errorLabel.customInit(topView: self.superview!, topAnchorView: topArchorView, siblingView: self)
        self.topArchorView = topArchorView
        self.parentVC = parentVC
        self.pickerRows = pickerRows
        self.fieldType = fieldType
        
        picker.delegate = self
        self.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissPicker))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.inputAccessoryView = toolBar
        
    }
    
    @objc func dismissPicker(){
        parentVC!.view.endEditing(true)
        self.text = pickerRows[picker.selectedRow(inComponent: 0)].title
    }
    
    func updateValue(place : GMSPlace){
        self.text = place.formattedAddress
        self.place = place
    }
    
    func checkIfIsValid() -> Bool{
        return !checkForErrors()
    }
    
    func buildError(fieldError : FieldError) -> String{
        return fieldError.rawValue.replacingOccurrences(of: "&&&", with: fieldType!.rawValue)
    }
    
    func checkForErrors() -> Bool{
        
        var fieldErrors = [FieldError]()
        
        if let fieldError = fieldIsEmpty() {fieldErrors.append(fieldError)}
        
        if(fieldErrors.count == 0){
            errorLabel.removeFromSuperview()
            return false
        }
        
        errorLabel.text = buildError(fieldError: fieldErrors[0])
        errorLabel.showError()
        return true
        
    }
    
    func fieldIsEmpty() -> FieldError?{
        if (self.text ?? "").isEmpty {
            return FieldError.Empty
        }else{
            return nil
        }
    }
    
}

extension TextFieldPicker: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerRows.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerRows[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        
        
    }
}


