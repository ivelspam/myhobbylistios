import UIKit

class TextFieldForm: UITextField {
    
    enum TextFieldTypes : String {
        case Email = "Email"
        case Name = "Name"
        case Password = "Password"
        case Username = "Username"
        case EventName = "Event Name"
        case PlaceName = "Plcae Name"
    }
    
    enum Error : String {
        
        case Space = "&&& Can't Have Space"
        case Empty = "&&& Can't Be Empty"
        case Format = "&&& Wrong Fromat"
        case NotOnlyLetters = "&&& Can Only Contain Letters"
        
    }
    
    enum ErrorType : String {
        case Space
        case Empty
        case Email
        case NotOnlyLetters
    }
    
    
    enum FieldErrors : String {
        
        case noSpace = "Can't Have Space"
        
        
        case EmailIsEmpty = "Email is Empty"
        case EmailIsWrongFormat = "Email is Wrong Format"
        
        case PasswordIsEmpty = "Password is Empty"
        
        case NameIsEmpty = "Name is Empty"
        case NameNotLetters = "Name Can Only Contain Letters"
        
        case UsernameIsEmpty = "Username is Empty"
        case usernameHasSpace = "User Can't Have Space"
    }

    var errorLabel = FormErrorLabel()
    
    var textFieldType : TextFieldTypes?
   
    
    var topAnchorView : UIView?
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func customInit(topAnchorView : UIView?, textFieldType : TextFieldTypes){
        self.topAnchorView = topAnchorView
        self.textFieldType = textFieldType
        errorLabel.customInit(topView: self.superview!, topAnchorView: topAnchorView, siblingView: self)
        
        
    }
    
    func checkIfIsValid() -> Bool{
        
        switch textFieldType! {
            case .Email:
                return !checkForErrors(errorTypes: [ErrorType.Empty, ErrorType.Email])
            case .Name:
                return !checkForErrors(errorTypes: [ErrorType.Empty, ErrorType.NotOnlyLetters])
            case .Password:
                return !checkForErrors(errorTypes: [ErrorType.Empty])
            case .Username:
                return !checkForErrors(errorTypes: [ErrorType.Empty, ErrorType.Space])
            case .EventName:
                return !checkForErrors(errorTypes: [ErrorType.Empty])
            case .PlaceName:
                return !checkForErrors(errorTypes: [ErrorType.Empty])
        }
    }
    
    func buildError(error : Error) -> String{
        return error.rawValue.replacingOccurrences(of: "&&&", with: textFieldType!.rawValue)
   }
    
    func checkForErrors(errorTypes : [ErrorType]) -> Bool{
        
        var errors = [Error]()
        
        print("check for error");
        for errorType in errorTypes{
            
            print(errorType.rawValue);
            
            switch errorType {
                case .Empty :
                    if let error = fieldIsEmpty() {errors.append(error)}
                case .NotOnlyLetters:
                    if let error = checkForNonLetter() {errors.append(error)}
                case .Space :
                    if let error = checkForSpace() {errors.append(error)}
                case .Email :
                    if let error = emailIsNotValid() {errors.append(error)}
                
            }
        }
        
        if(errors.count == 0){
           
            errorLabel.removeFromSuperview()
            
            return false
            
        }
        
        errorLabel.text = buildError(error: errors[0])
        errorLabel.showError()
        
        return true
        
    }
}


//Error generico
extension TextFieldForm{
    
    func fieldIsEmpty() -> Error?{
        if (self.text ?? "").isEmpty {
            return Error.Empty
        }else{
            return nil
        }
    }
    
    func checkForNonLetter() -> Error? {
        for chr in self.text ?? "" {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return Error.NotOnlyLetters
            }
        }
        return nil
    }
    
    func checkForSpace () -> Error? {
        
        let whitespace = CharacterSet.whitespaces
        
        let phrase = self.text ?? ""
        let range = phrase.rangeOfCharacter(from: whitespace)
        
        // range will be nil if no whitespace is found
        if range != nil {
            return Error.Space
        }
        return nil
    }
    
    func emailIsNotValid() -> Error? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if(emailTest.evaluate(with: self.text)){
            return nil
        }else{
            return Error.Format
        }
    }
}
