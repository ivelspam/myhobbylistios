import UIKit


class TextViewForm: UITextView, UITextViewDelegate {
    
    
    let informationTVPlaceHolder = "Extra Information"
    var parentVC : UIViewController?

    var isEmpty = true
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            self.text = informationTVPlaceHolder
            self.textColor = UIColor.lightGray
            isEmpty = true
            self.selectedTextRange = textView.textRange(from: self.beginningOfDocument, to: self.beginningOfDocument)
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, set
            // the text color to black then set its text to the
            // replacement string
        else if self.textColor == UIColor.lightGray && !text.isEmpty {
            self.textColor = UIColor.black
            self.text = text
        }
            
            // For every other case, the text should change with the usual
            // behavior...
        else {
            
            if self.textColor == UIColor.lightGray {
                isEmpty = true
                
            }
            return true
        }
        
        if self.textColor == UIColor.lightGray {
            isEmpty = true
            
        }

        
        // ...otherwise return false since the updates have already
        // been made
        return false
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
            if self.textColor == UIColor.lightGray {
                self.selectedTextRange = self.textRange(from: self.beginningOfDocument, to: self.beginningOfDocument)
            }
    }
    
    func customInit(parentVC : UIViewController){
        
        self.parentVC = parentVC
        self.delegate = self
        
        self.text = informationTVPlaceHolder
        self.textColor = UIColor.lightGray
        //        informationTV.becomeFirstResponder()
        self.selectedTextRange = self.textRange(from: self.beginningOfDocument, to: self.beginningOfDocument)
        
        
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = 5.0
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissCreateInformationTV))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.inputAccessoryView = toolBar

    }
    
    @objc func dismissCreateInformationTV(){
        parentVC!.view.endEditing(true)
    }
    
    func getText() -> String{
        if isEmpty {
            return ""
        }else{
            return self.text
        }
    }
    
}
