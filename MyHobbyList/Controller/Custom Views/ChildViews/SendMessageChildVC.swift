import UIKit

protocol sendMessageVCDelegate {
    func sendMessage(message : String)
}

class SendMessageChildVC: UIViewController {
    
    var chatMessageVCDelegate : sendMessageVCDelegate?
    @IBOutlet weak var messageTF: UITextField!
    var viewController : UIViewController?
    var bottomKeyboardConstraint : NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }

    func customInit(viewController : UIViewController, superview: UIView, chatMessageVCDelegate : sendMessageVCDelegate, bottomKeyboardConstraint : NSLayoutConstraint){
        self.viewController = viewController
        self.chatMessageVCDelegate = chatMessageVCDelegate
        self.bottomKeyboardConstraint = bottomKeyboardConstraint
        
        superview.addSubview(self.view)
        
        self.view.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
        self.view.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
        self.view.widthAnchor.constraint(equalTo: superview.widthAnchor, multiplier: 1).isActive = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func keyboardWillAppear(notification : Notification){
//        var userInfo = notification.userInfo! as [AnyHashable: Any]
//
//        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
//        if let tabBarHeight = viewController!.tabBarController?.tabBar.frame.height {
//            print("not nil")
//            bottomKeyboardConstraint!.constant = endFrame.height - tabBarHeight
//        }else{
//            bottomKeyboardConstraint!.constant = endFrame.height
//            print("nil")
//        }
        
        var userInfo = notification.userInfo! as [AnyHashable: Any]
        
        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
        if let tabBarHeight = viewController!.tabBarController?.tabBar.frame.height {
            print("not nil")
            viewController!.view.frame.origin.y = -endFrame.height + tabBarHeight
            
        }else{
            bottomKeyboardConstraint!.constant = endFrame.height
            print("nil")
        }
    }
    
    @objc func keyboardWillDisappear(notification : Notification){
        viewController!.view.frame.origin.y = 0
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        let cleasedMessage = (messageTF.text ?? "").removeRightSpaces()
        if !cleasedMessage.isEmpty {
            chatMessageVCDelegate!.sendMessage(message: cleasedMessage )
            messageTF.text = ""
        }
    }
    
    func endEditing(){
        messageTF.endEditing(true)
    }
    
    func removeObserver(){
        NotificationCenter.default.removeObserver(self)
    }
}


//        var userInfo = notification.userInfo! as [AnyHashable: Any]
//
//        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
//        if let tabBarHeight = viewController!.tabBarController?.tabBar.frame.height {
//            print("not nil")
//            bottomKeyboardConstraint!.constant = endFrame.height - tabBarHeight
//        }else{
//            bottomKeyboardConstraint!.constant = endFrame.height
//            print("nil")
//        }
