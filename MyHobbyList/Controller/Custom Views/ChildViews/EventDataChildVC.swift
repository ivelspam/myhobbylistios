import UIKit
import MapKit
import CoreLocation
import SwiftyJSON

class EventDataChildVC: UIViewController {
    var event : Event?
    var eventComments = [EventComment]()
    var eventMessageSocketIO : EventMessageSocketIO?
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var hobbyName: UILabel!
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var hours: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var quantityOfPeople: UILabel!
    @IBOutlet weak var aDescription: UILabel!

    var didFoundLocation = false

    override func viewDidLoad() {
        super.viewDidLoad()
        Radar.shared.getNewUpdate(delegate: self)
    }
    
    func customInit(event : Event) {
        self.event = event
    }
    
    func createStaticViews(){
        eventName.text = event!.Name
        hobbyName.text = event!.HobbyName
        owner.text = event!.AppUserName
        hours.text = "\(event!.StartDatetime.convertDateTo(format: "E, dd:mm")) - \(event!.EndDatetime.convertDateTo(format: "E, dd:mm"))"
        placeName.text = event!.PlaceName
        address.text = event!.Address
        
        quantityOfPeople.text = "There are 6 people in the \(event!.QuantityOfPeople)"
        aDescription.text = event!.Description
    }
}

extension EventDataChildVC : gotUpdatedLocationDelegate {
    func gotLocation(location: CLLocation?) {
        if let tempLocation = location {
            distance.text = "\(Utils.getDistanceFromLocation(eventCCLocation: CLLocation(latitude: event!.Latitude, longitude: event!.Longitude), gpsCCLocation: tempLocation))"
        }else{
            
        }
    }
    
}
