import UIKit
import SwiftyJSON

class EventCommentChildVC: UIViewController, sendMessageVCDelegate {

    var event : Event?
    var eventComments = [EventComment]()
    var eventMessageSocketIO : EventMessageSocketIO?
    
    @IBOutlet weak var eventCommentsTV: UITableView!
    @IBOutlet weak var sendMessageView: UIView!
    let sendMessageChildVC = SendMessageChildVC()
    var parentVC : UIViewController?
    
    @IBOutlet weak var bottomKeyboardConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("EventCommentChildVC: viewDidLoad")

        sendMessageChildVC.customInit(viewController: parentVC!, superview: sendMessageView, chatMessageVCDelegate: self, bottomKeyboardConstraint: bottomKeyboardConstraint)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        eventCommentsTV.addGestureRecognizer(tapGesture)
        buildTable()

    }
    
    func customInit(event : Event, parentVC : UIViewController){
        self.event = event
        self.parentVC = parentVC
    }
    
    @objc func tableViewTapped(){
        print("tableViewTapped")
        sendMessageChildVC.endEditing()
    }
    
    func sendMessage(message: String) {
        let eventComment = EventComment(comment: message, eventID: event!.EventID, appUserID: UserDefaults.standard.integer(forKey: DefaultKey.AppUserID_int.rawValue))

        eventComments.append(eventComment)
        eventCommentsTV.reloadData()
        scrollToBottom()

        eventMessageSocketIO!.sendMessage(eventComment: eventComment) { (worked, response) in
            print("response")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("EventCommentChildVC: viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("EventCommentChildVC: viewDidAppear")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("EventCommentChildVC: viewWillDisappear")

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print("EventCommentChildVC: viewDidDisappear")

    }
    
    func startSocket(){
        createSocket()
    }
    
    func endSocket(){
        eventMessageSocketIO!.disconnect()
        eventMessageSocketIO = nil
    }
}

extension EventCommentChildVC{
    func getMessages(){
        
        
        
        
        ServerRequest.getRequest(request: Eventcomment_getEventComments(eventID: event!.EventID)) { (response, worked) in
            if response["message"].stringValue == "worked" {
                
                for (_, eventComment):(String, JSON) in response["comments"] {
                    self.eventComments.append(EventComment(jsonEventCommentFromMySQL: eventComment))
                }
            }
            self.eventCommentsTV.reloadData()
            self.scrollToBottom()
        }
    }
}


//socketIO
extension EventCommentChildVC: EventCommentSocketIODelegate {
    func receivedMessageFromSocketIO(eventComment: EventComment) {
        eventComments.append(eventComment)
        eventCommentsTV.reloadData()
        scrollToBottom()
    }
    
    func createSocket(){
        print("create socket")
        eventMessageSocketIO = EventMessageSocketIO(event: event!, eventCommentSocketIODelegate: self)
    }
}

extension EventCommentChildVC : UITableViewDelegate, UITableViewDataSource{
    
    func buildTable(){
        
        eventCommentsTV.delegate = self
        eventCommentsTV.dataSource = self
        
        eventCommentsTV.estimatedRowHeight = 80
        eventCommentsTV.rowHeight = UITableViewAutomaticDimension
        eventCommentsTV.separatorStyle = .none
        
        registerNibs()
        getMessages()
    }
    
    func registerNibs(){
        eventCommentsTV.register(UINib(nibName: "EventCommentTVC", bundle: nil), forCellReuseIdentifier: "eventCommentTVC")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCommentTVC") as! EventCommentTVC
        
        cell.customInit(eventComment: eventComments[indexPath.row])
        
        return cell
    }
    
    func scrollToBottom(){
        print("Scroll to bottom")
        DispatchQueue.main.async {
            if self.eventComments.count > 0{
                let indexPath = IndexPath(row: self.eventComments.count-1, section: 0)
                self.eventCommentsTV.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    

}

