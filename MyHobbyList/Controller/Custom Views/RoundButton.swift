//
//  RoundButton.swift
//  MyHobbyList
//
//  Created by Nicholas on 5/8/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius : CGFloat = 0 {
        
        didSet{
            self.layer.cornerRadius = cornerRadius
            
        }
        
    }
    
    @IBInspectable public var isCircular: Bool = false {
        didSet {
            if(isCircular){
                self.layer.cornerRadius = self.frame.size.width / 2
            }
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        
        didSet{
            self.layer.borderWidth = borderWidth
            
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }

}
