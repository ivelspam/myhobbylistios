import UIKit

class FriendCVC: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func customInit(friend : Friend){
        
        label.text = friend.Name
        
        Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: friend.AppUserID, imageVersion: friend.ImageVersion) { (foundImage) in
            if foundImage != nil{
                
                self.img.image = foundImage
                
            }
        }
        
    }
    

}
