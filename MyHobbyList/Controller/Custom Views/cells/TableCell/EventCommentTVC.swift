import UIKit

class EventCommentTVC: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var appUserImage: roundImage!
    
    
    var eventComment : EventComment?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func customInit(eventComment : EventComment){
        
        self.eventComment = eventComment
        name.text = eventComment.Name!
        date.text = eventComment.CreatedTS.convertDateTo(format: "E hh:mma")
        message.text = eventComment.Comment
        
        Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: eventComment.AppUserID, imageVersion: eventComment.ImageVersion) { (image) in
            if let foundImage = image {
                self.appUserImage.image = foundImage
            }
        }
    }
    
    
    
}
