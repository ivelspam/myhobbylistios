import UIKit

class MessageSentRow: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var message: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func customInit(chatMessage : Chat){
        message.text = chatMessage.Message
        date.text = chatMessage.CreatedTS.convertDateTo(format : "hh:mm MM/dd")
    }
}
