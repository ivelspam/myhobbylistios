import UIKit

class MessageReceivedRow: UITableViewCell {

    @IBOutlet weak var img: roundImage!
    @IBOutlet weak var message: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func customInit(chatMessage : Chat, friend : Friend){
        message.text = chatMessage.Message
        Utils.checkIfImageExistsOrDownload(imageType: Download_image.ImageType.PROFILE, appUserID: friend.AppUserID, imageVersion: friend.ImageVersion){
                image in
   
            self.img.image = image
        }
 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
