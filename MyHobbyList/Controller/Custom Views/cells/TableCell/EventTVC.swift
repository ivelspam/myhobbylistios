import UIKit

class EventTVC: UITableViewCell {
    
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var howManyPeople: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var placeName: UILabel!
    
    var hobby : Hobby?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func customInit(event : Event){
        
        hobby = Motherbase().getHobbyTable().selectBy(hobbyID: event.HobbyID)
        eventType.text = hobby!.Name
        eventName.text = event.Name
        eventTime.text = "\(event.StartDatetime.convertDateTo(format: "HH:MM")) - \(event.EndDatetime.convertDateTo(format: "HH:MM"))"
        owner.text = event.AppUserName
        howManyPeople.text = "\(event.QuantityOfPeople) person in the event"
        distance.text = "Put Distance"
        placeName.text = event.PlaceName
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
