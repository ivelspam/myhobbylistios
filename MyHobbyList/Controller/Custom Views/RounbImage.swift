//
//  RounbImage.swift
//  MyHobbyList
//
//  Created by Nicholas on 5/9/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import UIKit

class RoundImage: UIImageView {

    @IBInspectable var cornerRadious : CGFloat = 0 {
        
        didSet{
            self.layer.cornerRadius = cornerRadious
            
        }
        
    }
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        
        didSet{
            self.layer.borderWidth = borderWidth
            
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
}

//class ProfileViewController: UIViewController {
//
//    @IBOutlet weak var ivProfile: UIImageView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        ivProfile.layer.cornerRadius = ivProfile.frame.size.width/2
//        ivProfile.clipsToBounds = true
//    }
//
//}


