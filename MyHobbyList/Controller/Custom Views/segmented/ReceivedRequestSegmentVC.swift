import UIKit

class ReceivedRequestSegmentVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var changeSegueDelegate : ChangeSegueDelegate?
    
    var friends : [Friend] = []
    let friendsTable = Motherbase().getFriendTable()
    let segueName = SegueName.ShowSentRequestFriend
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "FriendCVC", bundle: nil)
        friends = friendsTable.findFriendsByDateCreated(dateCreated: Utils.convertDateToDatetime(date: Date()), state: FriendState.ReceivedRequest)
        collectionView.register(nib, forCellWithReuseIdentifier: "friendCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("Collection View Cell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "friendCell", for: indexPath) as! FriendCVC
        cell.customInit(friend: friends[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(friends.count)
        return friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt")
        
        changeSegueDelegate?.changeSegue(segueName: SegueName.OpenFriendChat, friend: friends[indexPath.row])
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("ConfirmedVC perform segue")
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("didSelectItemAt")
    }
}
