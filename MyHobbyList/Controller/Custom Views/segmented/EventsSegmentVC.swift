import UIKit

class EventsSegmentVC: UIViewController {
    
    @IBOutlet weak var vEventsTVC: UITableView!
    var mEventsDict = [Int: Event]()
    var mEventsSorted = [Event]()
    var mEventType = 0
    
    static let SHOWING = 0
    static let HIDDEN = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "EventTVC", bundle: nil)
        

        if vEventsTVC == nil {
            print("Is Empty")
            
        }
        
        vEventsTVC.register(nib, forCellReuseIdentifier: "EventTVC")
        
        vEventsTVC.delegate = self
        vEventsTVC.dataSource = self
//        registerNibs()
    }

   
    func customInit(eventState : Int){
        mEventType = eventState
    }
    
    func change(event: Event){
//        //Remove
//        if event.State != mEventState && mEventsDict[event.EventID] != nil {
//            
//        }
//        
//        //add
//        if event.State == mEventState && mEventsDict[event.EventID] == nil {
//            
//        }
    }
    
    func makeSorted(){
        
        mEventsSorted.removeAll()
        
        for (_, value) in mEventsDict {

            mEventsSorted.append(value)
        
        }
        
        vEventsTVC.reloadData()
    }
}

extension EventsSegmentVC: UITableViewDelegate, UITableViewDataSource {
    
    func registerNibs(){
        vEventsTVC.register(UINib(nibName: "EventTVC", bundle: nil), forCellReuseIdentifier: "eventTVC")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mEventsSorted.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = vEventsTVC.dequeueReusableCell(withIdentifier: "eventTVC", for: indexPath) as! EventTVC
        cell.customInit(event: mEventsSorted[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueName.ShowEventInfo.rawValue, sender: mEventsSorted[indexPath.row])
    }
}
