import UIKit

class FriendSegmentVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var changeSegueDelegate : ChangeSegueDelegate?
    
    var mFriendsDict = [Int: Friend]()
    var mSortedList = [Friend]()
    
    var segueName : SegueName?
    var friendState : FriendState?
   
    func customInit(segueName: SegueName, friendState: FriendState, changeSegueDelegate : ChangeSegueDelegate){
        self.segueName = segueName
        self.friendState = friendState
        self.changeSegueDelegate = changeSegueDelegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("FriendSegmentVC viewDidLoad did load")
        
        let nib = UINib(nibName: "FriendCVC", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "friendCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("Collection View Cell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "friendCell", for: indexPath) as! FriendCVC
        cell.customInit(friend: mSortedList[indexPath.row])
        return cell
    }
    
    func removeFriend(friend : Friend){
        let index = mSortedList.index{ (friendObj) -> Bool in
            friend.AppUserID == friendObj.AppUserID
        }
        
        if let temp = index{
            print(temp)
            mSortedList.remove(at: temp)
            collectionView.reloadData()
        }else{
            print("Index not found")
        }
    }
    
    func addFriend(friend : Friend){
        
        
        //Remove
        collectionView.reloadData()
    }
    
    func makeChanges(friends : [Friend] ){
        //Removed
        for friend in friends {
            change(friend: friend)
        }
        makeSortedList()
    }
    
    func change(friend : Friend){
        
        if friend.State != friendState! && mFriendsDict[friend.AppUserID] != nil {
            
            mFriendsDict.removeValue(forKey: friend.AppUserID)
        }
        if friend.State == friendState! && mFriendsDict[friend.AppUserID] == nil {
            mFriendsDict[friend.AppUserID] = friend
        }

    }
    
    func makeSortedList(){
        mSortedList.removeAll()
        
        for (_, value) in mFriendsDict {
            mSortedList.append(value)
        }
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mSortedList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        changeSegueDelegate?.changeSegue(segueName: segueName!, friend: mSortedList[indexPath.row])
    }
}
