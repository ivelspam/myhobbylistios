import UIKit

class CategorySubviewVC: UIViewController {
    
    //CategoriesTable
    var categoriesRows : [Hobby] = []
    var categoriesRowsFiltered : [Hobby] = []
    var currentParent : Hobby?
    
    @IBOutlet weak var filterTF: UITextField!
    @IBOutlet weak var returnBT: UIButton!
    @IBOutlet weak var categoryTV: UITableView!
 
    var appUserHobbyTable = Motherbase().getAppUserHobbyTable()
    var hobbyTable = Motherbase().getHobbyTable()
    
    var myHobbiesProtocol : MyHobbiesProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        //returnBT Initial value
        var image = UIImage(named: "left arrow")
        image = image?.scaleImageToSize(newSize: CGSize(width: 30, height: 30))
        returnBT.setImage(image, for: .normal)
        loadNewParent(hobby: nil)
        createTable()
    }
    
    func customInit(myHobbiesProtocol : MyHobbiesProtocol?){
        self.myHobbiesProtocol = myHobbiesProtocol
    }
   
    @objc func textFieldDidChange(_ textField: UITextField) {
       
        filterList()
    }
    
    func filterList(){
        categoriesRowsFiltered.removeAll()
        let lowerCase = filterTF.text!.lowercased()
        if filterTF.text!.count == 0 {
            categoriesRowsFiltered = categoriesRows
        }else{
            for hobby in categoriesRows {
                if hobby.Name.lowercased().range(of: lowerCase) != nil {
                    categoriesRowsFiltered.append(hobby)
                }
            }
        }
        categoryTV.reloadData()
    }

    func updateItemInList(hobbyID : Int, inHobby : Bool){
        for hobby in categoriesRows {
            if hobby.HobbyID == hobbyID{
                hobby.InHobby = inHobby
            }
        }
        
//        hobbyTable
        filterList()
    }
    
    @IBAction func btGoToParent(_ sender: UIButton) {
        print("@IBAction func btGoToParent(_ sender: UIButton)")
        let parentHobby = hobbyTable.selectBy(hobbyID: currentParent!.ParentID)
        loadNewParent(hobby: parentHobby)
    }
    
    func getParentNode(){
        categoriesRows = appUserHobbyTable.getParentNodes()
        categoriesRowsFiltered = categoriesRows
        categoryTV.reloadData()
    }
    
}

extension CategorySubviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func createTable(){
        categoryTV.delegate = self
        categoryTV.dataSource = self
        
        getParentNode()
        
        categoryTV.register(UINib(nibName: "ParentTVC", bundle: nil), forCellReuseIdentifier: "ParentTVC")
        categoryTV.register(UINib(nibName: "HobbyTVC", bundle: nil), forCellReuseIdentifier: "HobbyTVC")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesRowsFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = categoriesRowsFiltered[indexPath.row]
        
        if row.aType == "child" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTVC") as! HobbyTVC
            cell.customInit(hobby: row, myHobbiesProtocol: myHobbiesProtocol!, option : "categories")
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParentTVC") as! ParentTVC
            cell.customInit(hobby: row)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hobby = categoriesRowsFiltered[indexPath.row]
        
        if hobby.aType == "parent" {
            loadNewParent(hobby: hobby)
        }
    }
    
    func loadNewParent(hobby: Hobby?){
        
        print("func loadNewParent(hobby: Hobby?)")
        
        var hobbyID = -1
        
        if let tempHobby = hobby {
            currentParent = tempHobby.copy() as? Hobby
            print(currentParent!.toString())
            hobbyID = currentParent!.HobbyID
        }

        categoriesRows = appUserHobbyTable.getHobbiesByLayers(hobbyID: hobbyID)
        categoriesRowsFiltered = categoriesRows
        categoryTV.reloadData()
        filterTF.text = ""
        
        if hobbyID == -1 {
            returnBT.setTitle("Categories", for: .normal)
            returnBT.isEnabled = false
            currentParent = nil

        }else{
            returnBT.isEnabled = true
            returnBT.setTitle(currentParent?.Name, for: .normal)
        }
    }
}
