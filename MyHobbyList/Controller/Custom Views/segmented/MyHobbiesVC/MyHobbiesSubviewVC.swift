import UIKit

class MyHobbiesSubviewVC: UIViewController,  UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var filterTF: UITextField!
    @IBOutlet weak var hobbyTV: UITableView!
    
    var appUserHobbyTable = Motherbase().getAppUserHobbyTable()
    var hobbyTable = Motherbase().getHobbyTable()
    
    var myHobbiesProtocol : MyHobbiesProtocol?
    
    //HobbiesTable
    var appUserHobbies : [Hobby] = []
    var appUserHobbiesFiltered : [Hobby] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hobbyTV.delegate = self
        hobbyTV.dataSource = self
        
        appUserHobbies = appUserHobbyTable.selectAppUserHobbyComplete()
        appUserHobbiesFiltered = appUserHobbies
        
        print(appUserHobbiesFiltered.count)
        filterTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        hobbyTV.register(UINib(nibName: "HobbyTVC", bundle: nil), forCellReuseIdentifier: "HobbyTVC")
    }
    
    func customInit(myHobbiesProtocol : MyHobbiesProtocol){
        self.myHobbiesProtocol = myHobbiesProtocol
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        filterList()
    }
    
    func filterList(){
        appUserHobbiesFiltered.removeAll()
        let lowerCase = filterTF.text!.lowercased()
        if filterTF.text!.count == 0 {
            appUserHobbiesFiltered = appUserHobbies
        }else{
            for hobby in appUserHobbies {
                if hobby.Name.lowercased().range(of: lowerCase) != nil {
                    appUserHobbiesFiltered.append(hobby)
                }
            }
        }        
        hobbyTV.reloadData()
    }
    
    func updateItemInList(hobbyID : Int, inHobby : Bool){
        
        var found = false
        
        for hobby in appUserHobbies {
            if hobby.HobbyID == hobbyID {
                hobby.InHobby = inHobby
                found = true
            }
        }
        
        if !found {
            let hobby = hobbyTable.selectHobbyBy(hobbyID: hobbyID)!
            hobby.InHobby = inHobby
            appUserHobbies.append(hobby)
        }
        
        filterList()
        hobbyTV.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appUserHobbiesFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HobbyTVC") as! HobbyTVC
        cell.customInit(hobby: appUserHobbiesFiltered[indexPath.row], myHobbiesProtocol: myHobbiesProtocol!, option : "list")
        return cell
    }
    
    @IBAction func tfHobbiesFilterEditingChanged(_ sender: UITextField) {
        if(filterTF.text?.count == 0){
            appUserHobbiesFiltered = appUserHobbies
        }else{
            appUserHobbiesFiltered = appUserHobbies.filter{ matches(for: filterTF.text!, in: $0.Name).count > 0 }
        }
        hobbyTV.reloadData()
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }

}

