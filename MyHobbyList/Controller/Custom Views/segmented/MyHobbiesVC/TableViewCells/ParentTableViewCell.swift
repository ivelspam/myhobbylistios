//
//  ParentTableViewCell.swift
//  MyHobbyList
//
//  Created by Nicholas on 5/17/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import UIKit

class ParentTableVC: UITableViewCell {
    
    var hobby : Hobby?
    @IBOutlet weak var hobbyName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func customInit(hobby : Hobby){
        self.hobby = hobby
       hobbyName.text = hobby.Name
    }
    
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
