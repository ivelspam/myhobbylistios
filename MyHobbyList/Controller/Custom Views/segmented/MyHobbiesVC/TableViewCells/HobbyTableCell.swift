import UIKit

class HobbyTC: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var swtRegistration: UISwitch!
    var hobby : Hobby?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
//    }
    
    func customInit(hobby : Hobby){
        self.hobby = hobby
        lblName.text = hobby.Name
    }
    
    
    @IBAction func swtRegistration(_ sender: Any) {
        
        if let tempHobby = hobby {
        
        if swtRegistration.isOn{
                ServerRequest.postRequest(url: "api/hobbies/addHobbiesToUser", parameters: ["HobbyID": String(tempHobby.HobbyID)], usingToken: true){
                    allInfoJSON, worked in
                    Motherbase().getAppUserHobbyTable().insertAppUserHobby(hobbyID: tempHobby.HobbyID)
                }
            tempHobby.InHobby = true
        }else{
            ServerRequest.postRequest(url: "api/hobbies/removeHobbyFromUser", parameters: ["HobbyID": String(tempHobby.HobbyID)], usingToken: true){
                allInfoJSON, worked in
                Motherbase().getAppUserHobbyTable().deleteAppUserHobby(hobbyID: tempHobby.HobbyID)
            }
            tempHobby.InHobby = false

        }
            
      }
    }
}
