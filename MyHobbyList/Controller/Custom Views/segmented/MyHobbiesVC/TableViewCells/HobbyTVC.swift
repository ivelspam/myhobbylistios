import UIKit

class HobbyTVC: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var swtRegistration: UISwitch!
    var hobby : Hobby?
    var myHobbiesProtocol : MyHobbiesProtocol?
    var option : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    func customInit(hobby : Hobby, myHobbiesProtocol : MyHobbiesProtocol?, option : String?){
        self.hobby = hobby
        lblName.text = hobby.Name
        swtRegistration.isOn = hobby.InHobby
        self.myHobbiesProtocol = myHobbiesProtocol
        self.option = option
    }
    
    @IBAction func swtRegistration(_ sender: Any) {
        
        if let tempHobby = hobby {
            if swtRegistration.isOn{
                ServerRequest.postRequest(request: Hobbies_addHobby(hobbyID : tempHobby.HobbyID)){
                        allInfoJSON, worked in
                        Motherbase().getAppUserHobbyTable().insertAppUserHobby(hobbyID: tempHobby.HobbyID)
                    }
                tempHobby.InHobby = true
            }else{
                ServerRequest.postRequest(request : Hobbies_removeHobby(hobbyID: tempHobby.HobbyID)){
                    allInfoJSON, worked in
                    Motherbase().getAppUserHobbyTable().deleteAppUserHobby(hobbyID: tempHobby.HobbyID)
                }
                tempHobby.InHobby = false
            }
            self.myHobbiesProtocol?.updateHobby(hobbyID: hobby!.HobbyID, inHobby: tempHobby.InHobby, option: option!)
        }
    }
}
