import UIKit

class ParentTVC: UITableViewCell {
    
    var hobby : Hobby?
    @IBOutlet weak var hobbyName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func customInit(hobby : Hobby){
        self.hobby = hobby
       hobbyName.text = hobby.Name
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
