//
//  EventFragmentVC.swift
//  MyHobbyList
//
//  Created by Nicholas Homolka on 9/7/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import UIKit

class EventsSegmentVC: UIViewController {
    
    @IBOutlet weak var vEventsTVC: UITableView!
    var mEventsDict = [Int: Friend]()
    var mEventsSorted = [Event]()
    var mEventState : Event.EventState?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vEventsTVC.delegate = self
        vEventsTVC.dataSource = self
        registerNibs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customInit(eventState : Event.EventState){
        
        mEventState = eventState
        
    }
    
    func change(event: Event){
        
        
        //Remove
        if event.State != mEventState && mEventsDict[event.EventID] != nil {
            
            
        }
        
        //add
        if event.State == mEventState && mEventsDict[event.EventID] == nil {
            
            
            
        }
        
    }
    
    func makeSorted(){
        
        mEventsSorted.removeAll()
        
        
        for (key, value) in mEventsDict {
            
//            mEventsSorted.append(value)
            
            
        }
        
//        m
        
    }
    
    
}


extension EventsSegmentVC: UITableViewDelegate, UITableViewDataSource {
    
    func registerNibs(){
        vEventsTVC.register(UINib(nibName: "EventTVC", bundle: nil), forCellReuseIdentifier: "eventTVC")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mEventsSorted.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = vEventsTVC.dequeueReusableCell(withIdentifier: "eventTVC", for: indexPath) as! EventTVC
        cell.customInit(event: mEventsSorted[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueName.ShowEventInfo.rawValue, sender: mEventsSorted[indexPath.row])
    }
}
