import UIKit
import SwiftyJSON
import SocketIO

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, ChatSocketDelegate{

    
    @IBOutlet weak var chatMessageTV: UITableView!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var btSendMessage: UIButton!
       
    @IBOutlet weak var messageView: UIView!

    var chatSocketIO : ChatSocketIO?
    
    var delegate : UpdateFriendsDelegate?
    
    var AppUserID : Int = -1
    
    var chatMessages : [ChatMessage] = []
    var chatMessageTable = Motherbase().getChatTable()
    
    var friend : Friend?
   
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
        print("\n\n\n\n\n")
        chatSocketIO = ChatSocketIO(friendAppUserID: friend?.AppUserID ?? -1, chatSocketDelegate: self)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        chatMessageTV.addGestureRecognizer(tapGesture)
        
        AppUserID = UserDefaults.standard.integer(forKey: UserDefaultsKeys.APPUSERID_INT.rawValue)
        chatMessageTV.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
        chatMessageTV.delegate = self
        chatMessageTV.dataSource = self
        
        tvMessage.delegate = self
        
        chatMessageTV.allowsSelection = false
        
        print("\n\n\n\n")
        
        chatMessages = chatMessageTable.getFriendChatMessages(owner: AppUserID, receiver: friend!.AppUserID, limit : 15)
        
        chatMessageTV.estimatedRowHeight = 80
        chatMessageTV.rowHeight = UITableViewAutomaticDimension
        chatMessageTV.separatorStyle = .none
        
        
        chatMessageTV.register(UINib(nibName: "MessageReceivedRow", bundle: nil), forCellReuseIdentifier: "MessageReceivedRow")
        chatMessageTV.register(UINib(nibName: "MessageSentRow", bundle: nil), forCellReuseIdentifier: "MessageSentRow")
        getNewMessages()

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowIndex = chatMessages.count - 1 - indexPath.row
        let row = chatMessages[rowIndex]
        
        if(AppUserID == row.Owner){
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSentRow") as! MessageSentRow
            cell.customInit(chatMessage: row)
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReceivedRow") as! MessageReceivedRow
        cell.customInit(chatMessage: row, friend : friend!)
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi))
        return cell
    }
    
    func updateTableContentInset() {
        let numRows = tableView(self.chatMessageTV, numberOfRowsInSection: 0)
        var contentInsetTop = self.chatMessageTV.bounds.size.height
        for i in 0..<numRows {
            let rowRect = self.chatMessageTV.rectForRow(at: IndexPath(item: i, section: 0))
            contentInsetTop -= rowRect.size.height
            if contentInsetTop <= 0 {
                contentInsetTop = 0
            }
        }
        self.chatMessageTV.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)
    }
    
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        UIView.animate(withDuration: 0.5) {
            self.heightContraint.constant = 308
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5) {
            self.heightContraint.constant = 40
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func sendChatMessage(_ sender: Any) {

        let message = tvMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if(message.count > 0){
            tvMessage.text = ""
            let chatMessage = ChatMessage(receiver: friend?.AppUserID ?? -1, message: message)
            chatMessages.append(chatMessage)
            chatMessageTable.insertAChatMessage(chatMessage: chatMessage)
            chatMessageTV.reloadData()
            chatSocketIO?.sendMessage(chatMessage: chatMessage){
                worked, result in
                let chatMessage = ChatMessage(fromMySQLChatMessageJSON: result)
                self.chatMessageTable.insertAChatMessage(chatMessage: chatMessage)
            }
        }
    }
    
    @objc func tableViewTapped(){
        tvMessage.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        chatSocketIO!.disconnect()
    }
    
    func receivedMessageFromSocketIO(chatMessage: ChatMessage) {
        chatMessages.append(chatMessage)
        chatMessageTable.insertAChatMessage(chatMessage: chatMessage)
        chatMessageTV.reloadData()
    }
    
    func getNewMessages(){
        let chatIDMax = chatMessageTable.getLastChatTime(friendAppUserID: AppUserID)
        
        ServerRequest.getRequest(url: "api/chat/getNewMessagesFromUser", parameters: ["ChatID" : String(chatIDMax), "FriendAppUserID" : String(friend!.AppUserID)], usingToken: true) { (response, worked) in
            
            let jsonValues = JSON(response)
            
            if(jsonValues["message"] == "new"){
                self.chatMessageTable.insertNew(chatMessages: jsonValues["chatMessages"])
                
                self.chatMessages = self.chatMessageTable.getFriendChatMessages(owner: self.AppUserID, receiver: self.friend!.AppUserID, limit : 15)
                self.chatMessageTV.reloadData()
                
            }else{
                print("Nothing found")
            }
        }
    }
    
}
