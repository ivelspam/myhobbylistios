import UIKit
import Alamofire
import SwiftyJSON

class LoginInVC: UIViewController {

    @IBOutlet weak var loginView: UIView!
    @IBOutlet var emailTV: TextFieldForm!
    @IBOutlet weak var passwordTV: TextFieldForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("LoginInViewController")
      
        emailTV.customInit(topAnchorView: nil, textFieldType: .Email)
        passwordTV.customInit(topAnchorView: emailTV, textFieldType: .Password)
    }
    
    @IBOutlet weak var temp: UIButton!
    
    func checkFormIsValid(){
        
        var valid = true
        
        if(!emailTV.checkIfIsValid()){
            valid = false
        }
        
        if(!passwordTV.checkIfIsValid()){
            valid = false
        }
        
        
        if(valid){
            submitLogin()
        }
        
    }
    
   func submitLogin(){
        
    ServerRequest.postRequest(request : Login_email(email: emailTV.text!, password: passwordTV.text!)) { (response, worked) in
            print(response)
            
            if response["message"].stringValue == "match"{
                UserDefaults.standard.set(response["token"].string, forKey: DefaultKey.Token_str.rawValue)
                self.performSegue(withIdentifier: SegueName.LoggedInSegue.rawValue, sender: nil)
            }
        }
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        print("login button pressed")
       checkFormIsValid()
        

    }
    
    @IBAction func otherLogin(_ sender: Any) {
        Alamofire.request("https://synchobby.com/api/login/email", method: .post, parameters: ["Email" : "junior_yxpksug_araujo@tfbnw.net", "Password" : "F0caliz4r"])
            .responseJSON{response in
                if response.result.isSuccess {
                    let requestJSON : JSON = JSON(response.result.value!)
                    let message = requestJSON["message"]
                    if message == "match"{
                        UserDefaults.standard.set(requestJSON["token"].string, forKey: DefaultKey.Token_str.rawValue)
                        self.performSegue(withIdentifier: SegueName.LoggedInSegue.rawValue, sender: nil)

                    }
                }
        }
    }
   
}

