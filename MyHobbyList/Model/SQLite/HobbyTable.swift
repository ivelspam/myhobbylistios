import UIKit
import SQLite
import SwiftyJSON

class HobbyTable{
    
    let hobbyTable = Table(TableN.hobby)
    let HobbyID = Expression<Int>(TableE.HobbyID)
    let Name = Expression<String>(TableE.Name)
    let aType = Expression<String>(TableE.aType)
    let Version = Expression<Int>(TableE.Version)
    let ParentID = Expression<Int>(TableE.ParentID)
    let ReceivedTS = TableE.ReceivedTS
    
    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){
        
        print("Creating hobbyTable")
        
        let createTable = self.hobbyTable.create{(table) in
            table.column(self.HobbyID, primaryKey: true)
            table.column(self.Name)
            table.column(self.aType)
            table.column(self.Version)
            table.column(self.ParentID, defaultValue : -1)
            table.column(ReceivedTS, defaultValue: Date().MySQLiteFormat)
        }
        
        do {
            try database.run(createTable)
            print("Created hobbyTable")
        }catch{
            print(error)
        }
    }
    
    func dropTable(){
        let dropTable = hobbyTable.drop(ifExists: true);
        do {
            
            try database.run(dropTable)
            print("drop hobbyTable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
        do {
            try database.run(hobbyTable.delete())
            print("clearTable hobbyTable")
        }catch{
            print(error)
        }
    }
    
    func addHobbiesToTable(allHobbies : JSON){
        do{
            for (_,hobby):(String, JSON) in allHobbies {
                try database.run(self.hobbyTable.insert(
                        or: .replace,
                        HobbyID <- hobby[MySQLColumns.HobbyID].int!,
                        Name <- hobby[MySQLColumns.Name].string!,
                        aType <- hobby[MySQLColumns.aType].string!,
                        Version <- hobby[MySQLColumns.Version].int!,
                        ParentID <- hobby[MySQLColumns.ParentID].int ?? -1
                        )
                    )
            }
        }catch{
            print(error)
        }
    }
    
    func selectHobbies() -> [Hobby]{
        return []
    }
    
    func selectBy(hobbyID : Int) -> Hobby?{
        print("func selectBy(hobbyID : Int) -> Hobby?")
        print(hobbyID)
        
        do {
            for row in try database.prepare(hobbyTable.filter(HobbyID == hobbyID).limit(1)){
                return Hobby(row : row)
            }
        }catch{
            print(error)
        }
        return nil
    }
    
    func toString() -> String{
        
        var values = "Hobby Table: \n"
        
        do {
            
            let stmt = try database.prepare("SELECT * FROM \(TableN.hobby)")
            for row in stmt {
                for (index, columnName) in stmt.columnNames.enumerated(){
                    values.append("\(columnName)=\(row[index] ?? -1),\t")
                }
                values.append("\n")
            }
            
        } catch {
            print(error)
        }
        
        return values
    }
    
    func selectHobbyByToken(token : String) -> [Hobby]{
        print("getHobbyByToken")
        print(token)
        
        var hobbies = [Hobby]()
        let appuserhobbytable = Motherbase().getAppUserHobbyTable().appuserhobbytable
        
        let query = hobbyTable.join(.leftOuter, appuserhobbytable, on:appuserhobbytable[HobbyID] == hobbyTable[HobbyID]).filter(aType
         == Hobby.child && Name.like("%\(token)%"))

        
        do {
            let result = try database.prepare(query)
           
            for row in result {
                
                print(row)
                
                var inHobby = false
                if row[appuserhobbytable[Expression<Int?>("HobbyID")]] != nil {
                   inHobby = true
                }

                hobbies.append(Hobby(   hobbyID: row[hobbyTable[HobbyID]],
                                        name: row[hobbyTable[Name]],
                                        type: row[hobbyTable[aType]],
                                        version: row[hobbyTable[Version]],
                                        parentID: row[hobbyTable[ParentID]],
                                        inHobby: inHobby)
                )
            }
        }catch {
            print(error)
        }
        
        return hobbies
    }
    
    
    func selectHobbyBy(hobbyID : Int) -> Hobby?{
        print("getHobbyByToken")
        
        do {
            let result = try database.prepare(hobbyTable.filter(HobbyID == hobbyID))
            
            for row in result {
                return (Hobby(row: row))
            }
        }catch {
            print(error)
        }
        
        return nil
    }
    
    
    func getHobbiesByParent(parentID : Int) -> [Hobby]{
        
        print("getHobbyByToken")
        
        var hobbies = [Hobby]()
        
        let appuserhobbytable = Motherbase().getAppUserHobbyTable().appuserhobbytable
        
        let query = hobbyTable.join(.leftOuter, appuserhobbytable, on:appuserhobbytable[HobbyID] == hobbyTable[HobbyID]).filter(ParentID == parentID)
        
        do {
            
            let result = try database.prepare(query)
            
            for row in result {
                
                var inHobby = false
                
                if row[appuserhobbytable[Expression<Int?>("HobbyID")]] != nil {
                    inHobby = true
                }
                
                hobbies.append(Hobby(   hobbyID: row[hobbyTable[HobbyID]],
                                        name: row[hobbyTable[Name]],
                                        type: row[hobbyTable[aType]],
                                        version: row[hobbyTable[Version]],
                                        parentID: row[hobbyTable[ParentID]],
                                        inHobby: inHobby)
                )
            }
        }catch {
            
            print(error)
        }
        
        return hobbies
    }
    

}
