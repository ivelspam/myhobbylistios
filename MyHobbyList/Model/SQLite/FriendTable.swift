import UIKit
import SQLite
import SwiftyJSON
class FriendTable{
    
    let friendTable = Table(TableN.friend)
    
    let AppUserID = TableE.AppUserID
    let EventID = TableE.EventID
    let ImageVersion = TableE.ImageVersion
    let Name = TableE.Name
    let State = TableE.State
    let UpdatedTS = TableE.UpdatedTS
    let CreatedTS = TableE.UpdatedTS
    let ReceivedTS = TableE.ReceivedTS
    let ClientUpdatedTS = TableE.ClientUpdatedTS
    
    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){
        print("createTable friendTable")
        
        let createTable = friendTable.create{(table) in
            table.column(AppUserID, primaryKey : true)
            table.column(EventID, defaultValue : -1)
            table.column(ImageVersion)
            table.column(Name)
            table.column(State)
            table.column(UpdatedTS)
            table.column(ReceivedTS, defaultValue: Date().MySQLiteFormat)
            table.column(ClientUpdatedTS, defaultValue : Date().MySQLiteFormat)
        }
        
        do {
            try database.run(createTable)
            print("Created FriendTable")
        }catch{
            print(error)
        }
    }
    
    func dropTable(){
        let dropTable = friendTable.drop(ifExists: true);
        
        do {
            
            try database.run(dropTable)
            print("drop friendTable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
        print("clearTable")
        do {
            try database.run(friendTable.delete())
            print("clearTable friendTable")
        }catch{
            print(error)
        }
    }
    
    func toString() -> String{
        var values = "Friend Table: \n"
        do {
            let stmt = try database.prepare("SELECT * FROM \(TableN.friend)")
            for row in stmt {
                for (index, columnName) in stmt.columnNames.enumerated(){
                    values.append("\(columnName)=\(row[index] ?? -1),\t")
                }
                values.append("\n")
            }
        } catch {
            print(error)
        }
        return values
    }
    
}

//DELETE
extension FriendTable {
    func funcDeleteFriend(appUserID : Int){
        do {
            let filter = friendTable.filter(TableE.AppUserID == appUserID)
            try database.run(filter.delete())
        }catch {
            print(error)
        }
    }
}

//Inserts

extension FriendTable {
    func insertOrReplace(friendFromMySQL : JSON){
        print("insertFriend")
        do {
            
            print(friendFromMySQL)
            
            try database.run(friendTable.insert(
                or: .replace,
                AppUserID <- friendFromMySQL[MySQLColumns.AppUserID].int!,
                ClientUpdatedTS <- Date().MySQLiteFormat,
                EventID <- friendFromMySQL[MySQLColumns.EventID].int ?? MagicNumbers.NOT_IN_EVENT,
                ImageVersion <- friendFromMySQL[MySQLColumns.ImageVersion].int!,
                Name <- friendFromMySQL[MySQLColumns.Name].string!,
                State <- friendFromMySQL[MySQLColumns.State].int!,
                UpdatedTS <- Date.MySQLToSQLite(dateString: friendFromMySQL[MySQLColumns.UpdatedTS].string!)
                )
            )
        }catch{
            print(error)
        }
        print(toString())
    }
    
    func insertOrReplace(friendsFromMySQL : JSON){
        print("func insertOrReplace(friends : JSON)")
        
        print(friendsFromMySQL)
        
        do {
            try database.transaction {
                
                
                for (_,friend):(String, JSON) in friendsFromMySQL {
                    insertOrReplace(friendFromMySQL: friend)
                }
            }
        }catch{
            print(error)
        }
        print(toString())
    }
}

//SELECT
extension FriendTable {
    func selectBy(clientUpdatedTS : String) -> [Friend]{
        print("func selectBy(clientUpdatedTS : String) -> [Friend]")
        
        var friends = [Friend]()
        do {
            let query = friendTable.filter(ClientUpdatedTS > clientUpdatedTS)
            for row in try database.prepare(query) {
                friends.append(Friend(row: row))
            }
        }catch{
            print(error)
        }
        return friends
    }
}


//UPDATE
extension FriendTable {
    func updateFriendState(appUserID : Int, state : Int){
        do {
            let filter = friendTable.filter(TableE.AppUserID == appUserID)
            try database.run(filter.update(TableE.State <- state))
        }catch {
            print(error)
        }
    }
    
}
