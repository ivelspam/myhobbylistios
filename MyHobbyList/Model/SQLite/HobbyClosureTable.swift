import UIKit
import SQLite
import SwiftyJSON

class HobbyClosureTable{
    
    let hobbyClosureTable = Table(TableN.hobbyclosure)
    let Ancestor = Expression<Int>("Ancestor")
    let Depth = Expression<Int>("Depth")
    let Descendant = Expression<Int>("Descendant")

    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){

        print("Creating hobbyClosureTable")
        
        let tableExec = "Create TABLE hobbyclosure(Ancestor INTEGER, Descendant INTEGER, Depth INTEGER, unique (Ancestor, Descendant))"
//
        do {
            
            try database.execute(tableExec)
            print("Created hobbyClosureTable")

        }catch{
            print(error)
        }
    }
    
    func dropTable(){
        let dropTable = self.hobbyClosureTable.drop(ifExists: true);
        
        do {
            
            try database.run(dropTable)
            print("drop hobbyClosureTable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
       
        do {
            
            try database.run(hobbyClosureTable.delete())
            print("clearTable hobbyClosureTable")
        }catch{
            print(error)
        }
    }
    
    
    func insertAllHobbyClosures(allHobbyClosures : JSON){
        print("insertAllHobbyClosures")
        
        for (_ ,hobbyClosure):(String, JSON) in allHobbyClosures {
           
            let ancestor = hobbyClosure["HobbyID"].int!
            var newDescendant = hobbyClosure["ParentID"].int ?? -1
            let hobbyID = hobbyClosure["HobbyID"].int!
            
            if newDescendant == -1 {
                newDescendant = ancestor
            }

            let query = "INSERT INTO hobbyclosure (Ancestor, Descendant, Depth) SELECT Ancestor, \(ancestor), (Depth + 1) FROM hobbyclosure WHERE descendant =\(newDescendant) UNION ALL SELECT \(hobbyID), \(hobbyID), 0";

           
            do{
                try database.execute(query)
            }catch{
                print(error)
            }
            
        }
    }
        
    func toString() -> String{
        
        var values = ""
        
        do {
            for hobbyClosure in try database.prepare(hobbyClosureTable) {
                values.append("Ancestor: \(hobbyClosure[Ancestor]), Descendant: \(hobbyClosure[Descendant]), Name: \(hobbyClosure[Depth])\n")
            }
        } catch {
            print(error)
        }
        
        return values
    }
   
}
