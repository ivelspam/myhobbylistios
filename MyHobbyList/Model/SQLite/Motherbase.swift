//
//  Motherbase.swift
//  MyHobbyList
//
//  Created by Nicholas on 5/6/18.
//  Copyright © 2018 Nicholas. All rights reserved.
//

import UIKit
import SQLite
import SwiftyJSON

enum TablesEnum {
    case APPUSER
    case FRIEND
    case CHAT
    case EVENT
    case HOBBY
}

class Motherbase {
    
    var database : Connection!
    
    init(){
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
            ).first!
        
        do{
           
            let url = NSURL(fileURLWithPath: "\(path)")
            if let pathComponent = url.appendingPathComponent("db.sqlite3") {
                let filePath = pathComponent.path
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
//                    print("FILE AVAILABLE")
                    database = try Connection("\(path)/db.sqlite3")
                } else {
//                    print("FILE NOT AVAILABLE")
                    database = try Connection("\(path)/db.sqlite3")
                    createTables()
                }
            } else {
                print("FILE PATH NOT AVAILABLE")
            }
        }catch{
            print(error)
        }
        
    }
    
    func getFriendTable()-> FriendTable{
        return FriendTable(connection: database)
    }
    
    func getHobbyTable()-> HobbyTable{
        return HobbyTable(connection: database)
    }
    
    func getAppUserHobbyTable()-> AppuserHobbyTable{
        return AppuserHobbyTable(connection: database)
    }
    
    func getClosureTable()-> HobbyClosureTable{
        return HobbyClosureTable(connection: database)
    }
    
    func getChatTable()-> ChatTable{
        return ChatTable(connection: database)
    }
    
    func getEventTable()-> EventTable{
        return EventTable(connection: database)
    }
    
    
    func createTables(){
    
        FriendTable(connection : database).createTable()
        HobbyTable(connection : database).createTable()
        HobbyClosureTable(connection : database).createTable()
        AppuserHobbyTable(connection: database).createTable()
        ChatTable(connection: database).createTable()
        EventTable(connection: database).createTable()
    }
    
    func recreateTables(){
        
        dropTables()
        createTables()
        
    }
    
    func dropTables(){
        FriendTable(connection : database).dropTable()
        HobbyTable(connection : database).dropTable()
        HobbyClosureTable(connection : database).dropTable()
        AppuserHobbyTable(connection: database).dropTable()
        ChatTable(connection: database).dropTable()
        EventTable(connection: database).dropTable()
        
    }
    
    func clearTables(){
        FriendTable(connection : database).clearTable()
        HobbyTable(connection : database).clearTable()
        HobbyClosureTable(connection : database).clearTable()
        AppuserHobbyTable(connection: database).clearTable()
        ChatTable(connection: database).clearTable()
        EventTable(connection: database).clearTable()
    }
    
}
