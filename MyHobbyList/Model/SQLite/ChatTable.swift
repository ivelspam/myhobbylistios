import UIKit
import SQLite
import SwiftyJSON

class ChatTable{
    
    let chattable = Table(TableN.chat)
    
    let ChatID = TableE.ChatID
    let ClientUpdatedTS = TableE.CreatedTS
    let CreatedTS = TableE.CreatedTS
    let Message = TableE.Message
    let Owner = TableE.Owner
    let Receiver = TableE.Receiver
    let ReceivedTS = TableE.ReceivedTS
    let SentDatetime = TableE.SentDatetime
    let State = TableE.State
    let aUUID = TableE.aUUID

    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){
        print("Creating chattable")
        
        let createTable = chattable.create{(table) in
            table.column(ChatID, defaultValue: -1)
            table.column(ClientUpdatedTS, defaultValue: Date().MySQLiteFormat)
            table.column(CreatedTS)
            table.column(Owner)
            table.column(Message)
            table.column(Receiver)
            table.column(ReceivedTS, defaultValue: Date().MySQLiteFormat)
            table.column(SentDatetime)
            table.column(State, defaultValue: 0)
            table.column(aUUID, primaryKey: true)
        }

        do {
            
            try database.run(createTable)
            print("Created chattable")

        }catch{
            print(error)
        }
    }
    
    func dropTable(){
        let dropTable = chattable.drop(ifExists: true);
        
        do {
            try database.run(dropTable)
            print("drop chattable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
        
        do {
            try database.run(chattable.delete())
            print("clearTable chattable")
        }catch{
            print(error)
        }
    }
    

    
    func toString() -> String{
        var values = ""
        
        do {
            let stmt = try database.prepare("SELECT * FROM \(TableN.chat)")
            for row in stmt {
                for (index, columnName) in stmt.columnNames.enumerated(){
                    values.append("\(columnName)=\(row[index]!), ")
            }
                values.append("\n")
            }
        } catch {
            print(error)
        }
        
        return values
    }
    
}

//INSERT
extension ChatTable {
    func insertOrReplace(chatsFromMySQL : JSON){
        print("insertAllchats")
        
        do{
            try database.transaction {
                for (_ ,chatMessage):(String, JSON) in chatsFromMySQL {
                    insertOrReplace(chatFromMySQL: chatMessage)
                }
            }
        }catch{
            print(error)
        }
        print("insert all chats done")
    }
    
    func insertOrReplace(chatFromMySQL : JSON){
        print("insertNewMessages")
        
        do{
            try database.run(self.chattable.insert(
                or: .replace,
                ChatID <- chatFromMySQL["ChatID"].int!,
                CreatedTS <- Date.MySQLToSQLite(dateString : chatFromMySQL["CreatedTS"].string!),
                Message <- chatFromMySQL["Message"].string!,
                Owner <- chatFromMySQL["Owner"].int!,
                Receiver <- chatFromMySQL["Receiver"].int!,
                SentDatetime <- Date.MySQLToSQLite(dateString: chatFromMySQL["SentDatetime"].string!),
                State <- chatFromMySQL["State"].int!,
                aUUID <- chatFromMySQL["UUID"].string!
                )
            )
        }catch{
            print(error)
        }
    }
    
    
    
    
    func insertOrReplace(chat : Chat){
        print("insertAChatMessage")
        do{
            let insert = chattable.insert(
                ChatID <- chat.ChatID,
                CreatedTS <- chat.CreatedTS.MySQLiteFormat,
                Owner <- chat.Owner,
                Message <- chat.Message,
                Receiver <- chat.Receiver,
                State <- chat.State,
                SentDatetime <- chat.SentDatetime.MySQLiteFormat,
                aUUID <- chat.aUUID
            )
            try database.run(insert)
        }catch{
            print(error)
        }
    }
}

//SELECT
extension ChatTable {
    func getFriendChatMessages(owner : Int, receiver : Int, limit : Int) -> [Chat]{
        print("getFriendChatMessages")
        var chatMessages : [Chat] = []
        
        do{
            let rows = try database.prepare(
                chattable.filter(
                    (TableE.Owner == owner && TableE.Receiver == receiver) ||
                        (TableE.Owner == receiver && TableE.Receiver == owner)
                    ).order(ChatID.asc)
            )
            
            for row in rows{
                chatMessages.append(Chat(row : row))
            }
        }catch{
            print(error)
        }
        return chatMessages
    }
    
    func selectLatestChatID() -> String{
        
        do {
            let query = chattable.order(CreatedTS.desc).limit(1)
            
            for row in try database.prepare(query) {
                return row[CreatedTS]
            }
            
        }catch{
            print(error)
        }
        return MagicNumbers.LOWEST_TS
    }
    
    func selectLatestID(friendAppUserID : Int) -> Int{
        do{
            print("getLastChatTime")
            if let max = try database.scalar(chattable.select(ChatID.max)){
                return max
            }
        }catch{
            print(error)
        }
        return MagicNumbers.NO_ID_FOUND
    }
}

//UPDATE
extension ChatTable {
    func update(chatMessage : Chat){
        do{
            let filter = chattable.filter(aUUID == chatMessage.aUUID)
            
            try database.run(filter.update([
                    ChatID <- chatMessage.ChatID,
                    ClientUpdatedTS <- Date().MySQLiteFormat,
                    CreatedTS <- chatMessage.CreatedTS.MySQLiteFormat,
                    Owner <- chatMessage.Owner,
                    Message <- chatMessage.Message,
                    Receiver <- chatMessage.Receiver,
                    State <- chatMessage.State,
                    SentDatetime <- chatMessage.SentDatetime.MySQLiteFormat
                ]))
        }catch{
            print(error)
        }
    }
}

