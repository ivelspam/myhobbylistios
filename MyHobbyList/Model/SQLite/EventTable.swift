import UIKit
import SQLite
import SwiftyJSON

class EventTable{
    
    let eventtable = Table(TableN.event)
    
    let Address = TableE.Address
    let AppUserID = TableE.AppUserID
    let AppUserName = TableE.AppUserName
    let CreatedTS = TableE.CreatedTS
    let ClientUpdatedTS = TableE.ClientUpdatedTS
    let Description = TableE.Description
    let EndDatetime = TableE.EndDatetime
    let EventID = TableE.EventID
    let HobbyID = TableE.HobbyID
    let HobbyName = TableE.HobbyName
    let Latitude = TableE.Latitude
    let Longitude = TableE.Longitude
    let Name = TableE.Name
    let PlaceName = TableE.PlaceName
    let QuantityOfPeople = TableE.QuantityOfPeople
    let Reach = TableE.Reach
    let ReceivedTS = TableE.ReceivedTS
    let StartDatetime = TableE.StartDatetime
    let State = TableE.State
    let UpdatedTS = TableE.UpdatedTS

    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){
        print("Creating Event Table")
        let createTable = eventtable.create{(table) in
            table.column(Address)
            table.column(AppUserID)
            table.column(AppUserName)
            table.column(ClientUpdatedTS, defaultValue : Date().MySQLiteFormat)
            table.column(CreatedTS)
            table.column(Description)
            table.column(EndDatetime)
            table.column(EventID, primaryKey : true)
            table.column(HobbyID)
            table.column(HobbyName)
            table.column(Latitude)
            table.column(Longitude)
            table.column(Name)
            table.column(PlaceName)
            table.column(QuantityOfPeople)
            table.column(Reach)
            table.column(ReceivedTS, defaultValue : Date().MySQLiteFormat)
            table.column(StartDatetime)
            table.column(State)
            table.column(UpdatedTS)
        }
        
        //
        do {
            
            try database.run(createTable)
            print("Created eventtable")

        }catch{
            print(error)
        }
    }
    
    func dropTable(){
        let dropTable = eventtable.drop(ifExists: true);
        do {
            try database.run(dropTable)
            print("drop eventtable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
        
        do {
            
            try database.run(eventtable.delete())
            print("clearTable appuserhobbytable")
        }catch{
            print(error)
        }
    }

    func toString() -> String{
        var values = "Event Table:\n"
        
        do {
            let stmt = try database.prepare("SELECT * FROM \(TableN.event)")
            for row in stmt {
                for (index, columnName) in stmt.columnNames.enumerated(){
                    values.append("\(columnName)=\(row[index]!), ")
                }
                values.append("\n")
            }
        } catch {
            print(error)
        }
        
        return values
    }
}



//Insert
extension EventTable {
    
    
    func insertOrIgnoreNew(eventsFromMySQL : JSON){
        print("insertOrIgnoreNew(eventsFromMySQL : JSON)")
        
        do{
            try database.transaction {
                for (_ ,event):(String, JSON) in eventsFromMySQL {
                    insertOrIgnoreEvent(fromMySQL: event)
                }
            }
            
        }catch{
            print(error)
        }
        
        print(toString())
    }
    
    func insertOrIgnoreEvent(fromMySQL : JSON){
        print("insertOrIgnoreEvent(fromMySQL : JSON)")
       
        do{
            try database.run(eventtable.insert(
                or: .ignore,
                    Address <- fromMySQL[MySQLColumns.Address].string!,
                    AppUserID <- fromMySQL[MySQLColumns.AppUserID].int!,
                    AppUserName <- fromMySQL[MySQLColumns.AppUserName].string!,
                    CreatedTS <- Date.MySQLToSQLite(dateString: fromMySQL[MySQLColumns.CreatedTS].string!),
                    Description <- fromMySQL[MySQLColumns.Description].string!,
                    EndDatetime <- Date.MySQLToSQLite(dateString: fromMySQL[MySQLColumns.EndDatetime].string!),
                    EventID <- fromMySQL[MySQLColumns.EventID].int!,
                    HobbyID <- fromMySQL[MySQLColumns.HobbyID].int!,
                    HobbyName <- fromMySQL[MySQLColumns.HobbyName].string!,
                    Latitude <- String(fromMySQL[MySQLColumns.Latitude].double!),
                    Longitude <- String(fromMySQL[MySQLColumns.Longitude].double!),
                    Name <- fromMySQL[MySQLColumns.Name].string!,
                    PlaceName <- fromMySQL[MySQLColumns.PlaceName].string!,
                    QuantityOfPeople <- fromMySQL[MySQLColumns.QuantityOfPeople].int!,
                    Reach <- fromMySQL[MySQLColumns.Reach].int!,
                    StartDatetime <- Date.MySQLToSQLite(dateString: fromMySQL[MySQLColumns.StartDatetime].string!),
                    State <- fromMySQL[MySQLColumns.State].int!,
                    UpdatedTS <- Date.MySQLToSQLite(dateString: fromMySQL[MySQLColumns.UpdatedTS].string!)
                )
            )
        }catch{
            print(error)
        }
        
        print(toString())
    }
    
}

//SELECT

extension EventTable {
    
    func selectNonClosedEvents() -> [Event]{
        print("getEventsNonClosedEvents")
        var events : [Event] = []
        print(toString())
        do{
            let filter = eventtable.filter(TableE.State <= 1)
            let rows = try database.prepare(filter)
            for row in rows{
                print(row)
                events.append(Event(row : row))
            }
        }catch{
            print(error)
        }
        return events
    }
    
    func selectEventsBy(state : Int) -> [Event]{
        var events : [Event] = []
        
        do{
            let filter = eventtable.filter(TableE.State == state)
            let rows = try database.prepare(filter)
            for row in rows{
                events.append(Event(row : row))
            }
        }catch{
            print(error)
        }
        return events
    }
    
    func selectEventsBy(clientUpdatedTS : String) -> [Event]{
        var events : [Event] = []
        
        do{
            let filter = eventtable.filter(TableE.ClientUpdatedTS > clientUpdatedTS)
            let rows = try database.prepare(filter)
            for row in rows{
                events.append(Event(row : row))
            }
        }catch{
            print(error)
        }
        return events
    }

    func selectEventBy(eventID : Int) -> Event?{
        print("selectEvent")
        
        do{
            let filter = eventtable.filter(TableE.EventID == eventID)
            let rows = try database.prepare(filter)
            for row in rows{
                return Event(row : row)
            }
        }catch{
            print(error)
        }
        return nil
    }
    
    func selectAllEvents() -> [Event]{
        var events : [Event] = []
        print(toString())
        
        do{
            let rows = try database.prepare(eventtable)
            for row in rows{
                print(row)
                
                events.append(Event(row : row))
            }
        }catch{
            print(error)
        }
        return events
    }
}

//update

extension EventTable {
    
    func update(eventsChanges : JSON){
        print("update(eventsChanges : JSON)")
        
        do{
            try database.transaction {
                for (_ ,eventChange):(String, JSON) in eventsChanges {
                    update(event: eventChange)
                }
            }
        }catch{
            print(error)
        }
    }
    
    func update(event : JSON){
        print("update(event : JSON)")
        
        let selectedUpdate = eventtable.filter(EventID == event[MySQLColumns.EventID].int!).limit(1)
        
        do{
            try database.run(selectedUpdate.update(
                    [
                        State <- event[MySQLColumns.State].int!,
                        QuantityOfPeople <- event[MySQLColumns.QuantityOfPeople].int!,
                        ClientUpdatedTS <- Date().MySQLiteFormat
                    ]
                )
            )
        }catch{
            print(error)
        }
    }
    
    func updateState(from eventID : Int, state: Int){
        do {
            let filter = eventtable.filter(TableE.EventID == eventID)
            try database.run(filter.update(State <- state))
        }catch{
            print(error)
        }
    }
    
    
    
}

