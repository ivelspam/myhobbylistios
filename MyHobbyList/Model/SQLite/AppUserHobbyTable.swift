import UIKit
import SQLite
import SwiftyJSON

class AppuserHobbyTable{
    
    let appuserhobbytable = Table(TableN.appuserhobby)
    let HobbyID = TableE.HobbyID
    let ReceivedTS = TableE.ReceivedTS
    let ClientUpdatedTS = TableE.ClientUpdatedTS

    var database : Connection!
    
    init (connection : Connection){
        self.database = connection
    }
    
    func createTable(){
  
        print("Creating appuserhobbytable")
        
        let createTable = appuserhobbytable.create{(table) in
            table.column(ClientUpdatedTS, defaultValue : Date().MySQLiteFormat)
            table.column(HobbyID, primaryKey: true)
            table.column(ReceivedTS, defaultValue : Date().MySQLiteFormat)

        }

        do {
            
            try database.run(createTable)
            print("appuserhobbytable")
        }catch{
            print(error)
        }
    }
    
    func selectAppUserHobbyComplete() -> [Hobby]{
        
        print("func selectAppUserHobbyComplete() -> [Hobby]")
        
        var hobbies : [Hobby] = []
        do{
            
            let hobbyTable = Motherbase().getHobbyTable().hobbyTable
            
            let filter = appuserhobbytable
                .join(.leftOuter, hobbyTable, on: hobbyTable[HobbyID] == appuserhobbytable[HobbyID])
                .filter(hobbyTable[TableE.aType] == TableValue.child)
            
            let stmt = try database.prepare(filter)
            for row in stmt {
                hobbies.append(Hobby(
                    hobbyID : row[appuserhobbytable[HobbyID]],
                    name : row[hobbyTable[TableE.Name]],
                    type : row[hobbyTable[TableE.aType]],
                    version : row[hobbyTable[TableE.Version]],
                    parentID : row[hobbyTable[TableE.ParentID]],
                    inHobby : true
                ))
            }
        }catch{
         print(error)
        }
        return hobbies
    }
    
    func dropTable(){
        let dropTable = appuserhobbytable.drop(ifExists: true);
        do {
            try database.run(dropTable)
            print("drop appuserhobbytable")
        }catch{
            print(error)
        }
    }
    
    func clearTable(){
        
        do {
            try database.run(appuserhobbytable.delete())
            print("clearTable appuserhobbytable")
        }catch{
            print(error)
        }
    }
    
    func getParentNodes() -> [Hobby] {
    print("getParentNodes")
        
        var hobbies : [Hobby] = []
        do{
            
            let hobbyTable = Motherbase().getHobbyTable().hobbyTable
            let hobbyClosureTable = Motherbase().getClosureTable().hobbyClosureTable
            
            let filter = appuserhobbytable
                .join(.leftOuter, hobbyClosureTable, on: hobbyClosureTable[TableE.Descendant] == appuserhobbytable[HobbyID])
                .join(.leftOuter, hobbyTable, on: hobbyTable[HobbyID] == hobbyClosureTable[TableE.Ancestor])
                .filter(hobbyTable[TableE.ParentID] == -1 )
                .group(TableE.Ancestor)
            
            for row in try database.prepare(filter) {
                print(row)
                hobbies.append(Hobby(
                    hobbyID : row[hobbyClosureTable[TableE.Ancestor]],
                    name : row[hobbyTable[TableE.Name]],
                    parentID : row[hobbyTable[TableE.ParentID]],
                    type : row[hobbyTable[TableE.aType]],
                    version : row[hobbyTable[TableE.Version]]
                ))
                
            }
            
        }catch{
            print(error)
        }
        return hobbies;
    }
    
    func getHobbiesWithInfo() -> [Hobby] {
        print("getHobbies")
        
        let query = """
            SELECT \(TableN.hobby).HobbyID, Name, Type FROM \(TableN.appuserhobby)
            LEFT JOIN \(TableN.hobby) ON \(TableN.appuserhobby).\(Columns.HobbyID)=\(TableN.hobby).\(Columns.HobbyID)
        """
        
        print(query)
        
        var hobbies : [Hobby] = []
        do{
            let stmt = try database.prepare(query)
            for row in stmt {
                print(row)
                hobbies.append(Hobby(hobbyID: row[0] as! Int64, name: row[1] as! String, type: row[2] as! String, parentID : -1))
            }
        }catch{
            print(error)
        }
        return hobbies;
    }
    
    func getHobbiesByLayers(hobbyID : Int) -> [Hobby]{
        print("getHobbiesByLayers()");
        print("hobbyID:  \(hobbyID)")
        
        var hobbies : [Hobby] = []
        do{
            
            let hobbyTable = Motherbase().getHobbyTable().hobbyTable
            let hobbyClosureTable = Motherbase().getClosureTable().hobbyClosureTable
            let filter = appuserhobbytable
                .join(.leftOuter, hobbyClosureTable, on: appuserhobbytable[HobbyID] == hobbyClosureTable[TableE.Descendant])
                .join(.leftOuter, hobbyTable, on: hobbyTable[HobbyID] == hobbyClosureTable[TableE.Ancestor])
                .filter(hobbyTable[TableE.ParentID] == hobbyID)
                .group(TableE.Ancestor)

            for row in try database.prepare(filter) {
                print("ROW")

                hobbies.append(Hobby(
                    hobbyID : row[hobbyTable[HobbyID]],
                    name : row[hobbyTable[TableE.Name]],
                    type : row[hobbyTable[TableE.aType]],
                    version : row[hobbyTable[TableE.Version]],
                    parentID : row[hobbyTable[TableE.ParentID]],
                    inHobby : true
                ))
            }
            
        }catch{
            print(error)
        }
        return hobbies;
    }
    
    
    func selectHobbyLayerParent(hobbyID : Int) -> [Hobby]{
        print("getHobbiesByLayers()");
        
        var hobbies : [Hobby] = []
        do{
            
            let hobbyTable = Motherbase().getHobbyTable().hobbyTable
            let hobbyClosureTable = Motherbase().getClosureTable().hobbyClosureTable
            
            let filter = appuserhobbytable
                .join(.leftOuter, hobbyClosureTable, on: hobbyClosureTable[TableE.Descendant] == appuserhobbytable[HobbyID])
                .join(.leftOuter, hobbyTable, on: hobbyTable[HobbyID] == hobbyClosureTable[TableE.Descendant])
                .filter(hobbyClosureTable[TableE.Ancestor] == hobbyID)
            
            let stmt = try database.prepare(filter)
            for row in stmt {
                hobbies.append(Hobby(
                    hobbyID : row[appuserhobbytable[HobbyID]],
                    name : row[hobbyTable[TableE.Name]],
                    type : row[hobbyTable[TableE.aType]],
                    version : row[hobbyTable[TableE.Version]],
                    parentID : row[hobbyTable[TableE.ParentID]],
                    inHobby : true
                ))
            }
            
        }catch{
            print(error)
        }
        return hobbies;
    }
    
    func insertAllAppUserHobbies(allAppUserHobbies : JSON){
        print("insertAllHobbyClosures")
        do{
        
            try database.run(appuserhobbytable.delete())
            
            for (_ ,appUserHobby):(String, JSON) in allAppUserHobbies {
                    try database.run(self.appuserhobbytable.insert(
                        or: .ignore,
                            HobbyID <- appUserHobby["HobbyID"].intValue
                        )
                )
            }
        }catch{
            print(error)
        }
    }
    
    func insertAppUserHobby(hobbyID : Int){
        
        print("insertAppUserHobby \(hobbyID)")

        do{
            try database.run(appuserhobbytable.insert(HobbyID <- hobbyID))
        }catch{
            print(error)
        }
    }
    
    
    func deleteAppUserHobby(hobbyID : Int){
        do{
            
            print("deleteAppUserHobby \(hobbyID)")
            
            let appUserHobbyFilter = appuserhobbytable.filter(HobbyID == hobbyID)
            try database.run(appUserHobbyFilter.delete())
        }catch{
            print(error)
        }
    }
    
    
    
    func toString() -> String{
        var values = ""
        
        do {
            for appUserHobby in try database.prepare(appuserhobbytable) {
                values.append("HobbyID: \(appUserHobby[HobbyID])\n")
            }
        } catch {
            print(error)
        }
        
        return values
    }
    
    func testTypes(temp : Any?){
        
        if temp is Double {
            print("Double type")
        } else if temp is Int {
            print("Int type")
        } else if temp is Float {
            print("Float type")
        } else if temp is Int64 {
            print("Int64 type")
        }else if temp is String {
            print("String type")
        } else {
            print("Unkown type")
        }
    }
    
}
