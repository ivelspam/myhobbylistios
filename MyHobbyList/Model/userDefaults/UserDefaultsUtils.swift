import Foundation
import SwiftyJSON
import JWTDecode

enum DefaultKey : String{
    case AppUserID_int = "AppUserID"
    case Token_str = "Token_str"
    case Name_str = "Name_str"
    case Email_str = "Email_str"
    case dob_str = "dob_str"
    case Username_str = "Username_str"
    case Distance_int = "Distance_int"
    case MeasureSystem_str = "MeasurementSystem_str"
    case EventID_int = "EventID_int"
    case ImageVersion_int = "ImageVersion_int"
    case UpdatedTS_str = "UpdatedTS_str"
    case latestChatID_str = "latestChatID_str"
    case eventUpdatedTS_str = "eventUpdatedTS_str"
    case inEventUpdatedTS_str = "inEventUpdatedTS_str"
    case friendUpdatedTS_str = "friendUpdatedTS_str"
}

class UserDefaultsUtils{

    static let keysArray = [    DefaultKey.AppUserID_int,
                                DefaultKey.Token_str,
                                DefaultKey.Name_str,
                                DefaultKey.Email_str,
                                DefaultKey.dob_str,
                                DefaultKey.Username_str,
                                DefaultKey.Distance_int,
                                DefaultKey.MeasureSystem_str,
                                DefaultKey.EventID_int,
                                DefaultKey.ImageVersion_int,
                                DefaultKey.UpdatedTS_str,
                                DefaultKey.latestChatID_str,
                                DefaultKey.eventUpdatedTS_str,
                                DefaultKey.inEventUpdatedTS_str,
                                DefaultKey.friendUpdatedTS_str
        ]
    
    
    static func fillTheUserDefaults(allInfoJSON : JSON){
        
        print(allInfoJSON)
        if allInfoJSON.string != "no change"{
            do{
                let jwt = try decode(jwt: UserDefaults.standard.string(forKey: DefaultKey.Token_str.rawValue)!)
                set(value: jwt.body["AppUserID"]!, key: .AppUserID_int)
                updateUserDefault(AppUserChange: allInfoJSON["user"])
            }catch{
                print(error)
            }
            
        }else{
            print("aqui")
        }
    }
    
    static func updateUserDefault(AppUserChange : JSON){
        
        print("updateUserDefault")
        print(AppUserChange)
        
        set(value: AppUserChange[MySQLColumns.Name].string, key: .Name_str)
        set(value: AppUserChange[MySQLColumns.Email].string, key: .Email_str)
        set(value: AppUserChange[MySQLColumns.DOB].string, key: .dob_str)
        set(value: AppUserChange[MySQLColumns.Username].string, key: .Username_str)
        set(value: AppUserChange[MySQLColumns.MeasureSystem].string, key: .MeasureSystem_str)
        set(value: AppUserChange[MySQLColumns.Distance].int, key: .Distance_int)
        set(value: AppUserChange[MySQLColumns.ImageVersion].int, key: .ImageVersion_int)
        set(value: AppUserChange[MySQLColumns.UpdatedTS].string, key: .UpdatedTS_str)
        set(value: AppUserChange[MySQLColumns.EventID].string, key: .EventID_int)

    }
    
    static func clearTheUserDefaults(){
        
        removeObject(key: .AppUserID_int)
        removeObject(key: .AppUserID_int)
        removeObject(key: .Token_str)
        removeObject(key: .Name_str)
        removeObject(key: .Email_str)
        removeObject(key: .dob_str)
        removeObject(key: .Username_str)
        removeObject(key: .Distance_int)
        removeObject(key: .MeasureSystem_str)
        removeObject(key: .EventID_int)
        removeObject(key: .ImageVersion_int)
        removeObject(key: .UpdatedTS_str)
    }
    
    static func removeObject(key : DefaultKey){
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
    
    static func set(value: Any?, key: DefaultKey){
        
        switch key {
            case .AppUserID_int:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .Token_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .Name_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .Email_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .dob_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .Username_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .Distance_int:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .MeasureSystem_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .EventID_int:
                UserDefaults.standard.set(value ?? MagicNumbers.NOT_IN_EVENT, forKey: key.rawValue)
            case .ImageVersion_int:
                UserDefaults.standard.set(value ?? MagicNumbers.NO_PICTURE, forKey: key.rawValue)
            case .UpdatedTS_str:
                UserDefaults.standard.set(Date.MySQLToSQLite(dateString: value as! String), forKey: key.rawValue)
            case .latestChatID_str:
                UserDefaults.standard.set(value, forKey: key.rawValue)
            case .eventUpdatedTS_str:
                UserDefaults.standard.set(Date.MySQLToSQLite(dateString: value as! String), forKey: key.rawValue)
            case .inEventUpdatedTS_str:
                UserDefaults.standard.set(Date.MySQLToSQLite(dateString: value as! String), forKey: key.rawValue)
            case .friendUpdatedTS_str:
                UserDefaults.standard.set(Date.MySQLToSQLite(dateString: value as! String), forKey: key.rawValue)
        }
    }
    
    
    static func updateUpdatedTS(jsonValue: JSON, key: DefaultKey){
        print("updateUpdatedTS")
        
        print(key.rawValue)
        switch key {
        case .inEventUpdatedTS_str:
            var updatedTS = MagicNumbers.LOWEST_MYSQL_TS
            
            if jsonValue.exists() {
                updatedTS = jsonValue[MySQLColumns.UpdatedTS].string!
            }
            updateHigherValue(key: key, value : updatedTS)

        case .UpdatedTS_str:
            var updatedTS = MagicNumbers.LOWEST_MYSQL_TS
            
            if jsonValue.exists() {
                updatedTS = jsonValue[MySQLColumns.UpdatedTS].string!
            }
            updateHigherValue(key: key, value : updatedTS)
        case .latestChatID_str:
            var value = -1
            for (_ , item):(String, JSON) in jsonValue {
                if value < item[MySQLColumns.ChatID].int! {
                   value = item[MySQLColumns.ChatID].int!
                }
            }
            updateHigherValue(key: key, value : value)
        default:
            var date = MagicNumbers.LOWEST_MYSQL_TS
            print(jsonValue)
            
            if jsonValue.exists() {
                for (_ , value):(String, JSON) in jsonValue {
                   if value[MySQLColumns.UpdatedTS].string!.caseInsensitiveCompare(date) == ComparisonResult.orderedAscending {
                        date = value[MySQLColumns.UpdatedTS].string!
                   }
                }
            }
            updateHigherValue(key: key, value : date)
        }
    }
    
    static func updateHigherValue(key: DefaultKey, value : Int){
        if(isKeyPresentInUserDefaults(key: key)){
            
            if UserDefaults.standard.integer(forKey: key.rawValue) < value {
                set(value: value, key: key )
            }
        }else{
            set(value: value, key: key)
        }
    }
    
    static func updateHigherValue(key: DefaultKey, value : String){
        if(isKeyPresentInUserDefaults(key: key)){
            
            if UserDefaults.standard.string(forKey: key.rawValue)! < value {
                set(value: value, key: key)
            }
        }else{
            set(value: value, key: key)
        }
    }
    
    static func isKeyPresentInUserDefaults(key: DefaultKey) -> Bool {
        return UserDefaults.standard.object(forKey: key.rawValue) != nil
    }
    
    static func toString() -> String{
        
        var value = ""
        for key in keysArray {
            value.append("\(key.rawValue): \(UserDefaults.standard.string(forKey: key.rawValue) ?? "No Value")\n")
        }
        return value
    }
    
}
