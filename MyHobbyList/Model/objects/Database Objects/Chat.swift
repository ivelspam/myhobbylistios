import UIKit
import SQLite
import SwiftyJSON

enum ChatMessageState : Int {
    
    case send = 0
    case ServerReceived = 1
    case SentFirebase = 2
    case AppReceived = 3
    case Read = 4
}

class Chat{
    
    var ChatID : Int = -1
    var aUUID : String = UUID().uuidString
    var Owner : Int
    var Receiver : Int
    var State : Int = 0
    var Message : String
    var CreatedTS : Date
    var SentDatetime : Date
    
    init(row : Row){
        print(row)
        ChatID = row[TableE.ChatID]
        aUUID = row[TableE.aUUID]
        Owner = row[TableE.Owner]
        Receiver = row[TableE.Receiver]
        State = row[TableE.State]
        Message = row[TableE.Message]
        CreatedTS = Date(fromSQLiteString : row[TableE.CreatedTS])
        SentDatetime = Date(fromSQLiteString : row[TableE.SentDatetime])
    }
    
    init(receiver : Int, message : String){
        Owner = UserDefaults.standard.integer(forKey: DefaultKey.AppUserID_int.rawValue)
        Receiver = receiver
        Message = message
        aUUID = UUID().uuidString
        let date = Date()
        CreatedTS = date
        SentDatetime = date
    }
    
    init(fromMySQLChatMessageJSON : JSON){
        ChatID = fromMySQLChatMessageJSON["ChatID"].int!
        aUUID = fromMySQLChatMessageJSON["UUID"].string!
        Owner = fromMySQLChatMessageJSON["Owner"].int!
        Receiver = fromMySQLChatMessageJSON["Receiver"].int!
        State = fromMySQLChatMessageJSON["State"].int!
        Message = fromMySQLChatMessageJSON["Message"].string!
        CreatedTS = Date(fromMySQLString: fromMySQLChatMessageJSON["CreatedTS"].string!)
        SentDatetime = Date(fromMySQLString: fromMySQLChatMessageJSON["SentDatetime"].string!)
    }
    
//    init(owner : Int, receiver : Int, state : Int, message : String, datetime : String){
//        Owner = owner
//        Receiver = receiver
//        State = state
//        Message = message
//        Datetime = datetime
//    }
    
    func getUUID() -> String{
        return UUID().uuidString
    }
    
    func getDictionary() -> Dictionary<String, Any>{
        
        var dictionary = Dictionary<String, Any>()
        dictionary["Owner"] = Owner
        dictionary["UUID"] = aUUID
        dictionary["Receiver"] = Receiver
        dictionary["State"] = State
        dictionary["Message"] = Message
        dictionary["CreatedTS"] = CreatedTS
        dictionary["SentDatetime"] = SentDatetime
        
        return dictionary
    }
    
    func getMySQLDictionary() -> Dictionary<String, Any>{
        print("getMySQLDictionary")

        var dictionary = Dictionary<String, Any>()
        dictionary["Owner"] = Owner
        dictionary["UUID"] = aUUID
        dictionary["Receiver"] = Receiver
        dictionary["State"] = State
        dictionary["Message"] = Message
        dictionary["CreatedTS"] = CreatedTS.MySQLiteFormat
        dictionary["SentDatetime"] = SentDatetime.MySQLiteFormat
        return dictionary
    }
    
    func getParameters() -> [String : String]{
        return [
                "Receiver" : String(Receiver),
                "Message" : Message,
                "SentDatetime" : SentDatetime.MySQLiteFormat,
                "UUID" : aUUID
        ]
    }
}
