import Foundation
import SwiftyJSON


class EventComment{
    
    var EventCommentID : Int = -1
    var EventID : Int
    var Comment : String
    var AppUserID : Int
    var CreatedTS : Date = Date()
//    var aUUID : String = UUID.init().uuidString
    var Name : String?
    var ImageVersion : Int

    
    init(jsonEventCommentFromMySQL : JSON){
        EventCommentID = jsonEventCommentFromMySQL["EventCommentID"].int!
        EventID = jsonEventCommentFromMySQL["EventID"].int!
        Comment = jsonEventCommentFromMySQL["Comment"].string!
        AppUserID = jsonEventCommentFromMySQL["AppUserID"].int!
        CreatedTS = Date(fromMySQLString: jsonEventCommentFromMySQL["CreatedTS"].string!)
//        aUUID = jsonEventCommentFromMySQL["UUID"].string!
        Name = jsonEventCommentFromMySQL["Name"].string!
        ImageVersion = jsonEventCommentFromMySQL["ImageVersion"].intValue

    }
   
    init(comment : String, eventID : Int, appUserID : Int){
        Comment = comment
        AppUserID = appUserID
        EventID = eventID
        Name = UserDefaults.standard.string(forKey: DefaultKey.Name_str.rawValue)
        ImageVersion = 0
    }
    
    func getMySQLDictionary() -> Dictionary<String, Any>{
        
        var dictionary = Dictionary<String, Any>()
        dictionary["EventID"] = EventID
        dictionary["Comment"] = Comment
        dictionary["AppUserID"] = AppUserID
//        dictionary["aUUID"] = aUUID
        return dictionary
    }
    
}
