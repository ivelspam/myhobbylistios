import Foundation
import SwiftyJSON
import SQLite

class Event{
    
    enum ReachOptions : Int {
        case Everyone = 0
        case Friends = 1
        case FriendsAndTheirsFriends = 2
    }
    
    enum EventState : Int {
        case Created = 0
        case Hided = 1
        case Ended = 2
        case Expired = 3
        case Deleted = 4

    }
    
    var Address : String
    var AppUserID : Int
    var AppUserName : String
    var ClientUpdatedTS : String
    var CreatedTS : Date
    var Description : String
    var EndDatetime : Date
    var EventID : Int
    var HobbyName : String
    var HobbyID : Int
    var Latitude : Double
    var Longitude : Double
    var Name : String
    var PlaceName : String
    var QuantityOfPeople : Int
    var Reach : ReachOptions
    var ReceivedTS : String
    var State : EventState
    var StartDatetime : Date
    var UpdatedTS : Date

    
//    init(eventMySQLJSON : JSON){
//        Address = eventMySQLJSON[MySQLColumns.Address].string!
//        AppUserID = eventMySQLJSON[MySQLColumns.AppUserID].int!
//        AppUserName = eventMySQLJSON[MySQLColumns.AppUserName].string!
//        ClientUpdatedTS = Date(fromSQLiteString: MagicNumbers.LOWEST_TS)
//        CreatedTS = Date(fromMySQLString: eventMySQLJSON[MySQLColumns.CreatedTS].string!)
//        Description = eventMySQLJSON[MySQLColumns.Description].string!
//        EndDatetime =  Date(fromMySQLString: eventMySQLJSON[MySQLColumns.EndDatetime].string!)
//        EventID = eventMySQLJSON[MySQLColumns.EventID].int!
//        HobbyName = eventMySQLJSON[MySQLColumns.HobbyName].string!
//        HobbyID = eventMySQLJSON[MySQLColumns.HobbyID].int!
//        Latitude = eventMySQLJSON[MySQLColumns.Latitude].double!
//        Longitude = eventMySQLJSON[MySQLColumns.Longitude].double!
//        QuantityOfPeople = eventMySQLJSON[MySQLColumns.QuantityOfPeople].int!
//        Name = eventMySQLJSON[MySQLColumns.Name].string!
//        PlaceName = eventMySQLJSON[MySQLColumns.PlaceName].string!
//        Reach = ReachOptions(rawValue: eventMySQLJSON[MySQLColumns.Reach].int!)!
//        StartDatetime = Date(fromMySQLString: eventMySQLJSON[MySQLColumns.StartDatetime].string!)
//        ReceivedTS =
//        
//        State = EventState(rawValue : eventMySQLJSON[MySQLColumns.State].int!)!
//        UpdatedTS = Date(fromMySQLString: eventMySQLJSON[MySQLColumns.UpdatedTS].string!)
//        
//    }
    
//    init(eventID : Int, name : String, appUserID : Int, appUserName : String, hobbyName : String, hobbyID : Int, address : String, latitude : String, longitude : String, startDatetime : String, endDatetime : String, reach : String, description : String, state : Int, CreatedTS : String, placeName : String, quantityOfPeople : Int, updated : Int){
//
//        EventID = eventID
//        Name = name
//        AppUserID = appUserID
//        AppUserName = appUserName
//        HobbyName = hobbyName
//        HobbyID = hobbyID
//        Address = address
//        Latitude = Double(latitude)
//        Longitude = Double(longitude)
//        StartDatetime = startDatetime
//        EndDatetime = endDatetime
//        Reach = reach
//        Description = description
//        State = state
//        CreatedTS = createdTS
//        PlaceName = placeName
//        QuantityOfPeople = quantityOfPeople
//        Updated = updated
//
//    }
    
   
    init(
        
        Address : String,  EndDatetime : Date,  Description : String, HobbyID : Int,  Latitude : Double, Longitude : Double, PlaceName : String, Name : String, Reach : ReachOptions, StartDatetime : Date){
        
        
        self.Address = Address
        AppUserID = -1
        AppUserName = "Creating Dont Need A Name"
        ClientUpdatedTS = MagicNumbers.LOWEST_TS
        CreatedTS = Date()
        self.Description = Description
        self.EndDatetime = EndDatetime
        EventID = -1
        self.HobbyID = HobbyID
        HobbyName = "Creating Dont Need A Name"
        self.Latitude = Latitude
        self.Longitude = Longitude
        self.PlaceName = PlaceName
        QuantityOfPeople = 0
        self.Name = Name
        self.Reach = Reach
        ReceivedTS = MagicNumbers.LOWEST_TS
        self.StartDatetime = StartDatetime
        State = EventState.Created
        UpdatedTS = Date()
        
    }
    
    func makeDictionary (
        address : String, endDatetime : Date, description : String, hobbyID : Int, latitude : Double, longitude : Double, placeName : String, name : String, reach : ReachOptions, startDatetime : Date ) -> [String: Any]{
        
        var dictionary = [String : Any]()
        dictionary[MySQLColumns.Address] = address
        dictionary[MySQLColumns.EndDatetime] = endDatetime.MySQLiteFormat
        dictionary[MySQLColumns.Description] = description
        dictionary[MySQLColumns.HobbyID] = hobbyID
        dictionary[MySQLColumns.Latitude] = latitude
        dictionary[MySQLColumns.Longitude] = longitude
        dictionary[MySQLColumns.StartDatetime] = startDatetime.MySQLiteFormat
        dictionary[MySQLColumns.Name] = name
        dictionary[MySQLColumns.PlaceName] = placeName
        return dictionary
        
        
    }
    
    init(row : Row){
        Address = row[TableE.Address]
        AppUserName = row[TableE.AppUserName]
        AppUserID = row[TableE.AppUserID]
        ClientUpdatedTS = row[TableE.ClientUpdatedTS]
        CreatedTS = Date(fromSQLiteString: row[TableE.CreatedTS])
        Description = row[TableE.Description]
        EndDatetime = Date(fromSQLiteString: row[TableE.EndDatetime])
        EventID = row[TableE.EventID]
        HobbyID = row[TableE.HobbyID]
        HobbyName = row[TableE.HobbyName]
        Latitude = Double(row[TableE.Latitude])!
        Longitude = Double(row[TableE.Longitude])!
        Name = row[TableE.Name]
        PlaceName = row[TableE.PlaceName]
        QuantityOfPeople = row[TableE.QuantityOfPeople]
        Reach = ReachOptions(rawValue: row[TableE.Reach])!
        ReceivedTS = row[TableE.ReceivedTS]
        StartDatetime = Date(fromSQLiteString: row[TableE.StartDatetime])
        State = EventState(rawValue: row[TableE.State])!
        UpdatedTS = Date(fromSQLiteString: row[TableE.UpdatedTS])
    }
    
    func getFieldDictionary() -> [String : Any]{
       
        var dictionary = [String : Any]()

        dictionary[MySQLColumns.EventID] = EventID
        dictionary[MySQLColumns.Name] = Name
        dictionary[MySQLColumns.AppUserID] = AppUserID
        dictionary[MySQLColumns.HobbyID] = HobbyID
        dictionary[MySQLColumns.Address] = Address
        dictionary[MySQLColumns.Latitude] = Latitude
        dictionary[MySQLColumns.Longitude] = Longitude
        dictionary[MySQLColumns.StartDatetime] = StartDatetime.MySQLiteFormat
        dictionary[MySQLColumns.EndDatetime] = EndDatetime.MySQLiteFormat
        dictionary[MySQLColumns.Reach] = Reach
        dictionary[MySQLColumns.Description] = Description
        dictionary[MySQLColumns.State] = State
        dictionary[MySQLColumns.CreatedTS] = CreatedTS.MySQLiteFormat
        dictionary[MySQLColumns.PlaceName] = PlaceName
        dictionary[MySQLColumns.QuantityOfPeople] = QuantityOfPeople
        dictionary[MySQLColumns.AppUserName] = AppUserName
        dictionary[MySQLColumns.HobbyName] = HobbyName
        
        return dictionary
    }
}


