import Foundation
import SQLite
import UIKit

class Hobby {
    
    static let child = "child"
    static let parent = "parent"
    
    var HobbyID : Int
    var InHobby : Bool = false

    var Name : String
    var ParentID : Int

    var aType : String
    var Version : Int = -1
    
    init(hobbyID : Int, name : String, type : String, version : Int, parentID : Int, inHobby : Bool){
        HobbyID = hobbyID
        Name = name
        aType = type
        Version = version
        ParentID = parentID
        InHobby = inHobby
    }
    
    init(hobbyID : Int, name : String, parentID : Int, type : String, version : Int){
        HobbyID = hobbyID
        Name = name
        ParentID = parentID
        aType = type
        Version = version
    }
    
    
    init(hobby : Statement.Element, inHobby : Bool){
        HobbyID = Int(exactly: hobby[0] as! Int64) ?? -1
        Name = hobby[1] as! String
        aType = hobby[2] as! String
        ParentID = -1
        InHobby = inHobby
    }
    
    init(row : Row){
        HobbyID = row[TableE.HobbyID]
        Name = row[TableE.Name]
        ParentID = row[TableE.ParentID]
        aType = row[TableE.aType]
        Version = row[TableE.Version]
    }
    
    
    init(hobbyID : Int64, name : String, type : String, parentID : Int64){
        HobbyID = Int(exactly: hobbyID)!
        Name = name
        aType = type
        ParentID = Int(exactly: parentID)!
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Hobby(hobbyID: HobbyID, name: Name, type: aType, version: Version, parentID: ParentID, inHobby: InHobby)
        return copy
    }
    
    func toString() -> String{
    
        return """
            HobbyID : \(HobbyID),
            InHobby : \(InHobby)
            Name : \(Name),
            ParentID : \(ParentID),
            aType : \(aType),
            Version : \(Version),
        """
    
    }
    
    
}
