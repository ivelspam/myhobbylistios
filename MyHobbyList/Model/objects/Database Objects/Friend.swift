import Foundation
import SwiftyJSON
import SQLite

enum FriendState : Int {
    case SentRequest = 0
    case ReceivedRequest = 1
    case Confirmed = 2
    case Banned = 3
    case Removed = 4

}

class Friend{

    var AppUserID : Int
    var ClientUpdatedTS : String
    var EventID : Int?
    var ImageVersion : Int
    var Name : String
    var ReceivedTS : String
    var State : FriendState
    var UpdatedTS : String

    init(friendJSONFromMySQL : JSON){
        AppUserID = friendJSONFromMySQL[MySQLColumns.AppUserID].int!
        ClientUpdatedTS = friendJSONFromMySQL[SQLiteColumns.ClientUpdatedTS].string!
        EventID = friendJSONFromMySQL[MySQLColumns.EventID].int
        ImageVersion = friendJSONFromMySQL[MySQLColumns.ImageVersion].int ?? MagicNumbers.NO_PICTURE
        Name = friendJSONFromMySQL[MySQLColumns.Name].string!
        ReceivedTS = friendJSONFromMySQL[MySQLColumns.UpdatedTS].string!
        State =  FriendState(rawValue: friendJSONFromMySQL[MySQLColumns.State].int!)!
        UpdatedTS = friendJSONFromMySQL[MySQLColumns.UpdatedTS].string!
    }
    
    init(row : Row){
        AppUserID = row[TableE.AppUserID]
        ClientUpdatedTS = row[TableE.ClientUpdatedTS]
        EventID = row[TableE.EventID]
        ImageVersion = row[TableE.ImageVersion]
        Name = row[TableE.Name]
        ReceivedTS = row[TableE.ReceivedTS]
        State = FriendState(rawValue: row[TableE.State])! 
        UpdatedTS = row[TableE.UpdatedTS]
    }
  
}
