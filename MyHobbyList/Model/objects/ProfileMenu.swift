import Foundation
import UIKit

class ProfileMenu{

    enum ProfileMenuCat : String{
        case Hobbies = "Hobbies"
        case Name = "Name"
        case Username = "Username"
        case Distance = "Distance"
        case LogOut = "Log Out"
        
    }
    
    var rows : [ProfileMenuItem] = [
        ProfileMenuItem(image : "hobbyProfileTable", profileMenuCat : ProfileMenuCat.Hobbies, segueName : SegueName.OpenMyHobbies),
        ProfileMenuItem(image : "nameProfileTable", profileMenuCat : ProfileMenuCat.Name, segueName : SegueName.ShowEditName),
        ProfileMenuItem(image : "usernameProfileTable", profileMenuCat : ProfileMenuCat.Username, segueName : SegueName.ShowEditUsername),
        ProfileMenuItem(image : "distanceProfileTable", profileMenuCat : ProfileMenuCat.Distance, segueName : SegueName.ShowChangeDistance),
        ProfileMenuItem(image : "logOutProfileTable", profileMenuCat : ProfileMenuCat.LogOut, segueName : SegueName.LogOutSegue)
        ]
    
    var count : Int = 5
    
    init(){
        count = rows.count
    }
    
    func getRowPosition(profileMenuCat : ProfileMenuCat) -> Int{
        
        var index = 0
        for row in rows{
            if profileMenuCat == row.profileMenuCat{
                return index
            }
            index = index + 1
        }
        
        return 0
        
    }
    
    
    class ProfileMenuItem{
        
        
        var image : String = ""
        var profileMenuCat : ProfileMenuCat
        var segueName : SegueName
        
        init(image : String, profileMenuCat: ProfileMenuCat, segueName : SegueName){
            self.image = image
            self.profileMenuCat = profileMenuCat
            self.segueName = segueName
        }
        
        func getValue() -> String{
            
            switch profileMenuCat {
            case .Name:
                if let name = UserDefaults.standard.string(forKey: DefaultKey.Name_str.rawValue){
                    return name
                }else{
                    return "missing name"
                }
            case .Username:
                if let username = UserDefaults.standard.string(forKey: DefaultKey.Username_str.rawValue){
                        return username
                }else{
                    return "missing username"
                }
                
            case .Distance:
                if let distance = UserDefaults.standard.string(forKey: DefaultKey.Distance_int.rawValue){
                    if let measurement = UserDefaults.standard.string(forKey: DefaultKey.MeasureSystem_str.rawValue){
                        if(measurement=="mi"){
                            return "\(distance) Miles"
                        }else{
                            return "\(distance) Kilometers"
                        }
                    }else{
                        return "\(distance)MIA"
                    }
                }else{
                    return "missing distance"
                }
            default:
                return ""
            }
        }

        func runSegue(profileViewController : UIViewController){
            switch profileMenuCat {
            case .LogOut:
                Motherbase().clearTables()
                UserDefaultsUtils.clearTheUserDefaults()
                profileViewController.performSegue(withIdentifier: segueName.rawValue, sender: profileViewController)
            case .Hobbies :
                profileViewController.performSegue(withIdentifier: segueName.rawValue, sender: nil)
            case .Distance :
                profileViewController.performSegue(withIdentifier: segueName.rawValue, sender: nil)
            case .Name :
                profileViewController.performSegue(withIdentifier: segueName.rawValue, sender: nil)
            case .Username :
                profileViewController.performSegue(withIdentifier: segueName.rawValue, sender: nil)

            }
        }
    }

}




