class TableN{
        static let appuserhobby = "appuserhobby"
        static let friend = "friend"
        static let event = "event"
        static let chat = "chat"
        static let hobby = "hobby"
        static let hobbyclosure = "hobbyclosure"

}

class Columns{
    static let HobbyID = "HobbyID"
    static let aType = "Type"
    static let ParentID = "ParentID"
    static let Ancestor = "Ancestor"
    static let Descendant = "Descendant"
    static let Name = "Name"
    static let DateCreated = "DateCreated"
    static let State = "State"

}

class TableO{
    static let JULIANDAY = "JULIANDAY"
}

class TableValue{
    static let child = "child"
    static let km = "km"
    static let mi = "mi"

}

import SQLite


class TableE{
    
    static let Address = Expression<String>("Address")
    static let AppUserID = Expression<Int>("AppUserID")
    static let Ancestor = Expression<Int>("Ancestor")

    static let AppUserName = Expression<String>("AppUserName")
    static let Autoincrement = Expression<Int>("Autoincrement")
    static let ChatID = Expression<Int>("ChatID")
    static let ClientUpdatedTS = Expression<String>("ClientUpdatedTS")
    static let CreatedTS = Expression<String>("CreatedTS")
    static let Description = Expression<String>("Description")
    static let DateCreated = Expression<String>("DateCreated")
    static let Datetime = Expression<String>("Datetime")
    static let Descendant = Expression<Int>("Descendant")

    static let EndDatetime = Expression<String>("EndDatetime")
    static let EventID = Expression<Int>("EventID")
    static let FriendID = Expression<String>("FriendID")
    static let HobbyID = Expression<Int>("HobbyID")
    static let HobbyName = Expression<String>("HobbyName")
    static let ImageVersion = Expression<Int>("ImageVersion")
    static let Latitude = Expression<String>("Latitude")
    static let Longitude = Expression<String>("Longitude")
    static let Name = Expression<String>("Name")
    static let Message = Expression<String>("Message")
    static let Owner = Expression<Int>("Owner")
    static let ParentID = Expression<Int>("ParentID")
    static let PlaceName = Expression<String>("PlaceName")
    static let QuantityOfPeople = Expression<Int>("QuantityOfPeople")
    static let Reach = Expression<Int>("Reach")
    static let ReadMessageDatetime = Expression<String>("ReadMessageDatetime")
    static let ReceivedTS = Expression<String>("ReceivedTS")
    static let Receiver = Expression<Int>("Receiver")
    static let SentDatetime = Expression<String>("SentDatetime")
    static let StartDatetime = Expression<String>("StartDatetime")
    static let State = Expression<Int>("State")
    static let Timestamp = Expression<String>("Timestamp")
    static let aType = Expression<String>("Type")
    static let UpdatedTS = Expression<String>("UpdatedTS")
    static let aUUID = Expression<String>("UUID")
    static let Version = Expression<Int>("Version")

}

