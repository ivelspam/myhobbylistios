import UIKit
import SQLite
import CoreLocation

class Utils{
    
    static func checkIfImageExists(fileName : String) -> UIImage?{
        print("checkIfImageExists")
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        
        print(fileURL)
        print(fileURL.path)
        

        if(FileManager.default.fileExists(atPath: fileURL.path)){
            print("file exists")
            do{
                let imageData = try Data(contentsOf: fileURL)
                return UIImage(data: imageData)
            }catch{
                print(error)
            }
        }

        return nil

    }
    
    static func checkIfImageExistsOrDownload(imageType : Download_image.ImageType,  appUserID : Int, imageVersion : Int, completion : @escaping (UIImage?)->()){
        
        if imageVersion == MagicNumbers.NO_PICTURE {
            completion(nil)
        }else{
            
            let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileURL = documentsUrl.appendingPathComponent("\(appUserID)-\(imageVersion).jpg")
            
            
            if(FileManager.default.fileExists(atPath: fileURL.path)){
                do{
                    let imageData = try Data(contentsOf: fileURL)
                    completion(UIImage(data: imageData))
                }catch{
                    print(error)
                }
            }else{
                
                ServerRequest.downloadFriendPicture(request: Download_image(imageType: imageType, appUserID: appUserID, imageVersion: imageVersion)){
                    imagePath, worked in
                    
                    do{
                        let imageData = try Data(contentsOf: imagePath)
                        completion(UIImage(data: imageData))
                    }catch{
                        print(error)
                    }
                }
            }
            
        }
    }
    
    static func convertDictToJSON(dictionary :[String : Any])-> String{
        let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: [])
        return String(data : jsonData, encoding: .utf8)!
    }
    
    static func tableToString(database : Connection, tableName : String) -> String{
        var values = "\n\(tableName) Table"
        
        do {
            let stmt = try database.prepare("SELECT * FROM \(tableName)")
            for row in stmt {
                for (index, columnName) in stmt.columnNames.enumerated(){
                    values.append("\(columnName)=\(row[index]!), ")
                }
                values.append("\n")
            }
        } catch {
            print(error)
        }
        return values
    }
    
    static func getDistanceFromLocation(eventCCLocation : CLLocation, gpsCCLocation : CLLocation) -> Double{
      
        let type = UserDefaults.standard.string(forKey: DefaultKey.MeasureSystem_str.rawValue)
        
        let distanceInMeters = eventCCLocation.distance(from: gpsCCLocation)
        
        var division = 1000.0
        
        if type == "mi" {
            division = 1609.0
        }
        
        return round(10 * (distanceInMeters/division)) / 10
    }
    

}
