import Foundation

class Values {
    
    static let  USERNAME_SIZE = 3,
                NAME_SIZE = 3
}

class MagicNumbers {
    
    static let  NOT_IN_EVENT = -1,
                NO_PICTURE = -1,
                LOWEST_TS = "1970-01-01 00:00:01.000",
                LOWEST_MYSQL_TS = "1970-01-01T00:00:01.000Z",
                NULL_PARENT_ID = -1,
                NO_ID_FOUND = -1
}


class RepTimers {
    
    static let  UPDATE_EVENTS = 10.0,
                UPDATE_FRIENDS = 10.0,
                SERVER_REQUEST_UPDATE = 10.0

}
