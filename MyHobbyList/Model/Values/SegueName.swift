import Foundation

enum SegueName : String{
    case OpenMyHobbies
    case EmailRegistrationSegue
    case TokenNotFound
    case TokenChecksOut
    case LogOutSegue
    case LoggedInSegue
    case EditNameSegue
    case OpenFriendChat
    case ShowFriendSentRequest
    case ShowFriendReceivedRequest
    case OpenFindFriends
    case OpenCreateEvent
    case ShowEventInfo
    case ShowChangeDistance
    case ShowEditName
    case ShowEditUsername
    case ShowRegisterHobbySegue
}
