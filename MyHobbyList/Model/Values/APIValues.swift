import Foundation


class APIValues {
    
    class Links {
        
        
        static let  event_register = "event_register",
                    event_leave = "event_leave",
                    event_close = "event_close",
                    event_join = "event_join",
                    event_hide = "event_hide",
                    event_getEventByDistance = "event_getEventByDistance",
                    event_getEventUpdate = "event_getEventUpdate",
                    eventcomment_getComments = "eventcomment_getComments",
                    eventcomment_send = "eventcomment_send",
                    appuser_updateUsername = "appuser_updateUsername",
                    appuser_updateName = "appuser_updateName",
                    appuser_findByAppUserID = "appuser_findByAppUserID",
                    appuser_UpdateDistanceAndMeasurementSystem = "appuser_UpdateDistanceAndMeasurementSystem",
                    appuser_findAUserByUsername = "appuser_findAUserByUsername",
        
                    allInfo_getAllInfo = "allInfo_getAllInfo",
                    allInfo_getUpdates = "allInfo_getUpdates",
                    allInfo_updates = "allInfo_getUpdates",
                    login_email = "login_email",
                    login_facebook = "login_facebook",
        
        
                    eventcomment_getEventComments = "eventcomment_getEventComments",
                    eventcomment_sendACommentMessage = "eventcomment_sendACommentMessage",
        
                    token_getNewToken = "token_getNewToken",
                    email_sendAnotherToken = "email_sendAnotherToken",
        
                    fcmToken_removeTokenFromSystem = "fcmToken_removeTokenFromSystem",
        
                    friend_cancel = "friend_cancel",
                    friend_accept = "friend_accept",
                    friend_sendRequest = "friend_sendRequest",
                    
                    registration_addUserByEmail = "registration_addUserByEmail",
        
        
                    download_image = "download_image",
        
                    upload_profilePicture = "upload_profilePicture",
        
                    hobbies_addHobby = "hobbies_addHobby",
                    hobbies_removeHobby = "hobbies_removeHobby",
        
                    chat_getNewFromFriends = "chat_getNewFromFriends"
        
    }
    
}

