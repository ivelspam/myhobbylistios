import Foundation


class MySQLColumns {
    
    static let
    

    AppUserID = "AppUserID",
    AppUserName = "AppUserName",
    Address = "Address",
    ChatID = "ChatID",
    Comment = "Comment",
    CreatedTS = "CreatedTS",
    Description = "Description",
    DeviceID = "DeviceID",
    DeviceType = "DeviceType",
    Distance = "Distance",
    DOB = "DOB",
    EndDatetime = "EndDatetime",
    Email = "Email",
    EventCommentID = "EventCommentID",
    EventID = "EventID",
    FacebookUserID = "FacebookUserID",
    FCMToken = "FCMToken",
    HobbyID = "HobbyID",
    HobbyName = "HobbyName",
    ImageVersion = "ImageVersion",
    Latitude = "Latitude",
    Longitude = "Longitude",
    Name = "Name",
    MeasureSystem = "MeasureSystem",
    Message = "Message",
    Owner = "Owner",
    ParentID = "ParentID",
    Password = "Password",
    PlaceName = "PlaceName",
    QuantityOfPeople = "QuantityOfPeople",
    Reach = "Reach",
    Receiver = "Receiver",
    SentDatetime = "SentDatetime",
    StartDatetime = "StartDatetime",
    State = "State",
    aType = "Type",
    UpdatedTS = "UpdatedTS",
    Username = "Username",
    UUID = "UUID",
    Version = "Version"
}

class SQLiteColumns {
    
    static let  ClientUpdatedTS = "ClientUpdatedTS",
                ReceivedTS = "ReceiveddTS"
    

}
