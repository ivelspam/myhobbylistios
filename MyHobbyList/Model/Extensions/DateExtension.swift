import UIKit

extension Date {
    
    init(fromMySQLString : String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: fromMySQLString)!
        self.init(timeIntervalSince1970: date.timeIntervalSince1970)
    }
    
    init(fromSQLiteString : String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: fromSQLiteString)!
        self.init(timeIntervalSince1970: date.timeIntervalSince1970)
    }
    
    static func MySQLToSQLite(dateString : String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let date = dateFormatterGet.date(from: dateString)
        
        return dateFormatterPrint.string(from: date!)
    }
    
    init(nearestMinuteRoundUp : Int){

        let initialDate = Date()
        let cal = Calendar.current
        let startOfMinute = cal.dateInterval(of: .hour, for: initialDate)!.start
       
        var minutes = initialDate.timeIntervalSince(startOfMinute)
        minutes = ceil(minutes / (600)) * 600
        let finalDate = startOfMinute.addingTimeInterval(minutes)
        
        self.init(timeIntervalSince1970: finalDate.timeIntervalSince1970)
        
    }
    
    func convertDateTo(format : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let now = dateFormatter.string(from: self)
        return now
    }
    
    var MySQLiteFormat : String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.string(from: self)
        return date
    }
    
    func nearestTenMinutes() -> Date {
        let cal = Calendar.current
        let startOfMinute = cal.dateInterval(of: .hour, for: self)!.start
        var minutes = self.timeIntervalSince(startOfMinute)
        minutes = ceil(minutes / (600)) * 600
        return startOfMinute.addingTimeInterval(minutes)
    }
    
    func getTimeWithoutSeconds() -> Date{
        var date = self;
        let timeInterval = floor(date.timeIntervalSinceReferenceDate / 60.0) * 60.0
        date = Date(timeIntervalSinceReferenceDate: timeInterval)
        
        return date

    }
    
    
    static func getMinMySQLTimestamp() -> Date{
        return Date(fromSQLiteString: "1970-01-01 00:00:01.000")
    }
    
        /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        if nanoseconds(from: date) > 0 { return "\(nanoseconds(from: date))ns" }
        return ""
    }
}
