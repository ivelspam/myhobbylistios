import Foundation


extension String {
    
    
    func removeRightSpaces() -> String{
        let cleansed = self.replacingOccurrences(of: "\\s+$",
                                                   with: "",
                                                   options: .regularExpression)
        
        return cleansed
        
    }
    
}
