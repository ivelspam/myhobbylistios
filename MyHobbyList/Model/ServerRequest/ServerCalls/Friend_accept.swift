import Foundation

class Friend_accept: APIRequest {
    
    class Param{
        static let  AppUserID = MySQLColumns.AppUserID
    }
    
    class Response{
        static let  accepted = "accepted"
    }
    
    class ResponseFields{
        
    }
    
    init(appUserID : Int){
        super.init()
        keyValues[Param.AppUserID] = appUserID
        APIUrl = APIValues.Links.friend_accept
    }
}
