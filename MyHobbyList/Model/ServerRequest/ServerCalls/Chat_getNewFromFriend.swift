import Foundation
import SwiftyJSON

class Chat_getNewFromFriend: APIRequest{

    class Param{
        static let  AppUserID = MySQLColumns.AppUserID,
                    lastestChatID = "lastestIDMax"
    }
    
    class Response{
        static let  foundNewMessages = "foundNewMessages",
                    notFoundNewMessages = "notFoundNewMessages"
    }
    
    class ResponseFields{
        static let  foundNewMessages = "foundNewMessages"

    }
    
    init(appUserID : Int){
        super.init()
        
        keyValues[Param.AppUserID] = appUserID
        keyValues[Param.lastestChatID] = Motherbase().getChatTable().selectLatestID(friendAppUserID: appUserID)

        APIUrl = APIValues.Links.chat_getNewFromFriends
        token = false
    }
}
