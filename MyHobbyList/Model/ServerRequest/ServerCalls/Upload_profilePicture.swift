import Foundation


class Upload_profileImage: APIRequest {
    
    class Param {
        
    }
    
    class Response {
        static let  err = "err",
                    done = "done"
    }
    
    class ResponseFields {
        
    }
    
    override init(){
        super.init()
        APIUrl = APIValues.Links.upload_profilePicture
    }
}
