import Foundation

class Appuser_updateName: APIRequest {
    
    class Param{
        static let  Name = MySQLColumns.Name
    }
    
    class Response{
        
    }
    
    class ResponseFields{
        
    }
    
    init(name : String){
        super.init()
        keyValues[Param.Name] = name
        APIUrl = APIValues.Links.appuser_updateName
    }
}
