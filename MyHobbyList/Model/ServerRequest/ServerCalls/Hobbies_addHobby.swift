import Foundation



class Hobbies_addHobby: APIRequest {
    
    class Param{
        static let  HobbyID = MySQLColumns.HobbyID
    }
    
    class Response{
        static let  AlreadyExists  = "alreadyExists",
                    HobbyDoNotExists = "hobbyDoNotExists",
                    HobbyAdded = "hobbyAdded";
    }
    
    class ResponseFields{

    }
    
    init(hobbyID : Int){
        super.init()
        keyValues[Param.HobbyID] = hobbyID
        APIUrl = APIValues.Links.hobbies_addHobby
    }
}
