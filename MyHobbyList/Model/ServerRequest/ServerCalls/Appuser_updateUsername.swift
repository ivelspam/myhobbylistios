import Foundation

class Appuser_updateUsername: APIRequest {
    
    
    class Param{
        static let  Username = MySQLColumns.Username
    }
    
    class Response{
        
    }
    
    class ResponseFields{
        
    }
    
    init(username : String){
        super.init()
        keyValues[Param.Username] = username
        APIUrl = APIValues.Links.appuser_updateUsername
    }
}


