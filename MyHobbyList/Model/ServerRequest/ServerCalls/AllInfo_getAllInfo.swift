
import Foundation
import UIKit

class AllInfo_getAllInfo: APIRequest {
    
    class Param{
        static let DeviceID = "DeviceID"
        static let DeviceType = "DeviceType"
        static let FCMToken = "FCMToken"
        static let Latitude = "Latitude"
        static let Longitude = "Longitude"
        
    }
    
    class Response{
        static let userNotFound = "userNotFound"

        static let userFound = "userFound"
    }
    
    class ResponseFields{
        static let
        
        appUserHobby = "appUserHobby",
        chats = "chats",
        events = "events",
        friends = "friends",
        hobbiesTable = "hobbiesTable",
        message = "message",
        userInEvent = "userInEvent"

    }
    
    override init(){
        super.init()
        keyValues[Param.DeviceID] = UIDevice.current.identifierForVendor!.uuidString
        keyValues[Param.DeviceType] = "IOS"
        keyValues[Param.FCMToken] = ""
        keyValues[Param.Latitude] = String(Radar.shared.locationManager.location!.coordinate.latitude)
        keyValues[Param.Longitude] = String(Radar.shared.locationManager.location!.coordinate.longitude)
        
        
        APIUrl = APIValues.Links.allInfo_getAllInfo
    }
    
    
    
    
}
