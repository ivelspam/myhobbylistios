import Foundation

class Friend_sendRequest: APIRequest{
    
    
    class Param{
        static let  AppUserID = MySQLColumns.AppUserID
    }
    
    class Response{
        
    }
    
    class ResponseFields{
        
    }
    
    init(appUserID : Int){
        super.init()
        keyValues[Param.AppUserID] = appUserID
        APIUrl = APIValues.Links.friend_sendRequest
        token = false
    }
}
