
import Foundation
import UIKit

class AllInfo_getUpdates: APIRequest {
    
    class Param{
        static let
        eventsIDs = "eventsIDs",
        eventUpdatedTS = "eventUpdatedTS",
        friendUpdatedTS = "friendUpdatedTS",
        inEventUpdatedTS = "inEventUpdatedTS",
        latestChatID = "latestChatID",
        Latitude = MySQLColumns.Latitude,
        Longitude = MySQLColumns.Longitude,
        UpdatedTS = MySQLColumns.UpdatedTS
    }
    
    class Response{
        static let noUpdates = "noUpdates"
        static let foundUpdates = "foundUpdates"
    }
    
    class ResponseFields{
        static let  AppUserChange = "AppUserChange",
                    eventsUpdate = "eventsUpdate",
                    friends = "friends",
                    inEvent = "inEvent",
                    newChats = "newChats",
                    newEvents = "newEvents",
                    message = "message"
    }
    
    override init(){
        super.init()
        keyValues[Param.eventsIDs] = eventIDList()
        keyValues[Param.eventUpdatedTS] = UserDefaults.standard.string(forKey: DefaultKey.eventUpdatedTS_str.rawValue)
        keyValues[Param.friendUpdatedTS] = UserDefaults.standard.string(forKey: DefaultKey.friendUpdatedTS_str.rawValue)
        keyValues[Param.inEventUpdatedTS] = UserDefaults.standard.string(forKey: DefaultKey.inEventUpdatedTS_str.rawValue)
        keyValues[Param.latestChatID] = UserDefaults.standard.string(forKey: DefaultKey.latestChatID_str.rawValue)
        keyValues[Param.Latitude] = String(Radar.shared.locationManager.location!.coordinate.latitude)
        keyValues[Param.Longitude] = String(Radar.shared.locationManager.location!.coordinate.longitude)
        keyValues[Param.UpdatedTS] = UserDefaults.standard.string(forKey: DefaultKey.UpdatedTS_str.rawValue)

        APIUrl = APIValues.Links.allInfo_getUpdates
    }
    
    func eventIDList() -> String{
        print("eventIDList")
        let events = Motherbase().getEventTable().selectNonClosedEvents()
        if(events.count == 0){
            return ""
        }
        var sb : String = "(-1,"
        
        for event in events {
            sb.append("\(event.EventID),")
        }
        
        sb = String(sb.dropLast(1))
        sb.append(")")
        
        print(sb)
        return sb
    }
    
}
