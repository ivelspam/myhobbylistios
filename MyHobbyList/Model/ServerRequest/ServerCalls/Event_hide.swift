import Foundation

class Event_hide: APIRequest {
    
    class Param{
        static let  EventID = MySQLColumns.EventID
    }
    
    class Response{
        static let  hidedEvent = "hideEvent"
    }
    
    class ResponseFields{
        
    }
    
    init(eventID : Int){
        super.init()
        keyValues[Param.EventID] = eventID
        APIUrl = APIValues.Links.event_hide
    }
}
