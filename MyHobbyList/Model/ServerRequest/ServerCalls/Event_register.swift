import Foundation

class Event_register: APIRequest {
    
    class Param{
        
        static let  Name = MySQLColumns.Name,
                    HobbyID = MySQLColumns.HobbyID,
                    Address = MySQLColumns.Address,
                    Latitude = MySQLColumns.Latitude,
                    Longitude = MySQLColumns.Longitude,
                    PlaceName = MySQLColumns.PlaceName,
                    Reach = MySQLColumns.Reach,
                    StartDatetime = MySQLColumns.StartDatetime,
                    EndDatetime = MySQLColumns.EndDatetime,
                    Description = MySQLColumns.Description;
        
    }
    
    class Response{
        static let  eventCreated  = "eventCreated",
                    userAlreadyInEvent = "userAlreadyInEvent";
    }
    
    class ResponseFields{
        static let event = "event"
    }
    
    init(
        address : String, description : String, endDatetime : Date,  hobbyID : Int, latitude : Double, longitude : Double,  name : String, placeName : String, reach : Event.ReachOptions, startDatetime : Date ){
        super.init()
        
        keyValues[Param.Address] = address
        keyValues[Param.Description] = description
        keyValues[Param.EndDatetime] = endDatetime
        keyValues[Param.HobbyID] = hobbyID
        keyValues[Param.Latitude] = latitude
        keyValues[Param.Longitude] = longitude
        keyValues[Param.Name] = name
        keyValues[Param.PlaceName] = placeName
        keyValues[Param.Reach] = reach
        keyValues[Param.StartDatetime] = startDatetime

        APIUrl = APIValues.Links.event_register
        
    }
  
    
}
