import Foundation


class Hobbies_removeHobby: APIRequest {
    
    class Param{
        static let  HobbyID = MySQLColumns.HobbyID
    }
    
    class Response{
        static let  deleted = "deleted"
    }
    
    class ResponseFields{
        static let  AlreadyExists  = "alreadyExists",
                    HobbyDoNotExists = "hobbyDoNotExists",
                    HobbyAdded = "hobbyAdded";
    }
    
    init(hobbyID : Int){
        super.init()
        keyValues[Param.HobbyID] = hobbyID
        APIUrl = APIValues.Links.hobbies_removeHobby
    }
}
