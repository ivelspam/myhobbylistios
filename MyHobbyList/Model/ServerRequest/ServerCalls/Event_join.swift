import Foundation

class Event_join: APIRequest {
    
    class Param {
        static let  EventID = MySQLColumns.EventID
    }
    
    class Response {
        static let  joinedEvent = "joinedEvent"
    }
    
    class ResponseFields {
        
    }
    
    init(eventID : Int) {
        super.init()
        keyValues[Param.EventID] = eventID
        APIUrl = APIValues.Links.event_join
    }
}
