import Foundation

class Friend_cancel: APIRequest {
    
    class Param{
        static let  AppUserID = MySQLColumns.AppUserID
    }
    
    class Response{
        static let  canceled = "canceled"
    }
    
    class ResponseFields{
    
    }
    
    init(appUserID : Int){
        super.init()
        keyValues[Param.AppUserID] = appUserID
        APIUrl = APIValues.Links.friend_cancel
    }
}
