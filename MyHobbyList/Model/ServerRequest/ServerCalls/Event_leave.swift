import Foundation

class Event_leave: APIRequest {
    
    class Param {
        static let  EventID = MySQLColumns.EventID
    }
    
    class Response {
        static let  leaveEvent = "leaveEvent"
    }
    
    class ResponseFields {
        
    }
    
    init(eventID : Int) {
        super.init()
        keyValues[Param.EventID] = eventID
        APIUrl = APIValues.Links.event_close
    }
}
