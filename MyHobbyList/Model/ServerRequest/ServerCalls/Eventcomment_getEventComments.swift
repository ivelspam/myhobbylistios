import Foundation

class Eventcomment_getEventComments: APIRequest{
    
    
    class Param{
        static let  EventID = MySQLColumns.EventID
    }
    
    class Response{
        static let  foundMessage = "foundMessage",
                    notFoundMessage = "notFoundMessage"
    }
    
    class ResponseFields{
        
    }
    
    init(eventID : Int){
        super.init()
        keyValues[Param.EventID] = eventID
        APIUrl = APIValues.Links.eventcomment_getComments
        token = false
    }
}
