import Foundation

class APIRequest {
    
    var APIUrl : String?
    var keyValues = [String: Any]()
    var token = true
    
    func getAPIUrl() -> String{
        return APIUrl!
        
    }
    
    func getParameters() -> Dictionary<String, Any>{
        return keyValues
    }
    
}
