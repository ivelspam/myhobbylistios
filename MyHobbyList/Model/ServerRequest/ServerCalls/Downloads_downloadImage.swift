import Foundation

class Download_image: APIRequest {
    
    class Param {
        static let  AppUserID = MySQLColumns.AppUserID,
                    ImageVersion = MySQLColumns.ImageVersion,
                    type = "type"
        
    }
    
    class Response {
        static let  joinedEvent = "joinedEvent"
    }
    
    class ResponseFields {
        
    }
    
    enum ImageType: String {
        case FRIEND = "FRIEND"
        case PROFILE = "PROFILE"
        case COMMENT = "COMMENT"
    }
    
    init(imageType : ImageType, appUserID : Int, imageVersion : Int){
        super.init()
        keyValues[Param.AppUserID] = appUserID
        keyValues[Param.ImageVersion] = imageVersion
        keyValues[Param.type] = imageType.rawValue
        APIUrl = APIValues.Links.download_image
    }
}

