import Foundation

class Event_close: APIRequest {
    
    class Param{
        static let  EventID = MySQLColumns.EventID
    }
    
    class Response{
        static let  closedEvent = "closedEvent"
    }
    
    class ResponseFields{
        
    }
    
    init(eventID : Int){
        super.init()
        keyValues[Param.EventID] = eventID
        APIUrl = APIValues.Links.event_close
    }
}

