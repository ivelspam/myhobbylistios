import Foundation

class Login_email: APIRequest {
    
    class Param{
        static let  Email = MySQLColumns.Email,
                    Password = MySQLColumns.Password
    }
    
    class Response{
        
    }
    
    class ResponseFields{
        
    }
    
    init(email : String, password: String){
        super.init()
        keyValues[Param.Email] = email
        keyValues[Param.Password] = password
        APIUrl = APIValues.Links.login_email
        token = false
    }
}
