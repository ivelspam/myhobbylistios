import Foundation

class Appuser_UpdateDistanceAndMeasurementSystem: APIRequest {
    
    class Param{
        
        static let  MeasureSystem = MySQLColumns.MeasureSystem,
                    Distance = MySQLColumns.Distance

        
    }
    
    class Response{
        static let  updated = "updated",
                    notUpdated = "notUpdated"
    }
    
    class ResponseFields{
        static let event = "event"
    }
    
    enum MeasureSystemType : String{
        
        case km
        case mi
        
    }
    
    init(measureSystemType : MeasureSystemType, distance : Int){
        super.init()
        
        keyValues[Param.MeasureSystem] = measureSystemType.rawValue
        keyValues[Param.Distance] = distance

        APIUrl = APIValues.Links.appuser_UpdateDistanceAndMeasurementSystem
    }
}
