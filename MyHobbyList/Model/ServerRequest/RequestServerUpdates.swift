import Foundation
import CoreLocation
import SwiftyJSON

class RequestServerUpdates: gotUpdatedLocationDelegate {

    var didFoundLocation = false
    
    func requestAllNewInfo(){
        print("requestAllNewInfo")
        Radar.shared.getNewUpdate(delegate: self)
    }
    
    func gotLocation(location: CLLocation?) {
        print("requestAllNewInfo")

        ServerRequest.postRequest(request : AllInfo_getUpdates()) { (updates, worked) in
            RequestServerUpdates.updateTables(updates: updates)
        }
    }
    
    static func updateTables(updates : JSON){
        print("static func updateTables(updates : JSON)")
        print(updates)
        
        
        if updates[AllInfo_getUpdates.ResponseFields.message].stringValue == AllInfo_getUpdates.Response.foundUpdates {
            
            //Friends Updates
            if updates[AllInfo_getUpdates.ResponseFields.friends].exists() {
                print("Friends Updates")
                Motherbase().getFriendTable().insertOrReplace(friendsFromMySQL: updates[AllInfo_getUpdates.ResponseFields.friends])
                UserDefaultsUtils.updateUpdatedTS(jsonValue: updates[AllInfo_getUpdates.ResponseFields.friends], key: .friendUpdatedTS_str)
            }
            
            //Chats New
            if updates[AllInfo_getUpdates.ResponseFields.newChats].exists() {
                Motherbase().getChatTable().insertOrReplace(chatsFromMySQL: updates[AllInfo_getUpdates.ResponseFields.newChats])
                UserDefaultsUtils.updateUpdatedTS(jsonValue: updates[AllInfo_getUpdates.ResponseFields.newChats], key: .latestChatID_str)
            }
            
            //AppUserChange
            if updates[AllInfo_getUpdates.ResponseFields.AppUserChange].exists() {
                UserDefaultsUtils.updateUserDefault(AppUserChange: updates[AllInfo_getUpdates.ResponseFields.AppUserChange])
                UserDefaultsUtils.updateUpdatedTS(jsonValue: updates[AllInfo_getUpdates.ResponseFields.AppUserChange], key: .UpdatedTS_str)
            }
            
            //Events New
            print("Events New")
            if updates[AllInfo_getUpdates.ResponseFields.newEvents].exists() {
                Motherbase().getEventTable().insertOrIgnoreNew(eventsFromMySQL: updates[AllInfo_getUpdates.ResponseFields.newEvents])
            }
            
            //Events Updates
            print("Events Updates")
            if updates[AllInfo_getUpdates.ResponseFields.eventsUpdate].exists() {
                Motherbase().getEventTable().update(eventsChanges: updates[AllInfo_getUpdates.ResponseFields.eventsUpdate])
                UserDefaultsUtils.updateUpdatedTS(jsonValue: updates[AllInfo_getUpdates.ResponseFields.eventsUpdate], key: .eventUpdatedTS_str)
            }
            
            //In Event Update
            print("In Event Update")
            if updates[AllInfo_getUpdates.ResponseFields.inEvent].exists() {
                Motherbase().getEventTable().update(event: updates[AllInfo_getUpdates.ResponseFields.inEvent])
                UserDefaultsUtils.updateUpdatedTS(jsonValue: updates[AllInfo_getUpdates.ResponseFields.inEvent], key: .inEventUpdatedTS_str)
            }
        }
        
//        print(UserDefaultsUtils.toString())
        
    }
    
    static func fillTheDatabase(allInfoJSON : JSON){
        
        print("static func fillTheDatabase(allInfoJSON : JSON)")
        
        print(allInfoJSON)
        
        let database = Motherbase().database!
        var fieldName = ""

        Motherbase().recreateTables()
        
        
        //HOBBIES TABLE
        fieldName = AllInfo_getAllInfo.ResponseFields.hobbiesTable
        print(fieldName)
        if(allInfoJSON[fieldName].exists()){
            HobbyTable(connection: database).addHobbiesToTable(allHobbies: allInfoJSON[fieldName])
            HobbyClosureTable(connection: database).insertAllHobbyClosures(allHobbyClosures: allInfoJSON[fieldName])
        }
        print("//USER HOBBIES")
        //USER HOBBIES
        fieldName = AllInfo_getAllInfo.ResponseFields.appUserHobby
        print(fieldName)

        if(allInfoJSON[fieldName].exists()){
            AppuserHobbyTable(connection: database).insertAllAppUserHobbies(allAppUserHobbies: allInfoJSON[fieldName])
        }

        //USER FRIENDS
        print("//USER FRIENDS")

        fieldName = AllInfo_getAllInfo.ResponseFields.friends
        print(fieldName)

        if(allInfoJSON[fieldName].exists()){
            FriendTable(connection: database).insertOrReplace(friendsFromMySQL: allInfoJSON[fieldName])
        }
        UserDefaultsUtils.updateUpdatedTS(jsonValue: allInfoJSON[fieldName], key: .friendUpdatedTS_str)

        //USER CHATS
        print("//USER CHATS")
        fieldName = AllInfo_getAllInfo.ResponseFields.chats
        print(fieldName)

        if(allInfoJSON[fieldName].exists()){
            ChatTable(connection: database).insertOrReplace(chatsFromMySQL: allInfoJSON[fieldName])
        }
        UserDefaultsUtils.updateUpdatedTS(jsonValue: allInfoJSON[fieldName], key: .latestChatID_str)

        
        //USER EVENTS
        print("//USER EVENTS")

        fieldName = AllInfo_getAllInfo.ResponseFields.events
        
        print(fieldName)

        if(allInfoJSON[fieldName].exists()){
            EventTable(connection: database).insertOrIgnoreNew(eventsFromMySQL: allInfoJSON[fieldName])
        }
        UserDefaultsUtils.updateUpdatedTS(jsonValue: allInfoJSON[fieldName], key: .eventUpdatedTS_str)

        
        //USER EVENT
        print("//USER EVENT")

        fieldName = AllInfo_getAllInfo.ResponseFields.userInEvent
        print(fieldName)

        if(allInfoJSON[fieldName].exists()){
            EventTable(connection: database).insertOrIgnoreEvent(fromMySQL: allInfoJSON[fieldName])
        }
        UserDefaultsUtils.updateUpdatedTS(jsonValue: allInfoJSON[fieldName], key: .inEventUpdatedTS_str)

        
        print("Printing User Default")
        
        print(UserDefaultsUtils.toString())
        
        
    }
}
