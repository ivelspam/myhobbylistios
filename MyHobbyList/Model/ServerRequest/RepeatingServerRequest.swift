import Foundation

class RepeatingServerRequest: NSObject {
    static let shared: RepeatingServerRequest = {
        let timer = RepeatingServerRequest()
        return timer
    }()
    
    var internalTimer: Timer?
    
    func startTimer() {
        
        internalTimer?.invalidate()
        internalTimer = Timer.scheduledTimer(timeInterval: RepTimers.SERVER_REQUEST_UPDATE, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
    }
    
    func pauseTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        internalTimer?.invalidate()
    }
    
    func stopTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        internalTimer?.invalidate()
    }
    
    @objc func doJob() {
        print("doJob")
        print(Date())
        RequestServerUpdates().requestAllNewInfo()
    }
}
