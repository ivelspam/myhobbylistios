import UIKit
import Alamofire
import SwiftyJSON
import JWTDecode
import CoreLocation

class ServerRequest {
    static let EXPRESS_ADDRESS = "https://synchobby.com/api/";
    
    static func  postRequest(request : APIRequest, completion :  @escaping (JSON, Bool) -> Void){
        print("postRequest");
        
        var headers : HTTPHeaders = [:]
        
        if request.token {
            headers["authorization"] = "bearer \(UserDefaults.standard.value(forKey: DefaultKey.Token_str.rawValue)!)"
        }
        
        let completeURL = "\(EXPRESS_ADDRESS)\(request.APIUrl!)";
        print(completeURL)
        
        Alamofire.request(completeURL, method: .post, parameters: request.getParameters(), headers: headers)
            .responseJSON{response in
                
                if response.result.isSuccess {
                     completion(JSON(response.result.value!), true)
                } else {
                    completion(JSON.null, false)
                }
        }
    }
    
    static func getRequest(request : APIRequest, completion :  @escaping (JSON, Bool) -> Void){
        print("postRequest");
        
        var headers : HTTPHeaders = [:]
        
        if request.token {
            headers["authorization"] = "bearer \(UserDefaults.standard.value(forKey: DefaultKey.Token_str.rawValue)!)"
        }
        
        let completeURL = "\(EXPRESS_ADDRESS)\(request.APIUrl!)";
        print(completeURL)
        
        
        Alamofire.request(completeURL, method: .get, parameters: request.keyValues, headers: headers)
            .responseJSON{response in
                
                if response.result.isSuccess {
                    completion(JSON(response.result.value!), true)
                } else {
                    completion(JSON.null, false)
                    
                }
        }
    }
    
    static func downloadFriendPicture(request : APIRequest, completion :  @escaping (URL, Bool) -> Void){

        
        print("downloadFriendPicture")
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("\(request.keyValues[MySQLColumns.AppUserID] as! Int)-\(request.keyValues[MySQLColumns.ImageVersion] as! Int).jpg")
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        let completeURL = "\(EXPRESS_ADDRESS)\(request.APIUrl!)";

        Alamofire.download(completeURL, method: .get, parameters : request.getParameters(), headers: [:], to: destination).response { response in
            
            if response.error == nil, let imagePath = response.destinationURL {
                completion(imagePath, true)
            }
        }
    }
   
    func checkIfFileExists(fileName : String) -> Bool{
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("fileName") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                return true
                
            } else {
                return false
            }
        }
        
        return false
    }
    
    
    
    
    static func uploadProfilePictureToServer(uiImage : UIImage, completion: @escaping (Bool)->Void){

//        let parameters: Parameters<String, String>
        var headers = [String: String]()
        headers["authorization"] = "bearer \(UserDefaults.standard.string(forKey: DefaultKey.Token_str.rawValue)!)"

        
        let upload_profileImage = Upload_profileImage()
        
        let fixedUIImage = uiImage.fixedOrientation()
        if let data = UIImageJPEGRepresentation(fixedUIImage,1) {
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(data, withName: "profileImage",fileName: "file.jpg", mimeType: "image/jpg")

//                for (key, value) in parameters {
//
//                    print("\(key) - \(value)")
//
//                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//                }
            },

            to:"\(EXPRESS_ADDRESS)\(upload_profileImage.APIUrl!)",
            method: .post,
            headers : headers)
            { (result) in
                switch result {
                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseJSON { response in
                        print(response.result.value as Any)
                    }

                    completion(true)


                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        }
    }

}
    
    

    

