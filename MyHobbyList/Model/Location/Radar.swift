import Foundation
import CoreLocation

protocol gotUpdatedLocationDelegate{
    func gotLocation(location : CLLocation?)
}

class Radar: NSObject, CLLocationManagerDelegate {
    
    static let shared = Radar()
    
    let locationManager : CLLocationManager
    var lastUpdate = Date()
    var delegate : gotUpdatedLocationDelegate?
    var gotUpdate = false
    
    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        super.init()
        locationManager.delegate = self
    }
    
    func start() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func getNewUpdate(delegate : gotUpdatedLocationDelegate){
        
        print("getNewUpdate")
        self.delegate = delegate
        
        if(lastUpdate.minutes(from: Date()) < 2){
            gotUpdate = false
            locationManager.startUpdatingLocation()
        }else{
            
            if let location = locationManager.location {
                
                delegate.gotLocation(location: location)
                
            }else{
                gotUpdate = false
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else {
            return
        }
    
        if(gotUpdate){
            
        }else{
            locationManager.stopUpdatingLocation()
            lastUpdate = Date()
            print(mostRecentLocation)
            gotUpdate = true
            delegate?.gotLocation(location: locations[0])
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        locationManager.stopUpdatingLocation()
    }
}
