import Foundation
import SocketIO
import SwiftyJSON

protocol ChatSocketDelegate {
    func receivedMessageFromSocketIO(chat : Chat)
}

class ChatSocketIO{
    let manager : SocketManager
    var friendAppUserID : String
    var socket : SocketIOClient
    var appUserID : String
    var chatSocketDelegate : ChatSocketDelegate
    
    init(friendAppUserID : Int, chatSocketDelegate : ChatSocketDelegate){
        
        print("ChatSocketIO")
        self.friendAppUserID = String(friendAppUserID)
        self.chatSocketDelegate = chatSocketDelegate
        
        appUserID = UserDefaults.standard.string(forKey: DefaultKey.AppUserID_int.rawValue)!
        manager = SocketManager(socketURL: URL(string: "https://synchobby.com")!,
                                config: [
                                    .log(true),
                                    .compress,
                                    .connectParams(["AppUserID" : appUserID, "type" : "chat", "friendAppUserID" : self.friendAppUserID])
                                    ]
        )

        socket = manager.defaultSocket

        socket.on("confirmedMessage") {data, ack in
            print("confirmedMessage")
        }

        socket.on("receivedMessage") {data, ack in
            let chatMessageJSON = JSON(data[0])
            let chat = Chat(fromMySQLChatMessageJSON: chatMessageJSON)
            chatSocketDelegate.receivedMessageFromSocketIO(chat: chat)
        }
        socket.connect()
    }

    func disconnect(){
        socket.disconnect()
    }

    func sendMessage(chatMessage : Chat, completion : @escaping (Bool, JSON)->()){

        print(Utils.convertDictToJSON(dictionary: chatMessage.getMySQLDictionary()))
        socket.emitWithAck("sendMessage", with: [Utils.convertDictToJSON(dictionary: chatMessage.getMySQLDictionary())]).timingOut(after: 1) {data in
           print("sendMessage ack")
            let chatMessageJSON = JSON(data[0])
            completion(true, chatMessageJSON)
        }
    }
    
}
