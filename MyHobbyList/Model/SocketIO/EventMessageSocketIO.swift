import Foundation
import SocketIO
import SwiftyJSON

protocol EventCommentSocketIODelegate {
    func receivedMessageFromSocketIO(eventComment : EventComment)
}

class EventMessageSocketIO{
    let manager : SocketManager
    var socket : SocketIOClient
    var appUserID : String
    var eventCommentSocketIODelegate : EventCommentSocketIODelegate
    var event : Event
    
    init(event : Event,  eventCommentSocketIODelegate : EventCommentSocketIODelegate){
        self.eventCommentSocketIODelegate = eventCommentSocketIODelegate
        self.event = event
        print("EventMessageSocketIO")

        appUserID = UserDefaults.standard.string(forKey: DefaultKey.AppUserID_int.rawValue)!
        manager = SocketManager(socketURL: URL(string: "https://synchobby.com")!,
                                config: [
                                    .log(false),
                                    .compress,
                                    .connectParams(["AppUserID" : appUserID, "type" : "event", "EventID" : event.EventID])
            ]
        )

        socket = manager.defaultSocket
        
        socket.on("clientReceiveEventMessage\(event.EventID)") {data, ack in
            
            print("clientReceiveEventMessage\(event.EventID)")
            
            let eventCommentJSON = JSON(data[0])
            let eventComment = EventComment(jsonEventCommentFromMySQL: eventCommentJSON)
            eventCommentSocketIODelegate.receivedMessageFromSocketIO(eventComment: eventComment)
        }
        socket.connect()
    }
    
    func disconnect(){
        socket.disconnect()
    }
    
    func sendMessage(eventComment : EventComment, completion : @escaping (Bool, JSON)->()){
        
        print(Utils.convertDictToJSON(dictionary: eventComment.getMySQLDictionary()))
        socket.emitWithAck("serverReceiveEventComment", with: [Utils.convertDictToJSON(dictionary: eventComment.getMySQLDictionary())]).timingOut(after: 1) {data in
            print("eventSendMessage ack")
            print(data)
            completion(true, JSON(data))
        }
    }
    
}
