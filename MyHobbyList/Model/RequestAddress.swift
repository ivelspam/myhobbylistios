enum ServerRequestAddress : String{
    
    case Event_leaveEvent = "api/event/leaveevent"
    case Event_closeEvent = "api/event/closeevent"
    case Event_joinEvent = "api/event/joinEvent"
    case Event_hideEvent = "api/event/hideEvent"
    
}


enum RequestResponse : String{
    
    case Event_leaveEvent_left = "left"
    case Event_closeEvent_closed = "closed"
    case Event_joinEvent_joined = "joined"
    case Event_hideEvent_hided = "hided"
    
}

